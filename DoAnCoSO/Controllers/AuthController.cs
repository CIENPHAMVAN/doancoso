﻿using Microsoft.AspNetCore.Mvc;
using DOANCOSO.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace DOANCOSO.Controllers
{
    public class AuthController : Controller
    {
        private readonly QuanlydichvuContext _quanlydichvuContext;
        public AuthController(QuanlydichvuContext quanlydichvuContext)
        {
            _quanlydichvuContext = quanlydichvuContext;
        }

        private const string SessionUsernameKey = "username";
        private const string SessionMaQuanLyKey = "MaQuanLy";

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(QuanLy model)
        {
            var user = _quanlydichvuContext.QuanLies.FirstOrDefault(u => u.Tk == model.Tk && u.Mk == model.Mk);
            if (user != null)
            {
                HttpContext.Session.SetString(SessionUsernameKey, user.TenQuanLy);
                HttpContext.Session.SetString(SessionMaQuanLyKey, user.MaQuanLy.ToString()); // Lưu MaQuanLy vào session
                TempData["SuccessMessage"] = "Login successful!";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["ErrorMessage"] = "Invalid username or password!";
                return View();
            }
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove(SessionUsernameKey);
            HttpContext.Session.Remove(SessionMaQuanLyKey); // Xóa MaQuanLy khỏi session
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Profile()
        {
            var maQuanLy = HttpContext.Session.GetString(SessionMaQuanLyKey);
            if (maQuanLy == null)
            {
                return RedirectToAction("Login");
            }

            int maQuanLyInt = int.Parse(maQuanLy);
            var quanly = _quanlydichvuContext.QuanLies.FirstOrDefault(q => q.MaQuanLy == maQuanLyInt);
            return View(quanly);
        }

        [HttpPost]
        public IActionResult Profile(QuanLy updatedQuanLy)
        {
            var maQuanLy = HttpContext.Session.GetString(SessionMaQuanLyKey);
            if (maQuanLy == null)
            {
                return RedirectToAction("Login");
            }

            int maQuanLyInt = int.Parse(maQuanLy);
            var quanLyFromDb = _quanlydichvuContext.QuanLies.FirstOrDefault(q => q.MaQuanLy == maQuanLyInt);

            if (quanLyFromDb != null)
            {
                quanLyFromDb.TenQuanLy = updatedQuanLy.TenQuanLy;
                quanLyFromDb.DiaChi = updatedQuanLy.DiaChi;
                quanLyFromDb.SoDienThoai = updatedQuanLy.SoDienThoai;

                _quanlydichvuContext.SaveChanges();

                TempData["SuccessMessage"] = "Updated personal information successfully!";
            }
            else
            {
                TempData["ErrorMessage"] = "Không tìm thấy thông tin cá nhân của người quản lý!";
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> ThayDoiMatKhau(string currentPassword, string newPassword, string renewPassword)
        {
            // Lấy mã quản lý từ session
            var maQuanLy = HttpContext.Session.GetString(SessionMaQuanLyKey);
            if (maQuanLy == null)
            {
                return RedirectToAction("Login");
            }

            int maQuanLyInt = int.Parse(maQuanLy);

            // Tìm quản lý trong cơ sở dữ liệu
            var quanLy = await _quanlydichvuContext.QuanLies.FindAsync(maQuanLyInt);
            if (quanLy == null)
            {
                return NotFound();
            }

/*            // Kiểm tra mật khẩu hiện tại
            if (quanLy.Mk != currentPassword)
            {
                ModelState.AddModelError("", "Mật khẩu hiện tại không đúng.");
                return View("Profile", quanLy); // Trả về view Profile và truyền lại model quanLy
            }*/

            // Kiểm tra mật khẩu mới và nhập lại mật khẩu mới
            if (newPassword != renewPassword)
            {
                ModelState.AddModelError("", "Mật khẩu mới và nhập lại mật khẩu mới không khớp.");
                return View("Profile", quanLy); // Trả về view Profile và truyền lại model quanLy
            }

            // Cập nhật mật khẩu mới
            quanLy.Mk = newPassword;
            _quanlydichvuContext.Update(quanLy);
            await _quanlydichvuContext.SaveChangesAsync();

            // Hiển thị thông báo thành công
            TempData["Success"] = "Đổi mật khẩu thành công.";
            return RedirectToAction("Index", "Home");
        }

    }
}
