﻿using DOANCOSO.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DOANCOSO.Controllers
{
    public class SuDungDichVuMayController : Controller
    {
        private readonly QuanlydichvuContext context;
        public SuDungDichVuMayController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }


        [HttpGet]
        [Route("/api/list-service-computer/{maMay}")]
        public async Task<ActionResult<IEnumerable<SuDungDichVuMay>>> GetSuDungDichVuBanByMaMay(int maMay)
        {
            var orederMay = await context.OrderMays
                .Include(ob => ob.SuDungDichVuMays)
                .ThenInclude(sd => sd.MaDichVuNavigation)
                .ThenInclude(dv => dv.MaLoaiDichVuNavigation)
                .FirstOrDefaultAsync(ob => ob.MaMayTinh == maMay && ob.ThoiGianKetThuc == null);
            if (orederMay == null)
            {
                return NotFound();
            }
            return Ok(orederMay);
        }
/*        [HttpGet]
        [Route("/api/service-with-computer/{maLoaiDichVu}")]
        public async Task<ActionResult<IEnumerable<DichVu>>> GetDichVuByLoaiComputer(int maLoaiDichVu)
        {
            var dichVus = await context.DichVus
                .Where(dv => dv.MaLoaiDichVu == maLoaiDichVu)
                .ToListAsync();
            if (dichVus == null || !dichVus.Any())
            {
                return NotFound();
            }

            return Ok(dichVus);
        }*/

        [HttpPost]
        [Route("/api/add-or-update-service-computer")]
        public async Task<ActionResult<SuDungDichVuMay>> AddOrUpdateSuDungDichVuMay([FromBody] SuDungDichVuMay suDungDichVuMay)
        {
            if (suDungDichVuMay == null)
            {
                return BadRequest();
            }
            var existingService = await context.SuDungDichVuMays
                .FirstOrDefaultAsync(s => s.MaOrderMay == suDungDichVuMay.MaOrderMay && s.MaDichVu == suDungDichVuMay.MaDichVu);
            if (existingService != null)
            {
                existingService.SoLuong += suDungDichVuMay.SoLuong;
                existingService.ThanhTien += suDungDichVuMay.ThanhTien;
                context.SuDungDichVuMays.Update(existingService);

            }
            else
            {
                context.SuDungDichVuMays.Add(suDungDichVuMay);

            }
            await context.SaveChangesAsync();
            return Ok();
        }
        // Tăng số lượng dịch vụ bên máy
        [HttpPost]
        [Route("/api/increment-service-computer/{maSd}")]
        public async Task<IActionResult> UpdateServiceQuantityComputer(int maSd)
        {
            try
            {
                var suDungDichVuMay = await context.SuDungDichVuMays.FindAsync(maSd);
                if (suDungDichVuMay == null)
                {
                    return NotFound();
                }
                var dichVu = await context.DichVus.FindAsync(suDungDichVuMay.MaDichVu);
                if (dichVu == null)
                {
                    return BadRequest("Không tìm thấy dịch vụ.");
                }
                suDungDichVuMay.SoLuong += 1;
                suDungDichVuMay.ThanhTien = suDungDichVuMay.SoLuong * dichVu.GiaDichVu;
                await context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                // Xử lý lỗi
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi khi cập nhật số lượng dịch vụ.");
            }
        }

        //Giảm số lượng bên máy
        [HttpPost]
        [Route("/api/decrease-service-computer/{maSd}")]
        public async Task<IActionResult> DescreaseServiceQuantityComputer(int maSd)
        {
            try
            {
                var suDungDichVuMay = await context.SuDungDichVuBans.FindAsync(maSd);

                if (suDungDichVuMay == null)
                {
                    return NotFound();
                }

                // Lấy giá dịch vụ từ bảng DichVu
                var dichVu = await context.DichVus.FindAsync(suDungDichVuMay.MaDichVu);

                if (dichVu == null)
                {
                    return BadRequest("Không tìm thấy dịch vụ.");
                }

                // Cập nhật số lượng
                suDungDichVuMay.SoLuong -= 1;

                // Tính lại thành tiền
                suDungDichVuMay.ThanhTien = suDungDichVuMay.SoLuong * dichVu.GiaDichVu;

                await context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                // Xử lý lỗi
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi khi cập nhật số lượng dịch vụ.");
            }
        }


        //Xóa dịch vụ sử dụng bên máy
        [HttpDelete]
        [Route("/api/delete-with-computer/{maSd}")]
        public async Task<IActionResult> DeleteServiceUseComputer(int maSd)
        {
            try
            {
                var sudungdichvu = await context.SuDungDichVuMays.FindAsync(maSd);
                if (sudungdichvu == null) { return NotFound(); }

                context.SuDungDichVuMays.Remove(sudungdichvu);

                await context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi khi xóa dịch vụ.");
            }
        }


    }
}
