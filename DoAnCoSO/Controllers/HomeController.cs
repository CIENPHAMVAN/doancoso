using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace DOANCOSO.Controllers
{
    [RequireLogin]
    public class HomeController : Controller
    {

        private IHttpContextAccessor Accessor;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IHttpContextAccessor _accessor)
        {
            _logger = logger;
            this.Accessor = _accessor;
        }
        
        public IActionResult Index()
        {
            HttpContext context = this.Accessor.HttpContext;
            return View();
        }
        public IActionResult Privacy()
        {
            HttpContext context = this.Accessor.HttpContext;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
