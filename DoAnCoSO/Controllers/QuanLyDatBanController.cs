﻿using Azure.Core;
using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.IdentityModel.Tokens;



namespace DOANCOSO.Controllers
{
    [RequireLogin]
    public class QuanLyDatBanController : Controller
    {

        private readonly QuanlydichvuContext context;

        public QuanLyDatBanController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }




        public async Task<IActionResult> Index()
        {
            var listBooking = await context.BookingBans.Include(b =>b.MaBanNavigation).Include(b=>b.MaKhachHangNavigation).Include(b=> b.MaTrangThaiNavigation).ToListAsync();

            var listTable = await context.BanBilliards.ToListAsync();
            ViewBag.ListTable = new SelectList(listTable, "MaBan", "TenBan");

            var typeCustomer = await context.KhachHangs.ToListAsync();
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaKhachHang", "TenKhachHang");
            return View(listBooking);
        }


        [HttpPost]
        [Route("/api/add-bookingban")]
        public async Task<IActionResult> AddBookingBan(BookingBan bookingBan)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToList();
                    return BadRequest(new { success = false, message = "Dữ liệu không hợp lệ", errors });
                }

                bookingBan.NgayDat = DateTime.Now;

                if (bookingBan.MaBan == null)
                {
                    // Nếu không có mã bàn, gán trạng thái là 1
                    bookingBan.MaTrangThai = 1;
                }
                else
                {
                    // Nếu có mã bàn, gán trạng thái là 2
                    bookingBan.MaTrangThai = 2;
                }

                // Thêm đặt bàn vào cơ sở dữ liệu
                context.BookingBans.Add(bookingBan);
                await context.SaveChangesAsync();

                return Ok(new { success = true, message = "Đặt bàn thành công !!!." });
            }
            catch (DbUpdateException dbEx)
            {
                var innerExceptionMessage = dbEx.InnerException?.Message ?? dbEx.Message;
                return StatusCode(500, new { success = false, message = "Có lỗi xảy ra khi đặt bàn", details = innerExceptionMessage });
            }
            catch (Exception ex)
            {
                // Log the exception here if needed
                return StatusCode(500, new { success = false, message = "Có lỗi xảy ra khi đặt bàn", details = ex.Message });
            }
        }



        [HttpGet]
        [Route("/api/bookingban/{bookingId}/edit")]
        public async Task<IActionResult> GetBookingBan(int bookingId)
        {
            var bookingban = await context.BookingBans.Include(b => b.MaBanNavigation).Include(b => b.MaKhachHangNavigation).FirstOrDefaultAsync(p => p.MaBookingBan == bookingId);
            var banbilliards = await context.BanBilliards.ToListAsync();


            if (bookingban == null)
            {
                return NotFound();
            }

            var data = new
            {
                BanBilliard = banbilliards,
                BookingBan = bookingban
            };

            return Ok(data);
        }

        [HttpDelete]
        [Route("/api/bookingban/{bookingId}")]
        public async Task<IActionResult> DeleteBookingBan(int bookingId)
        {
            var bookingban = await context.BookingBans.FindAsync(bookingId);
            if (bookingban == null)
            {
                return NotFound(new { success = false, message = "Không tìm thấy đặt bàn." });
            }

            context.BookingBans.Remove(bookingban);
            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Xóa đặt bàn thành công." });

        }

        [HttpPost]
        [Route("/api/bookingban/{bookingId}/cancel")]
        public async Task<IActionResult> CancelBookingBan(int bookingId)
        {
            var bookingban = await context.BookingBans.FindAsync(bookingId);
            if(bookingban == null)
            {
                return NotFound(new { success = false, message = "Không tìm thấy đặt bàn." });

            }

            bookingban.MaTrangThai = 4;
            await context.SaveChangesAsync(); 
            
            return Ok(new { success = true, message = "Hủy đặt bàn thành công." });

        }

        [HttpPost]
        [Route("/api/bookingban/{bookingId}/arrange")]
        public async Task<IActionResult> ArrangeBanbilliardswithbookingban(int bookingId,[FromBody]UpdateBanDatRequest request)
        {
            try
            {
                var bookingban = await context.BookingBans.FindAsync(bookingId);
                if (bookingban == null)
                {
                 
                    return NotFound(new { success = false, message = "Không tìm thấy đặt bàn." });
                }

                if (request.Maban != bookingban.MaBan)
                {
                    bookingban.MaBan = request.Maban;
                    bookingban.MaTrangThai = 2;
                }else if( request.NgayNhannew != bookingban.NgayNhan) 
                { bookingban.NgayNhan = request.NgayNhannew; }
                         
                 await context.SaveChangesAsync();

                return Ok(new { success = true, message = "Đã cập nhật đặt bàn thành công." });
            }
            catch (Exception ex)
            {
                var innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                return StatusCode(500, new { success = false, message = "Đã xảy ra lỗi khi xếp bàn. Chi tiết: " + ex.Message + ". Inner exception: " + innerExceptionMessage });
            }

        }

        [HttpPost]
        [Route("/api/bookingban/{bookingId}/receive")]
        public async Task<IActionResult> ReceiveBookingBan(int bookingId, [FromBody] int maban)
        {
            var bookingban = await context.BookingBans.FindAsync(bookingId);

            var infoban = await context.BanBilliards.FindAsync(maban);

            if(bookingban == null)
            {
                return NotFound(new { success = false, message = "Không tìm thấy đặt bàn." });
            }
            if(maban == null)
            {
                return BadRequest(new { succes = false, message = "Chưa có bàn được xếp. Vui lòng xếp bàn trước khi nhận bàn!" });
            }
            if(infoban.MaTinhTrangBan == 2)
            {
                return BadRequest(new { succes = false, message = "Bàn đang có người sử dụng vui lòng xếp bàn khác" });
            }
            var addorderban = new OrderBan{
                MaKhachHang=bookingban.MaKhachHang,
                MaBan = maban,
                ThoiGianBatDau = TimeOnly.FromDateTime(DateTime.Now),
                MaBookingBan = bookingId
            };

            bookingban.MaTrangThai = 3;
            bookingban.MaBan = maban;

            context.OrderBans.Add(addorderban);


            infoban.MaTinhTrangBan = 2;
            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Nhận bàn thành công." });
        }

        [HttpGet]
        [Route("/api/bookingban/getlist/{maTrangThai}")]
        public async Task<IActionResult> ListBookingBanWithTrangThai(int maTrangThai)
        {
            var list = await context.BookingBans
                    .Include(p => p.MaKhachHangNavigation)
                    .Include(p=> p.MaBanNavigation)
                    .Include(p=>p.MaTrangThaiNavigation)
                    .Where(dv => dv.MaTrangThai == maTrangThai)
                    .ToListAsync();

            return Ok(list);


        }



        public class UpdateBanDatRequest
        {
            public int Maban { get; set; }
            public DateTime NgayNhannew { get; set; }
        }

    }
}
