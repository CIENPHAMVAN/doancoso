﻿using Azure.Core;
using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
namespace DOANCOSO.Controllers
{
/*    [RequireLogin]
*/    public class ManementTableController : Controller
    {
        private readonly QuanlydichvuContext context;

        public ManementTableController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }

        public async Task<IActionResult>  Index()
        {
            var table = await context.BanBilliards.Include(p => p.MaLoaiBanNavigation).Include(p => p.MaTinhTrangBanNavigation).ToListAsync();
            var typeCustomer = await context.KhachHangs.ToListAsync();
            var typeService = await context.LoaiDichVus.ToListAsync();
            ViewBag.TypeService = new SelectList(typeService, "MaLoaiDichVu", "TenLoaiDv");
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaKhachHang", "TenKhachHang");
            return View(table);
        }

        [HttpGet]
        [Route("/api/table/types")]
        public async Task<IActionResult> GetTypeTable()
        {
            var typeTable =  await context.LoaiBans.ToListAsync();
            return Json(typeTable);
        }



        [HttpPost]
        [Route("/api/table/addtable")]
        public async Task<IActionResult> AddTable(BanBilliard banBilliard)
        {
            if (string.IsNullOrEmpty(banBilliard.TenBan))
            {
                ModelState.AddModelError("TenBan", "Tên Bàn bắt buộc phải nhập.");
            }
            if (banBilliard.MaLoaiBan == null)
            {
                ModelState.AddModelError("MaLoaiBan", "Loại Bàn bắt buộc phải chọn.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Tự động gán trạng thái bàn là 1 (bàn trống)
            banBilliard.MaTinhTrangBan = 1;

            context.BanBilliards.Add(banBilliard);
            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Thêm bàn thành công." });
        }


        [HttpGet("/api/BanBilliard/{id}/Edit")]
        public async Task<IActionResult> GetTable(int id)
        {
            var ban = await context.BanBilliards.FindAsync(id);
            var loaiBans = await context.LoaiBans.ToListAsync();

            if (ban == null)
            {
                return NotFound();
            }

            var data = new
            {
                BanBilliard = ban,
                LoaiBan = loaiBans
            };

            return Ok(data);
        }

        // Xử lý cập nhật 
        [HttpPut("/api/BanBilliard/{id}/Edit")]
        public async Task<IActionResult> UpdateBan(int id,[FromBody] BanBilliard ban)
        {
            if (id != ban.MaBan)
            {
                return BadRequest();
            }

            var existingTable = await context.BanBilliards.FindAsync(id);

            if (existingTable == null)
            {
                return NotFound();
            }

            existingTable.TenBan = ban.TenBan;
            existingTable.MaLoaiBan = ban.MaLoaiBan;
            // Cập nhật các trường khác tương tự nếu cần

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        private bool TableExists(int id)
        {
            return context.BanBilliards.Any(e => e.MaBan == id);
        }


        [HttpDelete("/api/table/delete-table/{id}")]
        public async Task<IActionResult> DeleteTable(int id)
        {
            var table = await context.BanBilliards.FindAsync(id);
            if (table == null)
            {
                return NotFound(); // Trả về mã lỗi 404 nếu không tìm thấy bàn
            }
            if( table.MaTinhTrangBan == 2) {
                return BadRequest($"Bạn chưa được thanh toán không thể xóa");
            }
            try
            {
                context.BanBilliards.Remove(table);
                await context.SaveChangesAsync();
                return NoContent(); // Trả về mã lỗi 204 (No Content) nếu xóa thành công
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Lỗi khi xóa bàn với ID {id}: {ex.Message}");
                return BadRequest($"Đã xảy ra lỗi khi xóa bàn: {ex.Message}"); // Trả về lỗi BadRequest và thông báo lỗi
            }
        }

        // Get Customer 
        [HttpGet]
        public async Task<IActionResult> GetCustomer()
        {
            var customers = await context.KhachHangs.Include(p => p.MaLoaiKhachHangNavigation).ToListAsync();
            return Json(customers);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrderBan(int maKH, int maBan)
        {
            try
            {
                // Tạo một đối tượng OrderBan mới
                
                var orderBan = new OrderBan
                {
                    MaKhachHang = maKH,
                    MaBan = maBan,
                    ThoiGianBatDau = TimeOnly.FromDateTime(DateTime.Now), // Lấy giờ hiện tại
                };

                // Lưu thay đổi vào cơ sở dữ liệu
                context.OrderBans.Add(orderBan);

                // Chuyển trạng thái của bàn sang "bận"
                var table = context.BanBilliards.Find(maBan);
                if (table != null)
                {
                    table.MaTinhTrangBan = 2; 
                    context.Entry(table).State = EntityState.Modified;
                }

                await context.SaveChangesAsync();

                // Trả về kết quả thành công
                return Ok(new { success = true, message = "Bàn đã được mở và orderban đã được tạo thành công." });
            }
            catch (Exception ex)
            {
                //Hiện ra lỗi chi tiết 
                var innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";

                // Trả về kết quả lỗi kèm thông tin từ inner exception
                return StatusCode(500, new { success = false, message = "Đã xảy ra lỗi khi mở bàn và tạo orderban. Chi tiết: " + ex.Message + ". Inner exception: " + innerExceptionMessage });
            }
        }

        // ADd Loại Bàn và Trạng Thái Bàn

        [HttpPost]
        public async Task<IActionResult> AddTypeTable([FromBody] LoaiBan typeTable)
        {
            if (ModelState.IsValid)
            {
                context.LoaiBans.Add(typeTable);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }

        // thêm trạng thái bàn 
        [HttpPost]
        public async Task<IActionResult> AddStatusTable([FromBody] TinhTrangBan statusTable)
        {
            if (ModelState.IsValid)
            {
                context.TinhTrangBans.Add(statusTable);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }




    }
}
