﻿using DOANCOSO.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DOANCOSO.Controllers
{
    public class SuDungDichVuController : Controller
    {
        private readonly QuanlydichvuContext context;

        public SuDungDichVuController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }


        [HttpGet]
        [Route("/api/list-service/{maBan}")]
        public async Task<ActionResult<IEnumerable<SuDungDichVuBan>>> GetSuDungDichVuBanByMaBan(int maBan)
        {
            var orderBan = await context.OrderBans
                   .Include(ob => ob.SuDungDichVuBans)
                    .ThenInclude(sd => sd.MaDichVuNavigation)
                        .ThenInclude(dv => dv.MaLoaiDichVuNavigation)
                            .FirstOrDefaultAsync(ob => ob.MaBan == maBan && ob.ThoiGianKetThuc == null);
          
            if (orderBan == null)
            {
                return NotFound(); 
            }

            return Ok(orderBan);
        }



        [HttpGet]
        [Route("/api/service-with/{maLoaiDichVu}")]
        public async Task<ActionResult<IEnumerable<DichVu>>> GetDichVuByLoai(int maLoaiDichVu)
        {
            var dichVus = await context.DichVus
                .Where(dv => dv.MaLoaiDichVu == maLoaiDichVu)
                .ToListAsync();

            if (dichVus == null || !dichVus.Any())
            {
                return NotFound();
            }

            return Ok(dichVus);
        }

        [HttpPost]
        [Route("/api/add-or-update-service")]
        public async Task<ActionResult<SuDungDichVuBan>> AddOrUpdateSuDungDichVuBan([FromBody] SuDungDichVuBan suDungDichVuBan)
        {
            if (suDungDichVuBan == null)
            {
                return BadRequest();
            }

            var existingService = await context.SuDungDichVuBans
                .FirstOrDefaultAsync(s => s.MaOrderBan == suDungDichVuBan.MaOrderBan && s.MaDichVu == suDungDichVuBan.MaDichVu);

            if (existingService != null)
            {
                // Cập nhật số lượng và thành tiền
                existingService.SoLuong += suDungDichVuBan.SoLuong;
                existingService.ThanhTien += suDungDichVuBan.ThanhTien;
                context.SuDungDichVuBans.Update(existingService);
            }
            else
            {
                // Thêm mới dịch vụ
                context.SuDungDichVuBans.Add(suDungDichVuBan);
            }

            await context.SaveChangesAsync();

            return Ok();
        }

        // Tăng số lượng dịch vụ bên bàn
        [HttpPost]
        [Route("/api/increment-service/{maSd}")]
        public async Task<IActionResult> UpdateServiceQuantity(int maSd)
        {
            try
            {
                var suDungDichVuBan = await context.SuDungDichVuBans.FindAsync(maSd);
            
                if (suDungDichVuBan == null)
                {
                    return NotFound();
                }

                // Lấy giá dịch vụ từ bảng DichVu
                var dichVu = await context.DichVus.FindAsync(suDungDichVuBan.MaDichVu);

                if (dichVu == null)
                {
                    return BadRequest("Không tìm thấy dịch vụ.");
                }

                // Cập nhật số lượng
                suDungDichVuBan.SoLuong += 1;

                // Tính lại thành tiền
                suDungDichVuBan.ThanhTien = suDungDichVuBan.SoLuong * dichVu.GiaDichVu;

                await context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                // Xử lý lỗi
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi khi cập nhật số lượng dịch vụ.");
            }
        }

        //Giảm số lượng bên bàn 
        [HttpPost]
        [Route("/api/decrease-service/{maSd}")]
        public async Task<IActionResult> DescreaseServiceQuantity(int maSd)
        {
            try
            {
                var suDungDichVuBan = await context.SuDungDichVuBans.FindAsync(maSd);

                if (suDungDichVuBan == null)
                {
                    return NotFound();
                }

                // Lấy giá dịch vụ từ bảng DichVu
                var dichVu = await context.DichVus.FindAsync(suDungDichVuBan.MaDichVu);

                if (dichVu == null)
                {
                    return BadRequest("Không tìm thấy dịch vụ.");
                }

                // Cập nhật số lượng
                suDungDichVuBan.SoLuong -= 1;

                // Tính lại thành tiền
                suDungDichVuBan.ThanhTien = suDungDichVuBan.SoLuong * dichVu.GiaDichVu;

                await context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                // Xử lý lỗi
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi khi cập nhật số lượng dịch vụ.");
            }
        }

        //Xóa dịch vụ sử dụng bên bàn
        [HttpDelete]
        [Route("/api/delete-with/{maSd}")]
        public async Task<IActionResult> DeleteServiceUse (int maSd)
        {
            try
            {
                var sudungdichvu = await context.SuDungDichVuBans.FindAsync (maSd);
                if(sudungdichvu == null) { return NotFound(); }

                context.SuDungDichVuBans.Remove(sudungdichvu);

                await context.SaveChangesAsync();

                return Ok();
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi khi xóa dịch vụ.");
            }
        }

    }
}
