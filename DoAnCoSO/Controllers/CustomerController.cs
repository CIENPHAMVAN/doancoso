﻿using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
namespace DOANCOSO.Controllers
{
    [RequireLogin]
    public class CustomerController : Controller
    {
        private readonly QuanlydichvuContext context;

        public CustomerController(QuanlydichvuContext _context)
        {
            context = _context;
        }


        public async Task<IActionResult> Index()
        {
            var customer = await context.KhachHangs.Include(p => p.MaLoaiKhachHangNavigation).ToListAsync();
            var typeCustomer = await context.LoaiKhachHangs.ToListAsync();
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaLoaiKhachHang", "TenLoaiKhachHang");
            return View(customer);
        }

        public async Task<IActionResult> AddCustomer()
        {
            var typeCustomer = await context.LoaiKhachHangs.ToListAsync();
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaLoaiKhachHang", "TenLoaiKhachHang");
            return View();

        }


        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> AddCustomer(KhachHang khachHang)
        {
            if (ModelState.IsValid)
            {
                context.KhachHangs.Add(khachHang);
                await context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập
            var typeCustomer = await context.LoaiKhachHangs.ToListAsync();
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaLoaiKhachHang", "TenLoaiKhachHang");
            return View(khachHang);
        }

        [HttpPost]
        public async Task<IActionResult> AddTypeCustomer([FromBody] LoaiKhachHang loaiKhachHang)
        {
            if (ModelState.IsValid)
            {
                context.LoaiKhachHangs.Add(loaiKhachHang);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }
        public async Task<IActionResult> EditCustomer(int id)
        {
            var khachHang = await context.KhachHangs.FindAsync(id);
            if (khachHang == null)
            {
                return NotFound();
            }

            var typeCustomer = await context.LoaiKhachHangs.ToListAsync();
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaLoaiKhachHang", "TenLoaiKhachHang", khachHang.MaLoaiKhachHang);
            return View(khachHang);
        }
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> EditCustomer(int id, KhachHang khachHang)
        {
            if (id != khachHang.MaKhachHang)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    context.Update(khachHang);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KhachHangExists(khachHang.MaKhachHang))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var typeCustomer = await context.LoaiKhachHangs.ToListAsync();
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaLoaiKhachHang", "TenLoaiKhachHang", khachHang.MaLoaiKhachHang);
            return View(khachHang);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            try
            {
                var khachHang = await context.KhachHangs.FindAsync(id);
                if (khachHang == null)
                {
                    return NotFound();
                }

                context.KhachHangs.Remove(khachHang);
                await context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                TempData["ErrorMessage"] = "Không thể xóa khách hàng vì khách hàng đang sử dụng một dịch vụ.";
                return RedirectToAction(nameof(Index));
            }
        }


        private bool KhachHangExists(int id)
        {
            return context.KhachHangs.Any(e => e.MaKhachHang == id);
        }

    }
}
