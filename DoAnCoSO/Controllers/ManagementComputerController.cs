﻿using Azure.Core;
using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
namespace DOANCOSO.Controllers
{
    [RequireLogin]
    public class ManagementComputerController : Controller
    {
        private readonly QuanlydichvuContext context;
     
        public ManagementComputerController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }

        public async Task<IActionResult> Index()
        {
            var table = await context.MayTinhs.Include(p => p.MaLoaiMayNavigation).Include(p => p.MaTinhTrangMayTinhNavigation).ToListAsync();
            var typeCustomer = await context.KhachHangs.ToListAsync();
            var typeService = await context.LoaiDichVus.ToListAsync();
            ViewBag.TypeService = new SelectList(typeService, "MaLoaiDichVu", "TenLoaiDv");
            ViewBag.TypeCustomer = new SelectList(typeCustomer, "MaKhachHang", "TenKhachHang");
            return View(table);
        }

        [HttpGet]
        [Route("/api/computer/types")]
        public async Task<IActionResult> GetTypeComputer()
        {
            var typeTable = await context.LoaiMays.ToListAsync();
            return Json(typeTable);
        }

        [HttpPost]
        [Route("/api/computer/addcomputer")]
        public async Task<IActionResult> AddComputer(MayTinh mayTinh)
        {
            if (string.IsNullOrEmpty(mayTinh.TenMayTinh))
            {
                ModelState.AddModelError("TenMayTinh", "Tên Máy Bắt Buộc Phải Nhập.");
            }
            if (mayTinh.MaLoaiMay == null)
            {
                ModelState.AddModelError("MaLoaiMay", "Loại Máy Bắt Buộc Phải Nhập");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Tự động gán trạng thái bàn là 1 (bàn trống)
            mayTinh.MaTinhTrangMayTinh= 1;

            context.MayTinhs.Add(mayTinh);
            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Thêm Máy Tính Mới Thành Công." });
        }

        //Chỉnh Sửa Bàn
        [HttpGet("/api/UpdateMay/{id}/Update")]
        public async Task<IActionResult> GetComputer(int id)
        {
            var computer = await context.MayTinhs.FindAsync(id);
            var loaimaytinh = await context.LoaiMays.ToListAsync();
            if (computer == null)
            {
                return NotFound();
            }
            var data = new
            {
                MayTinh = computer,
                LoaiMay = loaimaytinh
            };
            return Ok(data);
        }
        [HttpPut("/api/UpdateMay/{id}/Update")]
        public async Task<IActionResult> UpdateComputer(int id, [FromBody] MayTinh computer)
        {
            if (id != computer.MaMayTinh)
            {
                return BadRequest();
            }
            var exstingComputer = await context.MayTinhs.FindAsync(id);
            if (exstingComputer == null)
            {
                return NotFound();
            }
            exstingComputer.TenMayTinh = computer.TenMayTinh;
            exstingComputer.MaLoaiMay = computer.MaLoaiMay;
            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!ComputerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
               
            }
            return NoContent();
        }
        private bool ComputerExists(int id)
        {
            return context.MayTinhs.Any(a => a.MaMayTinh == id);   
        }
        

        [HttpDelete("/api/computer/delete-computer/{id}")]
        public async Task<IActionResult> DeleteComputer(int id)
        {
            var computer = await context.MayTinhs.FindAsync(id);
            if (computer == null)
            {
                return NotFound(); // Trả về mã lỗi 404 nếu không tìm thấy bàn
            }
            if (computer.MaTinhTrangMayTinh == 2)
            {
                return BadRequest($"Bạn chưa được thanh toán không thể xóa");
            }
            try
            {
                context.MayTinhs.Remove(computer);
                await context.SaveChangesAsync();
                return NoContent(); // Trả về mã lỗi 204 (No Content) nếu xóa thành công
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Lỗi khi xóa bàn với ID {id}: {ex.Message}");
                return BadRequest($"Đã xảy ra lỗi khi xóa bàn: {ex.Message}"); // Trả về lỗi BadRequest và thông báo lỗi
            }
        }

        [HttpGet]
         public async Task<IActionResult> GetKhachHang()
         {
            var customers = await context.KhachHangs.Include(p => p.MaLoaiKhachHangNavigation).ToListAsync();
            return Json(customers);
         }
        [HttpPost]
        public async Task<IActionResult> AddOrderMay(int maKhachHang, int maMayTinh)
        {
            try
            {
                var orderMay = new OrderMay
                {
                    MaKhachHang = maKhachHang,
                    MaMayTinh = maMayTinh,
                    ThoiGianBatDau = TimeOnly.FromDateTime(DateTime.Now),

                };
                context.OrderMays.Add(orderMay);
                var computer = context.MayTinhs.Find(maMayTinh);
                if (computer != null)
                {
                    computer.MaTinhTrangMayTinh = 2;
                    context.Entry(computer).State = EntityState.Modified;
                }
                await context.SaveChangesAsync();
                return Ok(new { success = true, message = "Máy đã được mở và Order Máy đã được tạo thành công." });
                
            }
            catch(Exception ex )
            {
                var innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                return StatusCode(500, new { success = false, message = "Đã xảy ra lỗi khi mở máy và tạo orderMay. Chi Tiết: " + ex.Message + " Inner exception: " + innerExceptionMessage });
            }
        }

        // Thêm loại Máy
        [HttpPost]
        public async Task<IActionResult> AddTypeComputer([FromBody] LoaiMay loaiMay)
        {
            if (ModelState.IsValid)
            {
                context.LoaiMays.Add(loaiMay);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }

        // Thêm Trạng Thái Máy
        [HttpPost]
        public async Task<IActionResult> AddStatusComputer([FromBody] TinhTrangMayTinh statusComputer)
        {
            if (ModelState.IsValid)
            {
                context.TinhTrangMayTinhs.Add(statusComputer);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }
    }
}
