﻿using DOANCOSO.DTOs;
using DOANCOSO.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DOANCOSO.Controllers
{
    public class HoaDonController : Controller
    {
        private readonly QuanlydichvuContext context;

        public HoaDonController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }




        //Thanh Toán Bên Bàn
        public IActionResult ThanhToan(int maBan)
        {
            var orderBan = context.OrderBans.FirstOrDefault(ob => ob.MaBan == maBan && ob.ThoiGianKetThuc == null);
            if (orderBan == null)
            {
                return NotFound();
            }
            orderBan.ThoiGianKetThuc = TimeOnly.FromDateTime(DateTime.Now);
            if (orderBan.ThoiGianBatDau.HasValue && orderBan.ThoiGianKetThuc.HasValue)
            {
                var duration = orderBan.ThoiGianKetThuc.Value - orderBan.ThoiGianBatDau.Value;
                orderBan.SoGio = (float)Math.Round(duration.TotalHours, 2);
                var table = context.BanBilliards.Include(t => t.MaLoaiBanNavigation).FirstOrDefault(b => b.MaBan == maBan);
                if (table != null)
                {
                    var giaTheoLoaiBan = table.MaLoaiBanNavigation.GiaTheoLoaiBan.GetValueOrDefault();
                    var soGio = orderBan.SoGio.GetValueOrDefault();
                    orderBan.TienBan = (decimal)soGio * giaTheoLoaiBan;
                    table.MaTinhTrangBan = 1;
                }
                context.SaveChanges();
            }
            var suDungDichVuBans = context.SuDungDichVuBans.Where(sddb => sddb.MaOrderBan == orderBan.MaOrderBan).ToList();  
            var tongThanhTien = suDungDichVuBans.Sum(sddb => sddb.ThanhTien ?? 0);
            var hoaDonBan = new HoaDonBan
            {
                MaOrderBan = orderBan.MaOrderBan,
                MaKhachHang = orderBan.MaKhachHang,
                MaPhuongThuc = null, 
                NgayThanhToanBan = DateTime.Now,
                TongTien = orderBan.TienBan + tongThanhTien
                };

            context.HoaDonBans.Add(hoaDonBan);
            context.SaveChanges();
            return RedirectToAction("Payment", new { maHoaDon = hoaDonBan.MaHoaDonBan });
        }
        //Thanh Toán Bên Máy
        public IActionResult ThanhToanMay(int maMay)
        {
            var orderMay = context.OrderMays.FirstOrDefault(ob => ob.MaMayTinh == maMay && ob.ThoiGianKetThuc == null);
            if (orderMay == null)
            {
                return NotFound();
            }
            orderMay.ThoiGianKetThuc = TimeOnly.FromDateTime(DateTime.Now);
            if (orderMay.ThoiGianBatDau.HasValue && orderMay.ThoiGianKetThuc.HasValue)
            {
                var duration = orderMay.ThoiGianKetThuc.Value - orderMay.ThoiGianBatDau.Value;
                orderMay.SoGio = (float)Math.Round(duration.TotalHours, 2);
                var table = context.MayTinhs.Include(t => t.MaLoaiMayNavigation).FirstOrDefault(b => b.MaMayTinh == maMay); 
                if(table != null)
                {
                    var giaTheoLoaiMay = table.MaLoaiMayNavigation.GiaTheoLoaiMay.GetValueOrDefault();
                    var soGio = orderMay.SoGio.GetValueOrDefault();
                    orderMay.TienMay = (decimal)soGio * giaTheoLoaiMay;
                    table.MaTinhTrangMayTinh = 1;
                }
                context.SaveChanges();
            }
            var suDungDichVuMays = context.SuDungDichVuMays.Where(sddb => sddb.MaOrderMay == orderMay.MaOrderMay).ToList();
            var tongThanhTien = suDungDichVuMays.Sum(sddb => sddb.ThanhTien ?? 0);

            var hoaDonMay = new HoaDonMay
            {
                MaOrderMay = orderMay.MaOrderMay,
                MaKhachHang = orderMay.MaKhachHang,
                MaPhuongThuc = null,
                NgayThanhToanBan = DateTime.Now,
                TongTien = orderMay.TienMay + tongThanhTien
            };

            context.HoaDonMays.Add(hoaDonMay);
            context.SaveChanges();
            return RedirectToAction("PaymentComputer", new {maHoaDon = hoaDonMay.MaHoaDonMay});
        }




        //Thanh Toán Bên Bàn
        public IActionResult Payment(int maHoaDon)
        {
            var hoadon = context.HoaDonBans
                .Include(ob => ob.MaKhachHangNavigation)
                .Include(ob => ob.MaOrderBanNavigation)
                .ThenInclude(ob => ob.MaBanNavigation)
                .ThenInclude(lb => lb.MaLoaiBanNavigation)
                .Include(ob => ob.MaOrderBanNavigation)
                .ThenInclude(ob => ob.SuDungDichVuBans)
                .ThenInclude(sd => sd.MaDichVuNavigation)
                .ThenInclude(dv => dv.MaLoaiDichVuNavigation)
                .FirstOrDefault(ob => ob.MaHoaDonBan == maHoaDon);


            if (hoadon == null)
            {
                // Handle the case where the order is not found
                return NotFound();
            }

            return View(hoadon);
        }
        //Thanh Toán Bên Máy
        public IActionResult PaymentComputer(int maHoaDon)
        {
            var hoadon = context.HoaDonMays
                .Include(Ob => Ob.MaKhachHangNavigation)
                .Include(Ob => Ob.MaOrderMayNavigation)
                .ThenInclude(Ob => Ob.MaMayTinhNavigation)
                .ThenInclude(ob => ob.MaLoaiMayNavigation)
                .Include(Ob => Ob.MaOrderMayNavigation)
                .ThenInclude(Sd => Sd.SuDungDichVuMays)
                .ThenInclude(Dv => Dv.MaDichVuNavigation)
                .ThenInclude(dv => dv.MaLoaiDichVuNavigation)
                .FirstOrDefault(Ob => Ob.MaHoaDonMay == maHoaDon);
            if (hoadon == null)
            {
                return NotFound();
            }
            return View(hoadon);
        }



        [HttpGet]
        [Route("/api/today-sales")]
        public async Task<ActionResult<decimal>> GetTodaySalesReport()
        {
            var today = DateTime.Today;
            var totalSales = await context.HoaDonBans
                .Where(h => h.NgayThanhToanBan.HasValue &&
                            h.NgayThanhToanBan.Value.Date == today)
                .SumAsync(h => h.TongTien ?? 0);

            return Ok(totalSales);
        }

        [HttpGet]
        [Route("/api/today-sales-computer")]
        public async Task<ActionResult<decimal>> GetTodaySalesReportConputer()
        {
            var today = DateTime.Today;
            var totalSales = await context.HoaDonMays
                .Where(h => h.NgayThanhToanBan.HasValue &&
                            h.NgayThanhToanBan.Value.Date == today)
                .SumAsync(h => h.TongTien ?? 0);

            return Ok(totalSales);
        }
        [HttpGet]
        //public IActionResult TodaySales()
        //{
        //    var today = DateTime.Today;
        //    var sales = context.HoaDonBans
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date == today)
        //        .Sum(h => h.TongTien) ?? 0;
        //    return Json(sales);
        //}
        //[HttpGet]
        //public IActionResult TodaySalesComputer()
        //{
        //    var today = DateTime.Today;
        //    var sales = context.HoaDonMays
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date == today)
        //        .Sum(h => h.TongTien) ?? 0;
        //    return Json(sales);
        //}

        //[HttpGet]
        //public IActionResult YesterdaySales()
        //{
        //    var yesterday = DateTime.Today.AddDays(-1);
        //    var sales = context.HoaDonBans
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date == yesterday)
        //        .Sum(h => h.TongTien) ?? 0;
        //    return Json(sales);
        //}
        //[HttpGet]
        //public IActionResult YesterdaySalesComputer()
        //{
        //    var yesterday = DateTime.Today.AddDays(-1);
        //    var sales = context.HoaDonMays
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date == yesterday)
        //        .Sum(h => h.TongTien) ?? 0;
        //    return Json(sales);
        //}

        //[HttpGet]
        //public IActionResult Last7DaysSales()
        //{
        //    var today = DateTime.Today;
        //    var sevenDaysAgo = today.AddDays(-7);
        //    var sales = context.HoaDonBans
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= sevenDaysAgo && h.NgayThanhToanBan.Value.Date <= today)
        //        .GroupBy(h => h.NgayThanhToanBan.Value.Date)
        //        .Select(g => new { Date = g.Key, TotalSales = g.Sum(h => h.TongTien) })
        //        .ToList();
        //    return Json(sales);
        //}
        //[HttpGet]
        //public IActionResult Last7DaysSalesComputer()
        //{
        //    var today = DateTime.Today;
        //    var sevenDaysAgo = today.AddDays(-7);
        //    var sales = context.HoaDonMays
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= sevenDaysAgo && h.NgayThanhToanBan.Value.Date <= today)
        //        .GroupBy(h => h.NgayThanhToanBan.Value.Date)
        //        .Select(g => new { Date = g.Key, TotalSales = g.Sum(h => h.TongTien) })
        //        .ToList();
        //    return Json(sales);
        //}
        //[HttpGet]
        //public IActionResult ThisMonthSales()
        //{
        //    var today = DateTime.Today;
        //    var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);
        //    var sales = context.HoaDonBans
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= firstDayOfMonth && h.NgayThanhToanBan.Value.Date <= today)
        //        .GroupBy(h => h.NgayThanhToanBan.Value.Date)
        //        .Select(g => new { Date = g.Key, TotalSales = g.Sum(h => h.TongTien) })
        //        .ToList();
        //    return Json(sales);
        //}
        //[HttpGet]
        //public IActionResult ThisMonthSalesComputer()
        //{
        //    var today = DateTime.Today;
        //    var firstDayOfMonth = new DateTime(today.Year, today.Month, 1);
        //    var sales = context.HoaDonMays
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= firstDayOfMonth && h.NgayThanhToanBan.Value.Date <= today)
        //        .GroupBy(h => h.NgayThanhToanBan.Value.Date)
        //        .Select(g => new { Date = g.Key, TotalSales = g.Sum(h => h.TongTien) })
        //        .ToList();
        //    return Json(sales);
        //}

        //[HttpGet]
        //public IActionResult LastMonthSales()
        //{
        //    var today = DateTime.Today;
        //    var firstDayOfThisMonth = new DateTime(today.Year, today.Month, 1);
        //    var lastMonth = firstDayOfThisMonth.AddMonths(-1);
        //    var firstDayOfLastMonth = new DateTime(lastMonth.Year, lastMonth.Month, 1);
        //    var lastDayOfLastMonth = firstDayOfThisMonth.AddDays(-1);

        //    var sales = context.HoaDonBans
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= firstDayOfLastMonth && h.NgayThanhToanBan.Value.Date <= lastDayOfLastMonth)
        //        .GroupBy(h => h.NgayThanhToanBan.Value.Date)
        //        .Select(g => new { Date = g.Key, TotalSales = g.Sum(h => h.TongTien) })
        //        .ToList();
        //    return Json(sales);
        //}
        //[HttpGet]
        //public IActionResult LastMonthSalesComputer()
        //{
        //    var today = DateTime.Today;
        //    var firstDayOfThisMonth = new DateTime(today.Year, today.Month, 1);
        //    var lastMonth = firstDayOfThisMonth.AddMonths(-1);
        //    var firstDayOfLastMonth = new DateTime(lastMonth.Year, lastMonth.Month, 1);
        //    var lastDayOfLastMonth = firstDayOfThisMonth.AddDays(-1);

        //    var sales = context.HoaDonMays
        //        .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= firstDayOfLastMonth && h.NgayThanhToanBan.Value.Date <= lastDayOfLastMonth)
        //        .GroupBy(h => h.NgayThanhToanBan.Value.Date)
        //        .Select(g => new { Date = g.Key, TotalSales = g.Sum(h => h.TongTien) })
        //        .ToList();
        //    return Json(sales);
        //}


        [HttpGet]
        public IActionResult CompletedOrdersCount()
        {
            var completedOrdersCount = context.HoaDonBans
                .Count(h => h.NgayThanhToanBan != null);
            return Json(completedOrdersCount);
        }


        [HttpGet]
        public IActionResult ServingOrdersCount()
        {
            var servingOrdersCount = context.OrderBans
                .Count(o => o.ThoiGianKetThuc == null);

            return Json(servingOrdersCount);
        }




        [HttpGet]
        public IActionResult CompletedOrdersCountComputer()
        {
            var completedOrdersCount = context.HoaDonMays
                .Count(h => h.NgayThanhToanBan != null);
            return Json(completedOrdersCount);
        }
        [HttpGet]
        public IActionResult ServingOrdersCountComputer()
        {
            var servingOrdersCount = context.OrderMays
                .Count(o => o.ThoiGianKetThuc == null);

            return Json(servingOrdersCount);
        }


        [HttpGet]
        public IActionResult CustomerCount()
        {
            var customerCount = context.KhachHangs.Count();
            return Json(customerCount);
        }
        [HttpGet]
        public IActionResult CombinedSales(string period)
        {
            DateTime today = DateTime.Today;
            DateTime startDate;
            DateTime endDate = today;

            switch (period)
            {
                case "today":
                    startDate = today;
                    break;
                case "yesterday":
                    startDate = today.AddDays(-1);
                    endDate = startDate;
                    break;
                case "last7days":
                    startDate = today.AddDays(-7);
                    break;
                case "thismonth":
                    startDate = new DateTime(today.Year, today.Month, 1);
                    break;
                case "lastmonth":
                    var firstDayOfThisMonth = new DateTime(today.Year, today.Month, 1);
                    var lastMonth = firstDayOfThisMonth.AddMonths(-1);
                    startDate = new DateTime(lastMonth.Year, lastMonth.Month, 1);
                    endDate = firstDayOfThisMonth.AddDays(-1);
                    break;
                default:
                    return BadRequest("Invalid period");
            }

            var salesBan = context.HoaDonBans
                .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= startDate && h.NgayThanhToanBan.Value.Date <= endDate)
                .GroupBy(h => h.NgayThanhToanBan.Value.Date)
                .Select(g => new SalesData { Date = g.Key, TotalSalesBan = g.Sum(h => h.TongTien), TotalSalesMay = 0 })
                .ToList();

            var salesMay = context.HoaDonMays
                .Where(h => h.NgayThanhToanBan.HasValue && h.NgayThanhToanBan.Value.Date >= startDate && h.NgayThanhToanBan.Value.Date <= endDate)
                .GroupBy(h => h.NgayThanhToanBan.Value.Date)
                .Select(g => new SalesData { Date = g.Key, TotalSalesBan = 0, TotalSalesMay = g.Sum(h => h.TongTien) })
                .ToList();

            var combinedSales = salesBan
                .Concat(salesMay)
                .GroupBy(s => s.Date)
                .Select(g => new SalesData
                {
                    Date = g.Key,
                    TotalSalesBan = g.Sum(s => s.TotalSalesBan),
                    TotalSalesMay = g.Sum(s => s.TotalSalesMay)
                })
                .OrderBy(s => s.Date)
                .ToList();

            return Json(combinedSales);
        }

        public class SalesData
        {
            public DateTime Date { get; set; }
            public decimal? TotalSalesBan { get; set; }
            public decimal? TotalSalesMay { get; set; }
        }
        public IActionResult Index()
        {
            return View();
        }
       
    }
}
