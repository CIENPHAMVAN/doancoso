﻿using Azure.Core;
using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DOANCOSO.Controllers
{

    [RequireLogin]
    public class DichVuController : Controller
    {
        private readonly QuanlydichvuContext context;
        public DichVuController(QuanlydichvuContext quanlydichvuContext)
        {
            context= quanlydichvuContext;
        }    
        public async Task<IActionResult> Index()
        {
            // Lấy danh sách các loại dịch vụ từ cơ sở dữ liệu
            var loaiDichVuList = await context.LoaiDichVus.ToListAsync();
            
            var dichvu = await context.DichVus.Include(p => p.MaLoaiDichVuNavigation).ToListAsync();
            // Tạo SelectList từ danh sách loại dịch vụ để sử dụng trong dropdown
            SelectList loaiDichVuSelectList = new SelectList(loaiDichVuList, "MaLoaiDichVu", "TenLoaiDv");

            // Truyền SelectList đến view thông qua ViewBag hoặc Model (tuỳ theo cách bạn thích)
            ViewBag.dichvu= dichvu;
            ViewBag.LoaiDichVuSelectList = loaiDichVuSelectList;
            ViewBag.LoaiDichVuList = loaiDichVuList;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ThemTypeDichVu([FromBody] LoaiDichVu loaiDichVu)
        {
            if (ModelState.IsValid)
            {
                // Kiểm tra xem TenLoaiDV đã tồn tại trong cơ sở dữ liệu hay chưa
                bool tenLoaiDichVuExists = await context.LoaiDichVus.AnyAsync(l => l.TenLoaiDv == loaiDichVu.TenLoaiDv);
                if (tenLoaiDichVuExists)
                {
                    return BadRequest("Tên loại dịch vụ đã tồn tại trong cơ sở dữ liệu.");
                }
                context.LoaiDichVus.Add(loaiDichVu);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> XoaTypeDichVu(int maLoaiDichVu)
        {
           
            var loaiDichVu = await context.LoaiDichVus.FindAsync(maLoaiDichVu);
            if (loaiDichVu != null)
            {
                // Kiểm tra xem loại dịch vụ có chứa dịch vụ nào không
                bool hasServices = await context.DichVus.AnyAsync(dv => dv.MaLoaiDichVu == maLoaiDichVu);
                if (hasServices)
                {
                    return BadRequest("Loại Dịch Vụ này Có Dịch Vụ trong đó, vui lòng xóa hết Dịch Vụ có trong Loại Dịch Vụ");
                }
                context.LoaiDichVus.Remove(loaiDichVu);
                await context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }


        [HttpPost]
        public async Task<IActionResult> SuaLoaiDichVu(string tenLoaiDv, string tenLoaiDvMoi)
        {
            try
            {
                // Tìm loại dịch vụ theo tên cũ
                var loaiDichVu = await context.LoaiDichVus.FirstOrDefaultAsync(lv => lv.TenLoaiDv == tenLoaiDv);

                if (loaiDichVu == null)
                {
                    return Json(new { success = false, message = "Không tìm thấy loại dịch vụ" });
                }

                // Cập nhật tên loại dịch vụ mới
                loaiDichVu.TenLoaiDv = tenLoaiDvMoi;

                // Lưu thay đổi vào cơ sở dữ liệu
                await context.SaveChangesAsync();

                return Json(new { success = true, message = "Cập nhật loại dịch vụ thành công" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Đã xảy ra lỗi: " + ex.Message });
            }
        }

        [HttpPost]
        [Route("/api/dichvu/adddichvu")]
        public async Task<IActionResult> AddDichVu (DichVu dichVu)
        {

            if (string.IsNullOrEmpty(dichVu.TenDichVu))
            {
                ModelState.AddModelError("TenDichVu", "Tên Dịch Vụ Bắt Buộc Phải Nhập.");               
            } 
            if (dichVu.MaLoaiDichVu == null)
            {
                ModelState.AddModelError("MaLoaiDichVu", "Loại Dịch Vụ Bắt Bược Phải Nhập");
            }   
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            dichVu.MaLoaiDichVuNavigation = null;
            context.DichVus.Add(dichVu);
            await context.SaveChangesAsync();
            return Ok(new { success = true, message = "Thêm Dịch Vụ Thành Công" });
        }

        [HttpGet("/api/DichVu/{id}/Edit")]
        public async Task<IActionResult> GetDichVu(int id)
        {
            var dichvu = await context.DichVus.FindAsync(id);
            var loaiDichVus = await context.LoaiDichVus.ToListAsync();

            if (dichvu == null)
            {
                return NotFound();
            }

            var data = new
            {
                DichVu = dichvu,
                LoaiDichVu = loaiDichVus
            };

            return Ok(data);
        }

        // Xử lý cập nhật dịch vụ
        [HttpPut("/api/dichvu/{id}/Edit")]
        public async Task<IActionResult> UpdateDichVu(int id, [FromBody] DichVu dichVu)
        {
            if (id != dichVu.MaDichVu)
            {
                return BadRequest();
            }

            var existingDichVu = await context.DichVus.FindAsync(id);

            if (existingDichVu == null)
            {
                return NotFound();
            }
            
            existingDichVu.TenDichVu = dichVu.TenDichVu;
            existingDichVu.GiaDichVu = dichVu.GiaDichVu;
            existingDichVu.MaLoaiDichVu = dichVu.MaLoaiDichVu;
            // Cập nhật các trường khác tương tự nếu cần

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DichVuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        private bool DichVuExists(int id)
        {
            return context.DichVus.Any(e => e.MaDichVu == id);
        }


        [HttpDelete("/api/service/delete-service/{id}")]
        public async Task<IActionResult> DeleteDichVu (int id)
        {
            var dichvu = await context.DichVus.FindAsync(id);
            if (dichvu == null)
            {
                return NotFound("Không tìm thấy dịch vụ để xóa");
            }
            try
            {
                context.DichVus.Remove(dichvu);
                await context.SaveChangesAsync();
                return NoContent(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Lỗi khi xóa dịch vụ với ID {id}: {ex.Message}");
                return BadRequest($"Đã xảy ra lỗi khi xóa bàn: {ex.Message}");
            }
        }
    }
}
