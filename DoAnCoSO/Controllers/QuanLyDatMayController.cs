﻿using DOANCOSO.Models;
using DOANCOSO.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DOANCOSO.Controllers
{
    [RequireLogin]
    public class QuanLyDatMayController : Controller
    {
        private readonly QuanlydichvuContext context;

        public QuanLyDatMayController(QuanlydichvuContext quanlydichvuContext)
        {
            context = quanlydichvuContext;
        }
        public async Task<IActionResult> Index()
        {
            var listBooking = await context.BookingMays.Include(b => b.MaMayTinhNavigation).Include(b => b.MaTrangThaiNavigation).ToListAsync();
            var listComputer = await context.MayTinhs.ToListAsync();
            ViewBag.listComputer = new SelectList(listComputer, "MaMayTinh", "TenMayTinh");

            var typeCustomer = await context.KhachHangs.ToListAsync();
            ViewBag.typeCustomer = new SelectList(typeCustomer, "MaKhachHang", "TenKhachHang");
            return View(listBooking);

        }
        [HttpPost]
        [Route("/api/add-bookingmay")]
        public async Task<IActionResult> AddBookingMay(BookingMay bookingMay)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToList();
                    return BadRequest(new { success = false, message = "Dữ liệu không hợp lệ", errors });
                }

                bookingMay.NgayDat = DateTime.Now;

                if (bookingMay.MaMayTinh == null)
                {
                    bookingMay.MaTrangThai = 1; // Trạng thái khi không có mã máy
                }
                else
                {
                    bookingMay.MaTrangThai = 2; // Trạng thái khi có mã máy
                }

                context.BookingMays.Add(bookingMay);
                await context.SaveChangesAsync();

                return Ok(new { success = true, message = "Đặt máy thành công !!!" });
            }
            catch (DbUpdateException dbEx)
            {
                var innerExceptionMessage = dbEx.InnerException?.Message ?? dbEx.Message;
                return StatusCode(500, new { success = false, message = "Có lỗi xảy ra khi đặt máy", details = innerExceptionMessage });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { success = false, message = "Có lỗi xảy ra khi đặt máy", details = ex.Message });
            }
        }

        [HttpGet]
        [Route("/api/bookingmay/{bookingId}/edit")]
        public async Task<IActionResult> GetBookingMay(int bookingId)
        {
            var bookingmay = await context.BookingMays.Include(b => b.MaMayTinhNavigation).Include(b => b.MaKhachHangNavigation).FirstOrDefaultAsync(p => p.MaBookingMay == bookingId);
            var maytinhs = await context.MayTinhs.ToListAsync();

            if (bookingmay == null)
            {
                return NotFound();
            }

            var data = new
            {
                MayTinhs = maytinhs,
                BookingMay = bookingmay
            };

            return Ok(data);
        }

        [HttpDelete]
        [Route("/api/bookingmay/{bookingId}")]
        public async Task<IActionResult> DeleteBookingMay(int bookingId)
        {
            var bookingmay = await context.BookingMays.FindAsync(bookingId);
            if (bookingmay == null)
            {
                return NotFound(new { success = false, message = "Không tìm thấy đặt máy." });
            }

            context.BookingMays.Remove(bookingmay);
            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Xóa đặt máy thành công." });
        }

        [HttpPost]
        [Route("/api/bookingmay/{bookingId}/cancel")]
        public async Task<IActionResult> CancelBookingMay(int bookingId)
        {
            var bookingmay = await context.BookingMays.FindAsync(bookingId);
            if (bookingmay == null)
            {
                return NotFound(new { success = false, message = "Không tìm thấy đặt máy." });
            }

            bookingmay.MaTrangThai = 4; // Trạng thái hủy
            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Hủy đặt máy thành công." });
        }

        [HttpPost]
        [Route("/api/bookingmay/{bookingId}/arrange")]
        public async Task<IActionResult> ArrangeMayTinhWithBookingMay(int bookingId, [FromBody] UpdateMayDatRequest request)
        {
            try
            {
                var bookingmay = await context.BookingMays.FindAsync(bookingId);
                if (bookingmay == null)
                {
                    return NotFound(new { success = false, message = "Không tìm thấy đặt máy." });
                }

                if (request.Mamay != bookingmay.MaMayTinh)
                {
                    bookingmay.MaMayTinh = request.Mamay;
                    bookingmay.MaTrangThai = 2; // Trạng thái khi đã xếp máy
                }
                else if (request.NgayNhannew != bookingmay.NgayNhan)
                {
                    bookingmay.NgayNhan = request.NgayNhannew;
                }

                await context.SaveChangesAsync();

                return Ok(new { success = true, message = "Đã cập nhật đặt máy thành công." });
            }
            catch (Exception ex)
            {
                var innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                return StatusCode(500, new { success = false, message = "Đã xảy ra lỗi khi xếp máy. Chi tiết: " + ex.Message + ". Inner exception: " + innerExceptionMessage });
            }
        }

        [HttpPost]
        [Route("/api/bookingmay/{bookingId}/receive")]
        public async Task<IActionResult> ReceiveBookingMay(int bookingId, [FromBody] int mamay)
        {
            var bookingmay = await context.BookingMays.FindAsync(bookingId);
            var infomay = await context.MayTinhs.FindAsync(mamay);

            if (bookingmay == null)
            {
                return NotFound(new { success = false, message = "Không tìm thấy đặt máy." });
            }
            if (mamay == null)
            {
                return BadRequest(new { success = false, message = "Chưa có máy được xếp. Vui lòng xếp máy trước khi nhận máy!" });
            }
            if (infomay.MaTinhTrangMayTinh == 2)
            {
                return BadRequest(new { success = false, message = "Máy đang có người sử dụng vui lòng xếp máy khác" });
            }

            var addordermay = new OrderMay
            {
                MaKhachHang = bookingmay.MaKhachHang,
                MaMayTinh = mamay,
                ThoiGianBatDau = TimeOnly.FromDateTime(DateTime.Now),
                MaBookingMay = bookingId
            };

            bookingmay.MaTrangThai = 3; // Trạng thái khi nhận máy
            bookingmay.MaMayTinh = mamay;

            context.OrderMays.Add(addordermay);
            infomay.MaTinhTrangMayTinh = 2; // Trạng thái máy đang sử dụng

            await context.SaveChangesAsync();

            return Ok(new { success = true, message = "Nhận máy thành công." });
        }

        [HttpGet]
        [Route("/api/bookingmay/getlist/{maTrangThai}")]
        public async Task<IActionResult> ListBookingMayWithTrangThai(int maTrangThai)
        {
            var list = await context.BookingMays
                    .Include(p => p.MaKhachHangNavigation)
                    .Include(p => p.MaMayTinhNavigation)
                    .Include(p => p.MaTrangThaiNavigation)
                    .Where(dv => dv.MaTrangThai == maTrangThai)
                    .ToListAsync();

            return Ok(list);
        }

        public class UpdateMayDatRequest
        {
            public int Mamay { get; set; }
            public DateTime NgayNhannew { get; set; }
        }

    }
}
