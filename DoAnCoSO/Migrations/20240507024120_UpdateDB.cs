﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DOANCOSO.Migrations
{
    /// <inheritdoc />
    public partial class UpdateDB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__HoaDonBan__MaLoa__7C4F7684",
                table: "HoaDonBan");

            migrationBuilder.DropForeignKey(
                name: "FK__HoaDonMay__MaLoa__05D8E0BE",
                table: "HoaDonMay");

            migrationBuilder.DropForeignKey(
                name: "FK__OrderBan__MaLoai__151B244E",
                table: "OrderBan");

            migrationBuilder.DropForeignKey(
                name: "FK__OrderMay__MaLoai__19DFD96B",
                table: "OrderMay");

            migrationBuilder.DropIndex(
                name: "IX_OrderMay_MaLoaiMay",
                table: "OrderMay");

            migrationBuilder.DropIndex(
                name: "IX_OrderBan_MaLoaiBan",
                table: "OrderBan");

            migrationBuilder.DropColumn(
                name: "MaLoaiMay",
                table: "OrderMay");

            migrationBuilder.DropColumn(
                name: "MaLoaiBan",
                table: "OrderBan");

            migrationBuilder.RenameColumn(
                name: "MaLoaiMay",
                table: "HoaDonMay",
                newName: "MaOrderMay");

            migrationBuilder.RenameIndex(
                name: "IX_HoaDonMay_MaLoaiMay",
                table: "HoaDonMay",
                newName: "IX_HoaDonMay_MaOrderMay");

            migrationBuilder.RenameColumn(
                name: "MaLoaiBan",
                table: "HoaDonBan",
                newName: "MaOrderBan");

            migrationBuilder.RenameIndex(
                name: "IX_HoaDonBan_MaLoaiBan",
                table: "HoaDonBan",
                newName: "IX_HoaDonBan_MaOrderBan");

            migrationBuilder.AddColumn<int>(
                name: "MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan",
                column: "MaLoaiBanNavigationMaLoaiBan");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDonBan_LoaiBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan",
                column: "MaLoaiBanNavigationMaLoaiBan",
                principalTable: "LoaiBan",
                principalColumn: "MaLoaiBan");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDonBan_OrderBan",
                table: "HoaDonBan",
                column: "MaOrderBan",
                principalTable: "OrderBan",
                principalColumn: "MaOrderBan");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDonMay_OrderMay",
                table: "HoaDonMay",
                column: "MaOrderMay",
                principalTable: "OrderMay",
                principalColumn: "MaOrderMay");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HoaDonBan_LoaiBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan");

            migrationBuilder.DropForeignKey(
                name: "FK_HoaDonBan_OrderBan",
                table: "HoaDonBan");

            migrationBuilder.DropForeignKey(
                name: "FK_HoaDonMay_OrderMay",
                table: "HoaDonMay");

            migrationBuilder.DropIndex(
                name: "IX_HoaDonBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan");

            migrationBuilder.DropColumn(
                name: "MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan");

            migrationBuilder.RenameColumn(
                name: "MaOrderMay",
                table: "HoaDonMay",
                newName: "MaLoaiMay");

            migrationBuilder.RenameIndex(
                name: "IX_HoaDonMay_MaOrderMay",
                table: "HoaDonMay",
                newName: "IX_HoaDonMay_MaLoaiMay");

            migrationBuilder.RenameColumn(
                name: "MaOrderBan",
                table: "HoaDonBan",
                newName: "MaLoaiBan");

            migrationBuilder.RenameIndex(
                name: "IX_HoaDonBan_MaOrderBan",
                table: "HoaDonBan",
                newName: "IX_HoaDonBan_MaLoaiBan");

            migrationBuilder.AddColumn<int>(
                name: "MaLoaiMay",
                table: "OrderMay",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaLoaiBan",
                table: "OrderBan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderMay_MaLoaiMay",
                table: "OrderMay",
                column: "MaLoaiMay");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBan_MaLoaiBan",
                table: "OrderBan",
                column: "MaLoaiBan");

            migrationBuilder.AddForeignKey(
                name: "FK__HoaDonBan__MaLoa__7C4F7684",
                table: "HoaDonBan",
                column: "MaLoaiBan",
                principalTable: "LoaiBan",
                principalColumn: "MaLoaiBan");

            migrationBuilder.AddForeignKey(
                name: "FK__HoaDonMay__MaLoa__05D8E0BE",
                table: "HoaDonMay",
                column: "MaLoaiMay",
                principalTable: "LoaiMay",
                principalColumn: "MaLoaiMay");

            migrationBuilder.AddForeignKey(
                name: "FK__OrderBan__MaLoai__151B244E",
                table: "OrderBan",
                column: "MaLoaiBan",
                principalTable: "LoaiBan",
                principalColumn: "MaLoaiBan");

            migrationBuilder.AddForeignKey(
                name: "FK__OrderMay__MaLoai__19DFD96B",
                table: "OrderMay",
                column: "MaLoaiMay",
                principalTable: "LoaiMay",
                principalColumn: "MaLoaiMay");
        }
    }
}
