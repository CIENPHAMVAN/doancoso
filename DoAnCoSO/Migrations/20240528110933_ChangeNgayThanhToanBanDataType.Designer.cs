﻿// <auto-generated />
using System;
using DOANCOSO.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace DOANCOSO.Migrations
{
    [DbContext(typeof(QuanlydichvuContext))]
    [Migration("20240528110933_ChangeNgayThanhToanBanDataType")]
    partial class ChangeNgayThanhToanBanDataType
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("DOANCOSO.Models.BanBilliard", b =>
                {
                    b.Property<int>("MaBan")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaBan"));

                    b.Property<int?>("MaLoaiBan")
                        .HasColumnType("int");

                    b.Property<int?>("MaTinhTrangBan")
                        .HasColumnType("int");

                    b.Property<string>("TenBan")
                        .IsRequired()
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaBan")
                        .HasName("PK__BanBilli__3520ED6C155B58BD");

                    b.HasIndex("MaLoaiBan");

                    b.HasIndex("MaTinhTrangBan");

                    b.ToTable("BanBilliards");
                });

            modelBuilder.Entity("DOANCOSO.Models.BookingBan", b =>
                {
                    b.Property<int>("MaBookingBan")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaBookingBan"));

                    b.Property<TimeOnly?>("GioNhan")
                        .HasColumnType("time");

                    b.Property<int?>("MaBan")
                        .HasColumnType("int");

                    b.Property<int?>("MaKhachHang")
                        .HasColumnType("int");

                    b.Property<int?>("MaTrangThai")
                        .HasColumnType("int");

                    b.Property<DateTime?>("NgayDat")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("NgayNhan")
                        .HasColumnType("datetime");

                    b.HasKey("MaBookingBan");

                    b.HasIndex("MaBan");

                    b.HasIndex("MaKhachHang");

                    b.HasIndex("MaTrangThai");

                    b.ToTable("BookingBan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.BookingMay", b =>
                {
                    b.Property<int>("MaBookingMay")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaBookingMay"));

                    b.Property<TimeOnly?>("GioNhan")
                        .HasColumnType("time");

                    b.Property<int?>("MaKhachHang")
                        .HasColumnType("int");

                    b.Property<int?>("MaMayTinh")
                        .HasColumnType("int");

                    b.Property<int?>("MaTrangThai")
                        .HasColumnType("int");

                    b.Property<DateTime?>("NgayDat")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("NgayNhan")
                        .HasColumnType("datetime");

                    b.HasKey("MaBookingMay");

                    b.HasIndex("MaKhachHang");

                    b.HasIndex("MaMayTinh");

                    b.HasIndex("MaTrangThai");

                    b.ToTable("BookingMay", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.DichVu", b =>
                {
                    b.Property<int>("MaDichVu")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaDichVu"));

                    b.Property<decimal>("GiaDichVu")
                        .HasColumnType("decimal(18, 0)");

                    b.Property<int?>("MaLoaiDichVu")
                        .HasColumnType("int");

                    b.Property<string>("TenDichVu")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("MaDichVu");

                    b.HasIndex("MaLoaiDichVu");

                    b.ToTable("DichVu", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.HoaDonBan", b =>
                {
                    b.Property<int>("MaHoaDonBan")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaHoaDonBan"));

                    b.Property<int?>("MaKhachHang")
                        .HasColumnType("int");

                    b.Property<int?>("MaOrderBan")
                        .HasColumnType("int");

                    b.Property<int?>("MaPhuongThuc")
                        .HasColumnType("int");

                    b.Property<DateTime?>("NgayThanhToanBan")
                        .HasColumnType("datetime2");

                    b.Property<decimal?>("TongTien")
                        .HasColumnType("decimal(19, 2)");

                    b.HasKey("MaHoaDonBan")
                        .HasName("PK__HoaDonBa__6A50CA8A2BAF87C5");

                    b.HasIndex("MaKhachHang");

                    b.HasIndex("MaOrderBan");

                    b.HasIndex("MaPhuongThuc");

                    b.ToTable("HoaDonBan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.HoaDonMay", b =>
                {
                    b.Property<int>("MaHoaDonMay")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaHoaDonMay"));

                    b.Property<int?>("MaKhachHang")
                        .HasColumnType("int");

                    b.Property<int?>("MaOrderMay")
                        .HasColumnType("int");

                    b.Property<int?>("MaPhuongThuc")
                        .HasColumnType("int");

                    b.Property<DateTime?>("NgayThanhToanBan")
                        .HasColumnType("datetime2");

                    b.Property<decimal?>("TongTien")
                        .HasColumnType("decimal(19, 2)");

                    b.HasKey("MaHoaDonMay")
                        .HasName("PK__HoaDonMa__3B9E10360F209BA3");

                    b.HasIndex("MaKhachHang");

                    b.HasIndex("MaOrderMay");

                    b.HasIndex("MaPhuongThuc");

                    b.ToTable("HoaDonMay", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.KhachHang", b =>
                {
                    b.Property<int>("MaKhachHang")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaKhachHang"));

                    b.Property<string>("DiaChi")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.Property<int?>("MaLoaiKhachHang")
                        .HasColumnType("int");

                    b.Property<string>("SoDienThoai")
                        .HasMaxLength(20)
                        .IsUnicode(false)
                        .HasColumnType("varchar(20)");

                    b.Property<string>("TenKhachHang")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaKhachHang")
                        .HasName("PK__KhachHan__88D2F0E56A3B0694");

                    b.HasIndex("MaLoaiKhachHang");

                    b.ToTable("KhachHang", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiBan", b =>
                {
                    b.Property<int>("MaLoaiBan")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaLoaiBan"));

                    b.Property<decimal?>("GiaTheoLoaiBan")
                        .HasColumnType("decimal(19, 2)");

                    b.Property<string>("TenLoaiBan")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaLoaiBan")
                        .HasName("PK__LoaiBan__96758ECA2BAAD9A3");

                    b.ToTable("LoaiBan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiDichVu", b =>
                {
                    b.Property<int>("MaLoaiDichVu")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaLoaiDichVu"));

                    b.Property<string>("TenLoaiDv")
                        .IsRequired()
                        .HasMaxLength(50)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(50)")
                        .HasColumnName("TenLoaiDV");

                    b.HasKey("MaLoaiDichVu");

                    b.ToTable("LoaiDichVu", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiKhachHang", b =>
                {
                    b.Property<int>("MaLoaiKhachHang")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaLoaiKhachHang"));

                    b.Property<string>("TenLoaiKhachHang")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaLoaiKhachHang")
                        .HasName("PK__LoaiKhac__6975E7AEF86CB669");

                    b.ToTable("LoaiKhachHang", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiMay", b =>
                {
                    b.Property<int>("MaLoaiMay")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaLoaiMay"));

                    b.Property<decimal?>("GiaTheoLoaiMay")
                        .HasColumnType("decimal(19, 2)");

                    b.Property<string>("TenLoaiMay")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaLoaiMay")
                        .HasName("PK__LoaiMay__612CE8E754403DDA");

                    b.ToTable("LoaiMay", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.MayTinh", b =>
                {
                    b.Property<int>("MaMayTinh")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaMayTinh"));

                    b.Property<int?>("MaLoaiMay")
                        .HasColumnType("int");

                    b.Property<int?>("MaTinhTrangMayTinh")
                        .HasColumnType("int");

                    b.Property<string>("TenMayTinh")
                        .IsRequired()
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaMayTinh")
                        .HasName("PK__MayTinh__AC328530C1CBC0D2");

                    b.HasIndex("MaLoaiMay");

                    b.HasIndex("MaTinhTrangMayTinh");

                    b.ToTable("MayTinh", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.OrderBan", b =>
                {
                    b.Property<int>("MaOrderBan")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaOrderBan"));

                    b.Property<int?>("MaBan")
                        .HasColumnType("int");

                    b.Property<int?>("MaBookingBan")
                        .HasColumnType("int");

                    b.Property<int?>("MaKhachHang")
                        .HasColumnType("int");

                    b.Property<float?>("SoGio")
                        .HasColumnType("real");

                    b.Property<TimeOnly?>("ThoiGianBatDau")
                        .HasColumnType("time");

                    b.Property<TimeOnly?>("ThoiGianKetThuc")
                        .HasColumnType("time");

                    b.Property<decimal?>("TienBan")
                        .HasColumnType("decimal(18, 0)");

                    b.HasKey("MaOrderBan")
                        .HasName("PK__OrderBan__50559EF7D0B417B8");

                    b.HasIndex("MaBan");

                    b.HasIndex("MaBookingBan");

                    b.HasIndex("MaKhachHang");

                    b.ToTable("OrderBan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.OrderMay", b =>
                {
                    b.Property<int>("MaOrderMay")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaOrderMay"));

                    b.Property<int?>("MaBookingMay")
                        .HasColumnType("int");

                    b.Property<int?>("MaKhachHang")
                        .HasColumnType("int");

                    b.Property<int?>("MaMayTinh")
                        .HasColumnType("int");

                    b.Property<float?>("SoGio")
                        .HasColumnType("real");

                    b.Property<TimeOnly?>("ThoiGianBatDau")
                        .HasColumnType("time");

                    b.Property<TimeOnly?>("ThoiGianKetThuc")
                        .HasColumnType("time");

                    b.Property<decimal?>("TienMay")
                        .HasColumnType("decimal(18, 0)");

                    b.HasKey("MaOrderMay")
                        .HasName("PK__OrderMay__F602CDA1E020FE24");

                    b.HasIndex("MaBookingMay");

                    b.HasIndex("MaKhachHang");

                    b.HasIndex("MaMayTinh");

                    b.ToTable("OrderMay", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.PhuongThucThanhToan", b =>
                {
                    b.Property<int>("MaPhuongThuc")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaPhuongThuc"));

                    b.Property<string>("TenPhuongThucThanhToan")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaPhuongThuc")
                        .HasName("PK__PhuongTh__35F7404EA6E0B906");

                    b.ToTable("PhuongThucThanhToan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.QuanLy", b =>
                {
                    b.Property<int>("MaQuanLy")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaQuanLy"));

                    b.Property<string>("DiaChi")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.Property<string>("Mk")
                        .HasMaxLength(10)
                        .IsUnicode(true)
                        .HasColumnType("nchar(10)")
                        .HasColumnName("MK")
                        .IsFixedLength();

                    b.Property<string>("SoDienThoai")
                        .HasMaxLength(20)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("TenQuanLy")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.Property<string>("Tk")
                        .HasMaxLength(10)
                        .IsUnicode(true)
                        .HasColumnType("nchar(10)")
                        .HasColumnName("TK")
                        .IsFixedLength();

                    b.HasKey("MaQuanLy")
                        .HasName("PK__QuanLy__2AB9EAF8B14AFD30");

                    b.ToTable("QuanLy", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.SuDungDichVuBan", b =>
                {
                    b.Property<int>("MaSd")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("MaSD");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaSd"));

                    b.Property<int?>("MaDichVu")
                        .HasColumnType("int");

                    b.Property<int?>("MaOrderBan")
                        .HasColumnType("int");

                    b.Property<int?>("SoLuong")
                        .HasColumnType("int");

                    b.Property<decimal?>("ThanhTien")
                        .HasColumnType("decimal(18, 0)");

                    b.HasKey("MaSd")
                        .HasName("PK_SuDungDichVu");

                    b.HasIndex("MaDichVu");

                    b.HasIndex("MaOrderBan");

                    b.ToTable("SuDungDichVuBan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.SuDungDichVuMay", b =>
                {
                    b.Property<int>("MaSd")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("MaSD");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaSd"));

                    b.Property<int?>("MaDichVu")
                        .HasColumnType("int");

                    b.Property<int?>("MaOrderMay")
                        .HasColumnType("int");

                    b.Property<int?>("SoLuong")
                        .HasColumnType("int");

                    b.Property<decimal?>("ThanhTien")
                        .HasColumnType("decimal(18, 0)");

                    b.HasKey("MaSd");

                    b.HasIndex("MaDichVu");

                    b.HasIndex("MaOrderMay");

                    b.ToTable("SuDungDichVuMay", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.TinhTrangBan", b =>
                {
                    b.Property<int>("MaTinhTrangBan")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaTinhTrangBan"));

                    b.Property<string>("TenTinhTrangBan")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaTinhTrangBan")
                        .HasName("PK__TinhTran__DA3AC4E7C6BF0DC6");

                    b.ToTable("TinhTrangBan", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.TinhTrangMayTinh", b =>
                {
                    b.Property<int>("MaTinhTrangMayTinh")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaTinhTrangMayTinh"));

                    b.Property<string>("TenTinhTrangMayTinh")
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("MaTinhTrangMayTinh")
                        .HasName("PK__TinhTran__94F46D445BC771EC");

                    b.ToTable("TinhTrangMayTinh", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.TrangThaiDat", b =>
                {
                    b.Property<int>("MaTrangThai")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MaTrangThai"));

                    b.Property<string>("KieuTrangThai")
                        .HasMaxLength(50)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("MaTrangThai");

                    b.ToTable("TrangThaiDat", (string)null);
                });

            modelBuilder.Entity("DOANCOSO.Models.BanBilliard", b =>
                {
                    b.HasOne("DOANCOSO.Models.LoaiBan", "MaLoaiBanNavigation")
                        .WithMany("BanBilliards")
                        .HasForeignKey("MaLoaiBan")
                        .HasConstraintName("FK__BanBillia__MaLoa__412EB0B6");

                    b.HasOne("DOANCOSO.Models.TinhTrangBan", "MaTinhTrangBanNavigation")
                        .WithMany("BanBilliards")
                        .HasForeignKey("MaTinhTrangBan")
                        .HasConstraintName("FK__BanBillia__MaTin__403A8C7D");

                    b.Navigation("MaLoaiBanNavigation");

                    b.Navigation("MaTinhTrangBanNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.BookingBan", b =>
                {
                    b.HasOne("DOANCOSO.Models.BanBilliard", "MaBanNavigation")
                        .WithMany("BookingBans")
                        .HasForeignKey("MaBan")
                        .HasConstraintName("FK_BookingBan_BanBilliards");

                    b.HasOne("DOANCOSO.Models.KhachHang", "MaKhachHangNavigation")
                        .WithMany("BookingBans")
                        .HasForeignKey("MaKhachHang")
                        .HasConstraintName("FK_BookingBan_KhachHang");

                    b.HasOne("DOANCOSO.Models.TrangThaiDat", "MaTrangThaiNavigation")
                        .WithMany("BookingBans")
                        .HasForeignKey("MaTrangThai")
                        .HasConstraintName("FK_BookingBan_TrangThaiDat");

                    b.Navigation("MaBanNavigation");

                    b.Navigation("MaKhachHangNavigation");

                    b.Navigation("MaTrangThaiNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.BookingMay", b =>
                {
                    b.HasOne("DOANCOSO.Models.KhachHang", "MaKhachHangNavigation")
                        .WithMany("BookingMays")
                        .HasForeignKey("MaKhachHang")
                        .HasConstraintName("FK_BookingMay_KhachHang");

                    b.HasOne("DOANCOSO.Models.MayTinh", "MaMayTinhNavigation")
                        .WithMany("BookingMays")
                        .HasForeignKey("MaMayTinh")
                        .HasConstraintName("FK_BookingMay_MayTinh");

                    b.HasOne("DOANCOSO.Models.TrangThaiDat", "MaTrangThaiNavigation")
                        .WithMany("BookingMays")
                        .HasForeignKey("MaTrangThai")
                        .HasConstraintName("FK_BookingMay_TrangThaiDat");

                    b.Navigation("MaKhachHangNavigation");

                    b.Navigation("MaMayTinhNavigation");

                    b.Navigation("MaTrangThaiNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.DichVu", b =>
                {
                    b.HasOne("DOANCOSO.Models.LoaiDichVu", "MaLoaiDichVuNavigation")
                        .WithMany("DichVus")
                        .HasForeignKey("MaLoaiDichVu")
                        .HasConstraintName("FK_DichVu_LoaiDichVu");

                    b.Navigation("MaLoaiDichVuNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.HoaDonBan", b =>
                {
                    b.HasOne("DOANCOSO.Models.KhachHang", "MaKhachHangNavigation")
                        .WithMany("HoaDonBans")
                        .HasForeignKey("MaKhachHang")
                        .HasConstraintName("FK__HoaDonBan__MaKha__7A672E12");

                    b.HasOne("DOANCOSO.Models.OrderBan", "MaOrderBanNavigation")
                        .WithMany("HoaDonBans")
                        .HasForeignKey("MaOrderBan")
                        .HasConstraintName("FK_HoaDonBan_OrderBan");

                    b.HasOne("DOANCOSO.Models.PhuongThucThanhToan", "MaPhuongThucNavigation")
                        .WithMany("HoaDonBans")
                        .HasForeignKey("MaPhuongThuc")
                        .HasConstraintName("FK__HoaDonBan__MaPhu__7B5B524B");

                    b.Navigation("MaKhachHangNavigation");

                    b.Navigation("MaOrderBanNavigation");

                    b.Navigation("MaPhuongThucNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.HoaDonMay", b =>
                {
                    b.HasOne("DOANCOSO.Models.KhachHang", "MaKhachHangNavigation")
                        .WithMany("HoaDonMays")
                        .HasForeignKey("MaKhachHang")
                        .HasConstraintName("FK__HoaDonMay__MaKha__03F0984C");

                    b.HasOne("DOANCOSO.Models.OrderMay", "MaOrderMayNavigation")
                        .WithMany("HoaDonMays")
                        .HasForeignKey("MaOrderMay")
                        .HasConstraintName("FK_HoaDonMay_OrderMay");

                    b.HasOne("DOANCOSO.Models.PhuongThucThanhToan", "MaPhuongThucNavigation")
                        .WithMany("HoaDonMays")
                        .HasForeignKey("MaPhuongThuc")
                        .HasConstraintName("FK__HoaDonMay__MaPhu__04E4BC85");

                    b.Navigation("MaKhachHangNavigation");

                    b.Navigation("MaOrderMayNavigation");

                    b.Navigation("MaPhuongThucNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.KhachHang", b =>
                {
                    b.HasOne("DOANCOSO.Models.LoaiKhachHang", "MaLoaiKhachHangNavigation")
                        .WithMany("KhachHangs")
                        .HasForeignKey("MaLoaiKhachHang")
                        .HasConstraintName("FK__KhachHang__MaLoa__398D8EEE");

                    b.Navigation("MaLoaiKhachHangNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.MayTinh", b =>
                {
                    b.HasOne("DOANCOSO.Models.LoaiMay", "MaLoaiMayNavigation")
                        .WithMany("MayTinhs")
                        .HasForeignKey("MaLoaiMay")
                        .HasConstraintName("FK__MayTinh__MaLoaiM__6EF57B66");

                    b.HasOne("DOANCOSO.Models.TinhTrangMayTinh", "MaTinhTrangMayTinhNavigation")
                        .WithMany("MayTinhs")
                        .HasForeignKey("MaTinhTrangMayTinh")
                        .HasConstraintName("FK__MayTinh__MaTinhT__6E01572D");

                    b.Navigation("MaLoaiMayNavigation");

                    b.Navigation("MaTinhTrangMayTinhNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.OrderBan", b =>
                {
                    b.HasOne("DOANCOSO.Models.BanBilliard", "MaBanNavigation")
                        .WithMany("OrderBans")
                        .HasForeignKey("MaBan")
                        .HasConstraintName("FK__OrderBan__MaBan__160F4887");

                    b.HasOne("DOANCOSO.Models.BookingBan", "MaBookingBanNavigation")
                        .WithMany("OrderBans")
                        .HasForeignKey("MaBookingBan")
                        .HasConstraintName("FK_OrderBan_BookingBan");

                    b.HasOne("DOANCOSO.Models.KhachHang", "MaKhachHangNavigation")
                        .WithMany("OrderBans")
                        .HasForeignKey("MaKhachHang")
                        .HasConstraintName("FK__OrderBan__MaKhac__14270015");

                    b.Navigation("MaBanNavigation");

                    b.Navigation("MaBookingBanNavigation");

                    b.Navigation("MaKhachHangNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.OrderMay", b =>
                {
                    b.HasOne("DOANCOSO.Models.BookingMay", "MaBookingMayNavigation")
                        .WithMany("OrderMays")
                        .HasForeignKey("MaBookingMay")
                        .HasConstraintName("FK_OrderMay_BookingMay");

                    b.HasOne("DOANCOSO.Models.KhachHang", "MaKhachHangNavigation")
                        .WithMany("OrderMays")
                        .HasForeignKey("MaKhachHang")
                        .HasConstraintName("FK__OrderMay__MaKhac__18EBB532");

                    b.HasOne("DOANCOSO.Models.MayTinh", "MaMayTinhNavigation")
                        .WithMany("OrderMays")
                        .HasForeignKey("MaMayTinh")
                        .HasConstraintName("FK__OrderMay__MaMayT__1AD3FDA4");

                    b.Navigation("MaBookingMayNavigation");

                    b.Navigation("MaKhachHangNavigation");

                    b.Navigation("MaMayTinhNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.SuDungDichVuBan", b =>
                {
                    b.HasOne("DOANCOSO.Models.DichVu", "MaDichVuNavigation")
                        .WithMany("SuDungDichVuBans")
                        .HasForeignKey("MaDichVu")
                        .HasConstraintName("FK_SuDungDichVu_DichVu");

                    b.HasOne("DOANCOSO.Models.OrderBan", "MaOrderBanNavigation")
                        .WithMany("SuDungDichVuBans")
                        .HasForeignKey("MaOrderBan")
                        .HasConstraintName("FK_SuDungDichVu_OrderBan");

                    b.Navigation("MaDichVuNavigation");

                    b.Navigation("MaOrderBanNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.SuDungDichVuMay", b =>
                {
                    b.HasOne("DOANCOSO.Models.DichVu", "MaDichVuNavigation")
                        .WithMany("SuDungDichVuMays")
                        .HasForeignKey("MaDichVu")
                        .HasConstraintName("FK_SuDungDichVuMay_DichVu");

                    b.HasOne("DOANCOSO.Models.OrderMay", "MaOrderMayNavigation")
                        .WithMany("SuDungDichVuMays")
                        .HasForeignKey("MaOrderMay")
                        .HasConstraintName("FK_SuDungDichVuMay_OrderMay");

                    b.Navigation("MaDichVuNavigation");

                    b.Navigation("MaOrderMayNavigation");
                });

            modelBuilder.Entity("DOANCOSO.Models.BanBilliard", b =>
                {
                    b.Navigation("BookingBans");

                    b.Navigation("OrderBans");
                });

            modelBuilder.Entity("DOANCOSO.Models.BookingBan", b =>
                {
                    b.Navigation("OrderBans");
                });

            modelBuilder.Entity("DOANCOSO.Models.BookingMay", b =>
                {
                    b.Navigation("OrderMays");
                });

            modelBuilder.Entity("DOANCOSO.Models.DichVu", b =>
                {
                    b.Navigation("SuDungDichVuBans");

                    b.Navigation("SuDungDichVuMays");
                });

            modelBuilder.Entity("DOANCOSO.Models.KhachHang", b =>
                {
                    b.Navigation("BookingBans");

                    b.Navigation("BookingMays");

                    b.Navigation("HoaDonBans");

                    b.Navigation("HoaDonMays");

                    b.Navigation("OrderBans");

                    b.Navigation("OrderMays");
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiBan", b =>
                {
                    b.Navigation("BanBilliards");
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiDichVu", b =>
                {
                    b.Navigation("DichVus");
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiKhachHang", b =>
                {
                    b.Navigation("KhachHangs");
                });

            modelBuilder.Entity("DOANCOSO.Models.LoaiMay", b =>
                {
                    b.Navigation("MayTinhs");
                });

            modelBuilder.Entity("DOANCOSO.Models.MayTinh", b =>
                {
                    b.Navigation("BookingMays");

                    b.Navigation("OrderMays");
                });

            modelBuilder.Entity("DOANCOSO.Models.OrderBan", b =>
                {
                    b.Navigation("HoaDonBans");

                    b.Navigation("SuDungDichVuBans");
                });

            modelBuilder.Entity("DOANCOSO.Models.OrderMay", b =>
                {
                    b.Navigation("HoaDonMays");

                    b.Navigation("SuDungDichVuMays");
                });

            modelBuilder.Entity("DOANCOSO.Models.PhuongThucThanhToan", b =>
                {
                    b.Navigation("HoaDonBans");

                    b.Navigation("HoaDonMays");
                });

            modelBuilder.Entity("DOANCOSO.Models.TinhTrangBan", b =>
                {
                    b.Navigation("BanBilliards");
                });

            modelBuilder.Entity("DOANCOSO.Models.TinhTrangMayTinh", b =>
                {
                    b.Navigation("MayTinhs");
                });

            modelBuilder.Entity("DOANCOSO.Models.TrangThaiDat", b =>
                {
                    b.Navigation("BookingBans");

                    b.Navigation("BookingMays");
                });
#pragma warning restore 612, 618
        }
    }
}
