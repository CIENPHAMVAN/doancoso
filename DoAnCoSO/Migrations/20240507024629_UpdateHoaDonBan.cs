﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DOANCOSO.Migrations
{
    /// <inheritdoc />
    public partial class UpdateHoaDonBan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HoaDonBan_LoaiBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan");

            migrationBuilder.DropIndex(
                name: "IX_HoaDonBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan");

            migrationBuilder.DropColumn(
                name: "MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan",
                column: "MaLoaiBanNavigationMaLoaiBan");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDonBan_LoaiBan_MaLoaiBanNavigationMaLoaiBan",
                table: "HoaDonBan",
                column: "MaLoaiBanNavigationMaLoaiBan",
                principalTable: "LoaiBan",
                principalColumn: "MaLoaiBan");
        }
    }
}
