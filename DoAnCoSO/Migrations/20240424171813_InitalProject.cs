﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DOANCOSO.Migrations
{

    public partial class InitalProject : Migration

    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoaiBan",
                columns: table => new
                {
                    MaLoaiBan = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenLoaiBan = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),

                    GiaTheoLoaiBan = table.Column<decimal>(type: "decimal(19,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__LoaiBan__96758ECA2BAAD9A3", x => x.MaLoaiBan);
                });

            migrationBuilder.CreateTable(
                name: "LoaiKhachHang",
                columns: table => new
                {
                    MaLoaiKhachHang = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenLoaiKhachHang = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__LoaiKhac__6975E7AEF86CB669", x => x.MaLoaiKhachHang);
                });

            migrationBuilder.CreateTable(
                name: "LoaiMay",
                columns: table => new
                {
                    MaLoaiMay = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GiaTheoLoaiMay = table.Column<decimal>(type: "decimal(19,2)", nullable: true),
                    TenLoaiMay = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__LoaiMay__612CE8E754403DDA", x => x.MaLoaiMay);
                });

            migrationBuilder.CreateTable(
                name: "PhuongThucThanhToan",
                columns: table => new
                {
                    MaPhuongThuc = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenPhuongThucThanhToan = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__PhuongTh__35F7404EA6E0B906", x => x.MaPhuongThuc);
                });

            migrationBuilder.CreateTable(
                name: "QuanLy",
                columns: table => new
                {
                    MaQuanLy = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenQuanLy = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    SoDienThoai = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    TK = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
                    MK = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true)

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__QuanLy__2AB9EAF8B14AFD30", x => x.MaQuanLy);
                });

            migrationBuilder.CreateTable(
                name: "TinhTrangBan",
                columns: table => new
                {
                    MaTinhTrangBan = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenTinhTrangBan = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__TinhTran__DA3AC4E7C6BF0DC6", x => x.MaTinhTrangBan);
                });

            migrationBuilder.CreateTable(
                name: "TinhTrangMayTinh",
                columns: table => new
                {
                    MaTinhTrangMayTinh = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenTinhTrangMayTinh = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__TinhTran__94F46D445BC771EC", x => x.MaTinhTrangMayTinh);
                });

            migrationBuilder.CreateTable(
                name: "KhachHang",
                columns: table => new
                {
                    MaKhachHang = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),

                    TenKhachHang = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),

                    SoDienThoai = table.Column<string>(type: "varchar(20)", unicode: false, maxLength: 20, nullable: true),
                    MaLoaiKhachHang = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__KhachHan__88D2F0E56A3B0694", x => x.MaKhachHang);
                    table.ForeignKey(
                        name: "FK__KhachHang__MaLoa__398D8EEE",
                        column: x => x.MaLoaiKhachHang,
                        principalTable: "LoaiKhachHang",
                        principalColumn: "MaLoaiKhachHang");
                });

            migrationBuilder.CreateTable(
                name: "BanBilliards",
                columns: table => new
                {
                    MaBan = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaTinhTrangBan = table.Column<int>(type: "int", nullable: true),
                    MaLoaiBan = table.Column<int>(type: "int", nullable: true),
                    TenBan = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__BanBilli__3520ED6C155B58BD", x => x.MaBan);
                    table.ForeignKey(
                        name: "FK__BanBillia__MaLoa__412EB0B6",
                        column: x => x.MaLoaiBan,
                        principalTable: "LoaiBan",
                        principalColumn: "MaLoaiBan");
                    table.ForeignKey(
                        name: "FK__BanBillia__MaTin__403A8C7D",
                        column: x => x.MaTinhTrangBan,
                        principalTable: "TinhTrangBan",
                        principalColumn: "MaTinhTrangBan");
                });

            migrationBuilder.CreateTable(
                name: "MayTinh",
                columns: table => new
                {
                    MaMayTinh = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaTinhTrangMayTinh = table.Column<int>(type: "int", nullable: true),
                    MaLoaiMay = table.Column<int>(type: "int", nullable: true),
                    TenMayTinh = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__MayTinh__AC328530C1CBC0D2", x => x.MaMayTinh);
                    table.ForeignKey(
                        name: "FK__MayTinh__MaLoaiM__6EF57B66",
                        column: x => x.MaLoaiMay,
                        principalTable: "LoaiMay",
                        principalColumn: "MaLoaiMay");
                    table.ForeignKey(
                        name: "FK__MayTinh__MaTinhT__6E01572D",
                        column: x => x.MaTinhTrangMayTinh,
                        principalTable: "TinhTrangMayTinh",
                        principalColumn: "MaTinhTrangMayTinh");
                });

            migrationBuilder.CreateTable(
                name: "HoaDonBan",
                columns: table => new
                {
                    MaHoaDonBan = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaKhachHang = table.Column<int>(type: "int", nullable: true),
                    MaPhuongThuc = table.Column<int>(type: "int", nullable: true),
                    MaLoaiBan = table.Column<int>(type: "int", nullable: true),
                    NgayThanhToanBan = table.Column<DateOnly>(type: "date", nullable: true),
                    TongTien = table.Column<decimal>(type: "decimal(19,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__HoaDonBa__6A50CA8A2BAF87C5", x => x.MaHoaDonBan);
                    table.ForeignKey(
                        name: "FK__HoaDonBan__MaKha__7A672E12",
                        column: x => x.MaKhachHang,
                        principalTable: "KhachHang",
                        principalColumn: "MaKhachHang");
                    table.ForeignKey(
                        name: "FK__HoaDonBan__MaLoa__7C4F7684",
                        column: x => x.MaLoaiBan,
                        principalTable: "LoaiBan",
                        principalColumn: "MaLoaiBan");
                    table.ForeignKey(
                        name: "FK__HoaDonBan__MaPhu__7B5B524B",
                        column: x => x.MaPhuongThuc,
                        principalTable: "PhuongThucThanhToan",
                        principalColumn: "MaPhuongThuc");
                });

            migrationBuilder.CreateTable(
                name: "HoaDonMay",
                columns: table => new
                {
                    MaHoaDonMay = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaKhachHang = table.Column<int>(type: "int", nullable: true),
                    MaPhuongThuc = table.Column<int>(type: "int", nullable: true),
                    MaLoaiMay = table.Column<int>(type: "int", nullable: true),
                    NgayThanhToanBan = table.Column<DateOnly>(type: "date", nullable: true),
                    TongTien = table.Column<decimal>(type: "decimal(19,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__HoaDonMa__3B9E10360F209BA3", x => x.MaHoaDonMay);
                    table.ForeignKey(
                        name: "FK__HoaDonMay__MaKha__03F0984C",
                        column: x => x.MaKhachHang,
                        principalTable: "KhachHang",
                        principalColumn: "MaKhachHang");
                    table.ForeignKey(
                        name: "FK__HoaDonMay__MaLoa__05D8E0BE",
                        column: x => x.MaLoaiMay,
                        principalTable: "LoaiMay",
                        principalColumn: "MaLoaiMay");
                    table.ForeignKey(
                        name: "FK__HoaDonMay__MaPhu__04E4BC85",
                        column: x => x.MaPhuongThuc,
                        principalTable: "PhuongThucThanhToan",
                        principalColumn: "MaPhuongThuc");
                });

            migrationBuilder.CreateTable(
                name: "OrderBan",
                columns: table => new
                {
                    MaOrderBan = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaBan = table.Column<int>(type: "int", nullable: true),
                    MaKhachHang = table.Column<int>(type: "int", nullable: true),
                    MaLoaiBan = table.Column<int>(type: "int", nullable: true),
                    NgayDat = table.Column<DateTime>(type: "datetime", nullable: true),
                    ThoiGianBatDau = table.Column<TimeOnly>(type: "time", nullable: true),
                    ThoiGianKetThuc = table.Column<TimeOnly>(type: "time", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__OrderBan__50559EF7D0B417B8", x => x.MaOrderBan);
                    table.ForeignKey(
                        name: "FK__OrderBan__MaBan__160F4887",
                        column: x => x.MaBan,
                        principalTable: "BanBilliards",
                        principalColumn: "MaBan");
                    table.ForeignKey(
                        name: "FK__OrderBan__MaKhac__14270015",
                        column: x => x.MaKhachHang,
                        principalTable: "KhachHang",
                        principalColumn: "MaKhachHang");
                    table.ForeignKey(
                        name: "FK__OrderBan__MaLoai__151B244E",
                        column: x => x.MaLoaiBan,
                        principalTable: "LoaiBan",
                        principalColumn: "MaLoaiBan");
                });

            migrationBuilder.CreateTable(
                name: "OrderMay",
                columns: table => new
                {
                    MaOrderMay = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaMayTinh = table.Column<int>(type: "int", nullable: true),
                    MaKhachHang = table.Column<int>(type: "int", nullable: true),
                    MaLoaiMay = table.Column<int>(type: "int", nullable: true),
                    NgayDat = table.Column<DateTime>(type: "datetime", nullable: true),
                    ThoiGianBatDau = table.Column<TimeOnly>(type: "time", nullable: true),
                    ThoiGianKetThuc = table.Column<TimeOnly>(type: "time", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__OrderMay__F602CDA1E020FE24", x => x.MaOrderMay);
                    table.ForeignKey(
                        name: "FK__OrderMay__MaKhac__18EBB532",
                        column: x => x.MaKhachHang,
                        principalTable: "KhachHang",
                        principalColumn: "MaKhachHang");
                    table.ForeignKey(
                        name: "FK__OrderMay__MaLoai__19DFD96B",
                        column: x => x.MaLoaiMay,
                        principalTable: "LoaiMay",
                        principalColumn: "MaLoaiMay");
                    table.ForeignKey(
                        name: "FK__OrderMay__MaMayT__1AD3FDA4",
                        column: x => x.MaMayTinh,
                        principalTable: "MayTinh",
                        principalColumn: "MaMayTinh");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BanBilliards_MaLoaiBan",
                table: "BanBilliards",
                column: "MaLoaiBan");

            migrationBuilder.CreateIndex(
                name: "IX_BanBilliards_MaTinhTrangBan",
                table: "BanBilliards",
                column: "MaTinhTrangBan");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonBan_MaKhachHang",
                table: "HoaDonBan",
                column: "MaKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonBan_MaLoaiBan",
                table: "HoaDonBan",
                column: "MaLoaiBan");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonBan_MaPhuongThuc",
                table: "HoaDonBan",
                column: "MaPhuongThuc");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonMay_MaKhachHang",
                table: "HoaDonMay",
                column: "MaKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonMay_MaLoaiMay",
                table: "HoaDonMay",
                column: "MaLoaiMay");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDonMay_MaPhuongThuc",
                table: "HoaDonMay",
                column: "MaPhuongThuc");

            migrationBuilder.CreateIndex(
                name: "IX_KhachHang_MaLoaiKhachHang",
                table: "KhachHang",
                column: "MaLoaiKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_MayTinh_MaLoaiMay",
                table: "MayTinh",
                column: "MaLoaiMay");

            migrationBuilder.CreateIndex(
                name: "IX_MayTinh_MaTinhTrangMayTinh",
                table: "MayTinh",
                column: "MaTinhTrangMayTinh");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBan_MaBan",
                table: "OrderBan",
                column: "MaBan");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBan_MaKhachHang",
                table: "OrderBan",
                column: "MaKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBan_MaLoaiBan",
                table: "OrderBan",
                column: "MaLoaiBan");

            migrationBuilder.CreateIndex(
                name: "IX_OrderMay_MaKhachHang",
                table: "OrderMay",
                column: "MaKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_OrderMay_MaLoaiMay",
                table: "OrderMay",
                column: "MaLoaiMay");

            migrationBuilder.CreateIndex(
                name: "IX_OrderMay_MaMayTinh",
                table: "OrderMay",
                column: "MaMayTinh");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HoaDonBan");

            migrationBuilder.DropTable(
                name: "HoaDonMay");

            migrationBuilder.DropTable(
                name: "OrderBan");

            migrationBuilder.DropTable(
                name: "OrderMay");

            migrationBuilder.DropTable(
                name: "QuanLy");

            migrationBuilder.DropTable(
                name: "PhuongThucThanhToan");

            migrationBuilder.DropTable(
                name: "BanBilliards");

            migrationBuilder.DropTable(
                name: "KhachHang");

            migrationBuilder.DropTable(
                name: "MayTinh");

            migrationBuilder.DropTable(
                name: "LoaiBan");

            migrationBuilder.DropTable(
                name: "TinhTrangBan");

            migrationBuilder.DropTable(
                name: "LoaiKhachHang");

            migrationBuilder.DropTable(
                name: "LoaiMay");

            migrationBuilder.DropTable(
                name: "TinhTrangMayTinh");
        }
    }
}
