﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DOANCOSO.Migrations
{
    /// <inheritdoc />
    public partial class UpdateDichVu : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "SoGio",
                table: "OrderMay",
                type: "real",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TienMay",
                table: "OrderMay",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "SoGio",
                table: "OrderBan",
                type: "real",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TienBan",
                table: "OrderBan",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LoaiDichVu",
                columns: table => new
                {
                    MaLoaiDichVu = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoaiDV = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiDichVu", x => x.MaLoaiDichVu);
                });

            migrationBuilder.CreateTable(
                name: "DichVu",
                columns: table => new
                {
                    MaDichVu = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenDichVu = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    GiaDichVu = table.Column<decimal>(type: "decimal(18,0)", nullable: false),
                    MaLoaiDichVu = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DichVu", x => x.MaDichVu);
                    table.ForeignKey(
                        name: "FK_DichVu_LoaiDichVu",
                        column: x => x.MaLoaiDichVu,
                        principalTable: "LoaiDichVu",
                        principalColumn: "MaLoaiDichVu");
                });

            migrationBuilder.CreateTable(
                name: "SuDungDichVuBan",
                columns: table => new
                {
                    MaSD = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    ThanhTien = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    MaDichVu = table.Column<int>(type: "int", nullable: true),
                    MaOrderBan = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuDungDichVu", x => x.MaSD);
                    table.ForeignKey(
                        name: "FK_SuDungDichVu_DichVu",
                        column: x => x.MaDichVu,
                        principalTable: "DichVu",
                        principalColumn: "MaDichVu");
                    table.ForeignKey(
                        name: "FK_SuDungDichVu_OrderBan",
                        column: x => x.MaOrderBan,
                        principalTable: "OrderBan",
                        principalColumn: "MaOrderBan");
                });

            migrationBuilder.CreateTable(
                name: "SuDungDichVuMay",
                columns: table => new
                {
                    MaSD = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    ThanhTien = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    MaDichVu = table.Column<int>(type: "int", nullable: true),
                    MaOrderMay = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuDungDichVuMay", x => x.MaSD);
                    table.ForeignKey(
                        name: "FK_SuDungDichVuMay_DichVu",
                        column: x => x.MaDichVu,
                        principalTable: "DichVu",
                        principalColumn: "MaDichVu");
                    table.ForeignKey(
                        name: "FK_SuDungDichVuMay_OrderMay",
                        column: x => x.MaOrderMay,
                        principalTable: "OrderMay",
                        principalColumn: "MaOrderMay");
                });

            migrationBuilder.CreateIndex(
                name: "IX_DichVu_MaLoaiDichVu",
                table: "DichVu",
                column: "MaLoaiDichVu");

            migrationBuilder.CreateIndex(
                name: "IX_SuDungDichVuBan_MaDichVu",
                table: "SuDungDichVuBan",
                column: "MaDichVu");

            migrationBuilder.CreateIndex(
                name: "IX_SuDungDichVuBan_MaOrderBan",
                table: "SuDungDichVuBan",
                column: "MaOrderBan");

            migrationBuilder.CreateIndex(
                name: "IX_SuDungDichVuMay_MaDichVu",
                table: "SuDungDichVuMay",
                column: "MaDichVu");

            migrationBuilder.CreateIndex(
                name: "IX_SuDungDichVuMay_MaOrderMay",
                table: "SuDungDichVuMay",
                column: "MaOrderMay");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SuDungDichVuBan");

            migrationBuilder.DropTable(
                name: "SuDungDichVuMay");

            migrationBuilder.DropTable(
                name: "DichVu");

            migrationBuilder.DropTable(
                name: "LoaiDichVu");

            migrationBuilder.DropColumn(
                name: "SoGio",
                table: "OrderMay");

            migrationBuilder.DropColumn(
                name: "TienMay",
                table: "OrderMay");

            migrationBuilder.DropColumn(
                name: "SoGio",
                table: "OrderBan");

            migrationBuilder.DropColumn(
                name: "TienBan",
                table: "OrderBan");
        }
    }
}
