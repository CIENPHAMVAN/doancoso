﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DOANCOSO.Migrations
{
    /// <inheritdoc />
    public partial class AddBookingBanandMay : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NgayDat",
                table: "OrderMay");

            migrationBuilder.DropColumn(
                name: "NgayDat",
                table: "OrderBan");

            migrationBuilder.AddColumn<int>(
                name: "MaBookingMay",
                table: "OrderMay",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaBookingBan",
                table: "OrderBan",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TenMayTinh",
                table: "MayTinh",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayThanhToanBan",
                table: "HoaDonBan",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateOnly),
                oldType: "date",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MaLoaiDichVu",
                table: "DichVu",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "TrangThaiDat",
                columns: table => new
                {
                    MaTrangThai = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KieuTrangThai = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrangThaiDat", x => x.MaTrangThai);
                });

            migrationBuilder.CreateTable(
                name: "BookingBan",
                columns: table => new
                {
                    MaBookingBan = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NgayDat = table.Column<DateTime>(type: "datetime", nullable: true),
                    NgayNhan = table.Column<DateTime>(type: "datetime", nullable: true),
                    GioNhan = table.Column<TimeOnly>(type: "time", nullable: true),
                    MaBan = table.Column<int>(type: "int", nullable: true),
                    MaKhachHang = table.Column<int>(type: "int", nullable: true),
                    MaTrangThai = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingBan", x => x.MaBookingBan);
                    table.ForeignKey(
                        name: "FK_BookingBan_BanBilliards",
                        column: x => x.MaBan,
                        principalTable: "BanBilliards",
                        principalColumn: "MaBan");
                    table.ForeignKey(
                        name: "FK_BookingBan_KhachHang",
                        column: x => x.MaKhachHang,
                        principalTable: "KhachHang",
                        principalColumn: "MaKhachHang");
                    table.ForeignKey(
                        name: "FK_BookingBan_TrangThaiDat",
                        column: x => x.MaTrangThai,
                        principalTable: "TrangThaiDat",
                        principalColumn: "MaTrangThai");
                });

            migrationBuilder.CreateTable(
                name: "BookingMay",
                columns: table => new
                {
                    MaBookingMay = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NgayDat = table.Column<DateTime>(type: "datetime", nullable: true),
                    NgayNhan = table.Column<DateTime>(type: "datetime", nullable: true),
                    GioNhan = table.Column<TimeOnly>(type: "time", nullable: true),
                    MaMayTinh = table.Column<int>(type: "int", nullable: true),
                    MaKhachHang = table.Column<int>(type: "int", nullable: true),
                    MaTrangThai = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingMay", x => x.MaBookingMay);
                    table.ForeignKey(
                        name: "FK_BookingMay_KhachHang",
                        column: x => x.MaKhachHang,
                        principalTable: "KhachHang",
                        principalColumn: "MaKhachHang");
                    table.ForeignKey(
                        name: "FK_BookingMay_MayTinh",
                        column: x => x.MaMayTinh,
                        principalTable: "MayTinh",
                        principalColumn: "MaMayTinh");
                    table.ForeignKey(
                        name: "FK_BookingMay_TrangThaiDat",
                        column: x => x.MaTrangThai,
                        principalTable: "TrangThaiDat",
                        principalColumn: "MaTrangThai");
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderMay_MaBookingMay",
                table: "OrderMay",
                column: "MaBookingMay");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBan_MaBookingBan",
                table: "OrderBan",
                column: "MaBookingBan");

            migrationBuilder.CreateIndex(
                name: "IX_BookingBan_MaBan",
                table: "BookingBan",
                column: "MaBan");

            migrationBuilder.CreateIndex(
                name: "IX_BookingBan_MaKhachHang",
                table: "BookingBan",
                column: "MaKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_BookingBan_MaTrangThai",
                table: "BookingBan",
                column: "MaTrangThai");

            migrationBuilder.CreateIndex(
                name: "IX_BookingMay_MaKhachHang",
                table: "BookingMay",
                column: "MaKhachHang");

            migrationBuilder.CreateIndex(
                name: "IX_BookingMay_MaMayTinh",
                table: "BookingMay",
                column: "MaMayTinh");

            migrationBuilder.CreateIndex(
                name: "IX_BookingMay_MaTrangThai",
                table: "BookingMay",
                column: "MaTrangThai");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderBan_BookingBan",
                table: "OrderBan",
                column: "MaBookingBan",
                principalTable: "BookingBan",
                principalColumn: "MaBookingBan");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderMay_BookingMay",
                table: "OrderMay",
                column: "MaBookingMay",
                principalTable: "BookingMay",
                principalColumn: "MaBookingMay");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderBan_BookingBan",
                table: "OrderBan");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderMay_BookingMay",
                table: "OrderMay");

            migrationBuilder.DropTable(
                name: "BookingBan");

            migrationBuilder.DropTable(
                name: "BookingMay");

            migrationBuilder.DropTable(
                name: "TrangThaiDat");

            migrationBuilder.DropIndex(
                name: "IX_OrderMay_MaBookingMay",
                table: "OrderMay");

            migrationBuilder.DropIndex(
                name: "IX_OrderBan_MaBookingBan",
                table: "OrderBan");

            migrationBuilder.DropColumn(
                name: "MaBookingMay",
                table: "OrderMay");

            migrationBuilder.DropColumn(
                name: "MaBookingBan",
                table: "OrderBan");

            migrationBuilder.AddColumn<DateTime>(
                name: "NgayDat",
                table: "OrderMay",
                type: "datetime",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NgayDat",
                table: "OrderBan",
                type: "datetime",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TenMayTinh",
                table: "MayTinh",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<DateOnly>(
                name: "NgayThanhToanBan",
                table: "HoaDonBan",
                type: "date",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MaLoaiDichVu",
                table: "DichVu",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
