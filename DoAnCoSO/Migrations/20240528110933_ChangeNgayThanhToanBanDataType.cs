﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DOANCOSO.Migrations
{
    /// <inheritdoc />
    public partial class ChangeNgayThanhToanBanDataType : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "NgayThanhToanBan",
                table: "HoaDonMay",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateOnly),
                oldType: "date",
                oldNullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateOnly>(
                name: "NgayThanhToanBan",
                table: "HoaDonMay",
                type: "date",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);
        }
    }
}
