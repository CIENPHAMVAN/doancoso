﻿namespace DOANCOSO.Models
{
    public class BookingMay
    {

        public int MaBookingMay { get; set; }

        public DateTime? NgayDat { get; set; }

        public DateTime? NgayNhan { get; set; }

        public TimeOnly? GioNhan { get; set; }

        public int? MaMayTinh { get; set; }

        public int? MaKhachHang { get; set; }

        public int? MaTrangThai { get; set; }

        public virtual KhachHang? MaKhachHangNavigation { get; set; }

        public virtual MayTinh? MaMayTinhNavigation { get; set; }

        public virtual TrangThaiDat? MaTrangThaiNavigation { get; set; }

        public virtual ICollection<OrderMay> OrderMays { get; set; } = new List<OrderMay>();

    }
}
