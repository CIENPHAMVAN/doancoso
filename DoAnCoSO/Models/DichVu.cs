﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOANCOSO.Models
{
    public class DichVu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        [Key]
        public int MaDichVu { get; set; }

        public string TenDichVu { get; set; }

        public decimal GiaDichVu { get; set; }

        public int? MaLoaiDichVu { get; set; }

        public virtual LoaiDichVu? MaLoaiDichVuNavigation { get; set; }
        public virtual ICollection<SuDungDichVuBan> SuDungDichVuBans { get; set; } = new List<SuDungDichVuBan>();

        public virtual ICollection<SuDungDichVuMay> SuDungDichVuMays { get; set; } = new List<SuDungDichVuMay>();
    }
}
