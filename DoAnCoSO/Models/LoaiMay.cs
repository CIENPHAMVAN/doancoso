﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOANCOSO.Models;

public partial class LoaiMay
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaLoaiMay { get; set; }

    public decimal? GiaTheoLoaiMay { get; set; }
    [StringLength(50)]
    public string? TenLoaiMay { get; set; }

    public virtual ICollection<MayTinh> MayTinhs { get; set; } = new List<MayTinh>();

}
