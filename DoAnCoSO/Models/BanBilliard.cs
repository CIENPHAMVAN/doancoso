﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOANCOSO.Models;

public partial class BanBilliard
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

    [Key]
    public int MaBan { get; set; }

    public int? MaTinhTrangBan { get; set; }

    public int? MaLoaiBan { get; set; }

    public string TenBan { get; set; }

    public virtual ICollection<BookingBan> BookingBans { get; set; } = new List<BookingBan>();

    public virtual LoaiBan? MaLoaiBanNavigation { get; set; }

    public virtual TinhTrangBan? MaTinhTrangBanNavigation { get; set; }

    public virtual ICollection<OrderBan> OrderBans { get; set; } = new List<OrderBan>();
}
