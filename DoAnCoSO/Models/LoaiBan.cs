﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class LoaiBan
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaLoaiBan { get; set; }

    [StringLength(50)]
    public string? TenLoaiBan { get; set; }

    public decimal? GiaTheoLoaiBan { get; set; }

    public virtual ICollection<BanBilliard> BanBilliards { get; set; } = new List<BanBilliard>();

}
