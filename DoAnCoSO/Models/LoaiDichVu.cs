﻿namespace DOANCOSO.Models
{
    public class LoaiDichVu
    {
        public int MaLoaiDichVu { get; set; }

        public string TenLoaiDv { get; set; }

        public virtual ICollection<DichVu> DichVus { get; set; } = new List<DichVu>();
    }
}
