﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class OrderBan
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaOrderBan { get; set; }

    public int? MaBan { get; set; }

    public int? MaKhachHang { get; set; }

    public TimeOnly? ThoiGianBatDau { get; set; }

    public TimeOnly? ThoiGianKetThuc { get; set; }
    public float? SoGio { get; set; }
    public decimal? TienBan { get; set; }

    public int? MaBookingBan { get; set; }

    public virtual BanBilliard? MaBanNavigation { get; set; }
    public virtual KhachHang? MaKhachHangNavigation { get; set; }
    public virtual BookingBan? MaBookingBanNavigation { get; set; }
    public virtual ICollection<HoaDonBan> HoaDonBans { get; set; } = new List<HoaDonBan>();  
    public virtual ICollection<SuDungDichVuBan> SuDungDichVuBans { get; set; } = new List<SuDungDichVuBan>();
}
