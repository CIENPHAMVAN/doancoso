﻿namespace DOANCOSO.Models
{
    public class BookingBan
    {
        public int MaBookingBan { get; set; }

        public DateTime? NgayDat { get; set; }

        public DateTime? NgayNhan { get; set; }

        public TimeOnly? GioNhan { get; set; }

        public int? MaBan { get; set; }

        public int? MaKhachHang { get; set; }

        public int? MaTrangThai { get; set; }

        public virtual BanBilliard? MaBanNavigation { get; set; }

        public virtual KhachHang? MaKhachHangNavigation { get; set; }

        public virtual TrangThaiDat? MaTrangThaiNavigation { get; set; }

        public virtual ICollection<OrderBan> OrderBans { get; set; } = new List<OrderBan>();

    }
}
