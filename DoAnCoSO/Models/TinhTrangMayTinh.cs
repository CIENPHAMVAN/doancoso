﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOANCOSO.Models;

public partial class TinhTrangMayTinh
{
    [Key]

    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaTinhTrangMayTinh { get; set; }
    [StringLength(50)]
    public string? TenTinhTrangMayTinh { get; set; }

    public virtual ICollection<MayTinh> MayTinhs { get; set; } = new List<MayTinh>();
}
