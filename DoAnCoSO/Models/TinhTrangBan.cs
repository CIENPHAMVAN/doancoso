﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class TinhTrangBan
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaTinhTrangBan { get; set; }
    [StringLength(50)]
    public string? TenTinhTrangBan { get; set; }

    public virtual ICollection<BanBilliard> BanBilliards { get; set; } = new List<BanBilliard>();
}
