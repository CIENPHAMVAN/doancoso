﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class HoaDonBan
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaHoaDonBan { get; set; }
    public int? MaOrderBan { get; set; }
    public int? MaKhachHang { get; set; }

    public int? MaPhuongThuc { get; set; }

    public DateTime? NgayThanhToanBan { get; set; }

    public decimal? TongTien { get; set; }

    public virtual KhachHang? MaKhachHangNavigation { get; set; }

    public virtual OrderBan? MaOrderBanNavigation { get; set; }
    public virtual PhuongThucThanhToan? MaPhuongThucNavigation { get; set; }


    [NotMapped]
    public bool IsCompleted => NgayThanhToanBan.HasValue;
}
