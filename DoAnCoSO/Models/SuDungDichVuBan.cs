﻿namespace DOANCOSO.Models
{
    public class SuDungDichVuBan
    {
        public int MaSd { get; set; }

        public int? SoLuong { get; set; }

        public decimal? ThanhTien { get; set; }

        public int? MaDichVu { get; set; }

        public int? MaOrderBan { get; set; }

        public virtual DichVu? MaDichVuNavigation { get; set; }

        public virtual OrderBan? MaOrderBanNavigation { get; set; }
    }
}
