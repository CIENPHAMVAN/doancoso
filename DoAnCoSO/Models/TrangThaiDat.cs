﻿namespace DOANCOSO.Models
{
    public class TrangThaiDat
    {
        public int MaTrangThai { get; set; }

        public string? KieuTrangThai { get; set; }

        public virtual ICollection<BookingBan> BookingBans { get; set; } = new List<BookingBan>();

        public virtual ICollection<BookingMay> BookingMays { get; set; } = new List<BookingMay>();
    }
}
