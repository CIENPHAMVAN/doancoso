﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class QuanLy
{
    [Key]
    public int MaQuanLy { get; set; }
    [StringLength(50)]
    public string? TenQuanLy { get; set; }

    public string? DiaChi { get; set; }

    public string? SoDienThoai { get; set; }

    public string? Tk { get; set; }

    public string? Mk { get; set; }
}
