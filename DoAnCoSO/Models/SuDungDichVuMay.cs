﻿namespace DOANCOSO.Models
{
    public class SuDungDichVuMay
    {
        public int MaSd { get; set; }

        public int? SoLuong { get; set; }

        public decimal? ThanhTien { get; set; }

        public int? MaDichVu { get; set; }

        public int? MaOrderMay { get; set; }

        public virtual DichVu? MaDichVuNavigation { get; set; }

        public virtual OrderMay? MaOrderMayNavigation { get; set; }
    }
}
