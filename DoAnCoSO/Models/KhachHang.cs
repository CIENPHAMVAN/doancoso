﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class KhachHang
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaKhachHang { get; set; }
    [StringLength(50)]
    [Display(Name = "Tên Khách Hàng")]
    public string? TenKhachHang { get; set; }
    [Display(Name = "Địa Chỉ")]
    public string? DiaChi { get; set; }
    [Display(Name = "Số Điện Thoại")]
    public string? SoDienThoai { get; set; }
    [Display(Name = "Nhóm Khách Hàng")]
    public int? MaLoaiKhachHang { get; set; }
    public virtual ICollection<BookingBan> BookingBans { get; set; } = new List<BookingBan>();

    public virtual ICollection<BookingMay> BookingMays { get; set; } = new List<BookingMay>();

    public virtual ICollection<HoaDonBan> HoaDonBans { get; set; } = new List<HoaDonBan>();

    public virtual ICollection<HoaDonMay> HoaDonMays { get; set; } = new List<HoaDonMay>();

    public virtual LoaiKhachHang? MaLoaiKhachHangNavigation { get; set; }

    public virtual ICollection<OrderBan> OrderBans { get; set; } = new List<OrderBan>();

    public virtual ICollection<OrderMay> OrderMays { get; set; } = new List<OrderMay>();
}
