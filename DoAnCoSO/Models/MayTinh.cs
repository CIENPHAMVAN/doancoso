﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOANCOSO.Models;

public partial class MayTinh
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public int MaMayTinh { get; set; }

    public int? MaTinhTrangMayTinh { get; set; }

    public int? MaLoaiMay { get; set; }
    
    public string TenMayTinh { get; set; }

    public virtual ICollection<BookingMay> BookingMays { get; set; } = new List<BookingMay>();

    public virtual LoaiMay? MaLoaiMayNavigation { get; set; }

    public virtual TinhTrangMayTinh? MaTinhTrangMayTinhNavigation { get; set; }

    public virtual ICollection<OrderMay> OrderMays { get; set; } = new List<OrderMay>();
}
