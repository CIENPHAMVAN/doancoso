﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class HoaDonMay
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaHoaDonMay { get; set; }

    public int? MaKhachHang { get; set; }

    public int? MaPhuongThuc { get; set; }

    public DateTime? NgayThanhToanBan { get; set; }

    public int? MaOrderMay { get; set; }

    public decimal? TongTien { get; set; }

    public virtual KhachHang? MaKhachHangNavigation { get; set; }

    public virtual PhuongThucThanhToan? MaPhuongThucNavigation { get; set; }

    public virtual OrderMay? MaOrderMayNavigation { get; set; }
}
