﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DOANCOSO.Models;

public partial class QuanlydichvuContext : DbContext
{

    public QuanlydichvuContext(DbContextOptions<QuanlydichvuContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BanBilliard> BanBilliards { get; set; }
    public virtual DbSet<BookingBan> BookingBans { get; set; }
    public virtual DbSet<BookingMay> BookingMays { get; set; }
    public virtual DbSet<DichVu> DichVus { get; set; }
    public virtual DbSet<HoaDonBan> HoaDonBans { get; set; }

    public virtual DbSet<HoaDonMay> HoaDonMays { get; set; }

    public virtual DbSet<KhachHang> KhachHangs { get; set; }

    public virtual DbSet<LoaiBan> LoaiBans { get; set; }

    public virtual DbSet<LoaiKhachHang> LoaiKhachHangs { get; set; }
    public virtual DbSet<LoaiDichVu> LoaiDichVus { get; set; }  
    public virtual DbSet<LoaiMay> LoaiMays { get; set; }

    public virtual DbSet<MayTinh> MayTinhs { get; set; }

    public virtual DbSet<OrderBan> OrderBans { get; set; }

    public virtual DbSet<OrderMay> OrderMays { get; set; }

    public virtual DbSet<PhuongThucThanhToan> PhuongThucThanhToans { get; set; }

    public virtual DbSet<QuanLy> QuanLies { get; set; }

    public virtual DbSet<TinhTrangBan> TinhTrangBans { get; set; }

    public virtual DbSet<TinhTrangMayTinh> TinhTrangMayTinhs { get; set; }
    public virtual DbSet<SuDungDichVuBan> SuDungDichVuBans { get; set; }

    public virtual DbSet<SuDungDichVuMay> SuDungDichVuMays { get; set; }
    public virtual DbSet<TrangThaiDat> TrangThaiDats { get; set; }

    /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
 #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
         => optionsBuilder.UseSqlServer("Data Source=DESKTOP-L0PS58Q\\VANCHIEN;Initial Catalog=QUANLYDICHVU;Integrated Security=True;Connect Timeout=30;Encrypt=True;Trust Server Certificate=True;Application Intent=ReadWrite;Multi Subnet Failover=False");
 */
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<BanBilliard>(entity =>
        {
            entity.HasKey(e => e.MaBan).HasName("PK__BanBilli__3520ED6C155B58BD");

            entity.Property(e => e.MaBan).ValueGeneratedOnAdd(); // để set value tự động tăng

            entity.Property(e => e.TenBan)
                .HasMaxLength(255)
                .IsUnicode(true);

            entity.HasOne(d => d.MaLoaiBanNavigation).WithMany(p => p.BanBilliards)
                .HasForeignKey(d => d.MaLoaiBan)
                .HasConstraintName("FK__BanBillia__MaLoa__412EB0B6");

            entity.HasOne(d => d.MaTinhTrangBanNavigation).WithMany(p => p.BanBilliards)
                .HasForeignKey(d => d.MaTinhTrangBan)
                .HasConstraintName("FK__BanBillia__MaTin__403A8C7D");
        });


        modelBuilder.Entity<BookingBan>(entity =>
        {
            entity.HasKey(e => e.MaBookingBan);

            entity.ToTable("BookingBan");

            entity.Property(e => e.MaBookingBan).ValueGeneratedOnAdd();
            entity.Property(e => e.NgayDat).HasColumnType("datetime");
            entity.Property(e => e.NgayNhan).HasColumnType("datetime");

            entity.HasOne(d => d.MaBanNavigation).WithMany(p => p.BookingBans)
                .HasForeignKey(d => d.MaBan)
                .HasConstraintName("FK_BookingBan_BanBilliards");

            entity.HasOne(d => d.MaKhachHangNavigation).WithMany(p => p.BookingBans)
                .HasForeignKey(d => d.MaKhachHang)
                .HasConstraintName("FK_BookingBan_KhachHang");

            entity.HasOne(d => d.MaTrangThaiNavigation).WithMany(p => p.BookingBans)
                .HasForeignKey(d => d.MaTrangThai)
                .HasConstraintName("FK_BookingBan_TrangThaiDat");
        });

        modelBuilder.Entity<BookingMay>(entity =>
        {
            entity.HasKey(e => e.MaBookingMay);

            entity.ToTable("BookingMay");

            entity.Property(e => e.MaBookingMay).ValueGeneratedOnAdd();
            entity.Property(e => e.NgayDat).HasColumnType("datetime");
            entity.Property(e => e.NgayNhan).HasColumnType("datetime");

            entity.HasOne(d => d.MaKhachHangNavigation).WithMany(p => p.BookingMays)
                .HasForeignKey(d => d.MaKhachHang)
                .HasConstraintName("FK_BookingMay_KhachHang");

            entity.HasOne(d => d.MaMayTinhNavigation).WithMany(p => p.BookingMays)
                .HasForeignKey(d => d.MaMayTinh)
                .HasConstraintName("FK_BookingMay_MayTinh");

            entity.HasOne(d => d.MaTrangThaiNavigation).WithMany(p => p.BookingMays)
                .HasForeignKey(d => d.MaTrangThai)
                .HasConstraintName("FK_BookingMay_TrangThaiDat");
        });


        modelBuilder.Entity<DichVu>(entity =>
        {
            entity.HasKey(e => e.MaDichVu);
            entity.Property(e => e.MaDichVu).ValueGeneratedOnAdd();
            entity.ToTable("DichVu");

            entity.Property(e => e.GiaDichVu).HasColumnType("decimal(18, 0)");
            entity.Property(e => e.TenDichVu).HasMaxLength(50);

            entity.HasOne(d => d.MaLoaiDichVuNavigation).WithMany(p => p.DichVus)
                .HasForeignKey(d => d.MaLoaiDichVu)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DichVu_LoaiDichVu");
        });

        modelBuilder.Entity<HoaDonBan>(entity =>
        {
            entity.HasKey(e => e.MaHoaDonBan).HasName("PK__HoaDonBa__6A50CA8A2BAF87C5");

            entity.ToTable("HoaDonBan");

            entity.Property(e => e.MaHoaDonBan).ValueGeneratedOnAdd();
            entity.Property(e => e.TongTien).HasColumnType("decimal(19, 2)");

            entity.HasOne(d => d.MaKhachHangNavigation).WithMany(p => p.HoaDonBans)
                .HasForeignKey(d => d.MaKhachHang)
                .HasConstraintName("FK__HoaDonBan__MaKha__7A672E12");

            entity.HasOne(d => d.MaPhuongThucNavigation).WithMany(p => p.HoaDonBans)
                .HasForeignKey(d => d.MaPhuongThuc)
                .HasConstraintName("FK__HoaDonBan__MaPhu__7B5B524B");

            entity.HasOne(d => d.MaOrderBanNavigation).WithMany(p => p.HoaDonBans)
                .HasForeignKey(d => d.MaOrderBan)
                .HasConstraintName("FK_HoaDonBan_OrderBan");
        });

        modelBuilder.Entity<HoaDonMay>(entity =>
        {
            entity.HasKey(e => e.MaHoaDonMay).HasName("PK__HoaDonMa__3B9E10360F209BA3");

            entity.ToTable("HoaDonMay");

            entity.Property(e => e.MaHoaDonMay).ValueGeneratedOnAdd();
            entity.Property(e => e.TongTien).HasColumnType("decimal(19, 2)");

            entity.HasOne(d => d.MaKhachHangNavigation).WithMany(p => p.HoaDonMays)
                .HasForeignKey(d => d.MaKhachHang)
                .HasConstraintName("FK__HoaDonMay__MaKha__03F0984C");

            entity.HasOne(d => d.MaOrderMayNavigation).WithMany(p => p.HoaDonMays)
                .HasForeignKey(d => d.MaOrderMay)
                .HasConstraintName("FK_HoaDonMay_OrderMay");

            entity.HasOne(d => d.MaPhuongThucNavigation).WithMany(p => p.HoaDonMays)
                .HasForeignKey(d => d.MaPhuongThuc)
                .HasConstraintName("FK__HoaDonMay__MaPhu__04E4BC85");
        });

        modelBuilder.Entity<KhachHang>(entity =>
        {
            entity.HasKey(e => e.MaKhachHang).HasName("PK__KhachHan__88D2F0E56A3B0694");

            entity.ToTable("KhachHang");

            entity.Property(e => e.MaKhachHang).ValueGeneratedOnAdd();
            entity.Property(e => e.DiaChi)
                .HasMaxLength(255)
                .IsUnicode(true);
            entity.Property(e => e.SoDienThoai)
                .HasMaxLength(20)
                .IsUnicode(false);
            entity.Property(e => e.TenKhachHang)
                .HasMaxLength(255)
                .IsUnicode(true);

            entity.HasOne(d => d.MaLoaiKhachHangNavigation).WithMany(p => p.KhachHangs)
                .HasForeignKey(d => d.MaLoaiKhachHang)
                .HasConstraintName("FK__KhachHang__MaLoa__398D8EEE");
        });
        modelBuilder.Entity<LoaiDichVu>(entity =>
        {
            entity.HasKey(e => e.MaLoaiDichVu);

            entity.ToTable("LoaiDichVu");
            entity.Property(e => e.MaLoaiDichVu).ValueGeneratedOnAdd();
            entity.Property(e => e.TenLoaiDv)
                .HasMaxLength(50)
                .HasColumnName("TenLoaiDV")
                .IsUnicode(true);
        });


        modelBuilder.Entity<LoaiBan>(entity =>
        {
            entity.HasKey(e => e.MaLoaiBan).HasName("PK__LoaiBan__96758ECA2BAAD9A3");

            entity.ToTable("LoaiBan");

            entity.Property(e => e.MaLoaiBan).ValueGeneratedOnAdd();
            entity.Property(e => e.GiaTheoLoaiBan).HasColumnType("decimal(19, 2)");
            entity.Property(e => e.TenLoaiBan)
                .HasMaxLength(255)
                .IsUnicode(true);
        });

        modelBuilder.Entity<LoaiKhachHang>(entity =>
        {
            entity.HasKey(e => e.MaLoaiKhachHang).HasName("PK__LoaiKhac__6975E7AEF86CB669");

            entity.ToTable("LoaiKhachHang");

            entity.Property(e => e.MaLoaiKhachHang).ValueGeneratedOnAdd();
            entity.Property(e => e.TenLoaiKhachHang)
                .HasMaxLength(255)
                .IsUnicode(true);
        });

        modelBuilder.Entity<LoaiMay>(entity =>
        {
            entity.HasKey(e => e.MaLoaiMay).HasName("PK__LoaiMay__612CE8E754403DDA");

            entity.ToTable("LoaiMay");

            entity.Property(e => e.MaLoaiMay).ValueGeneratedOnAdd();
            entity.Property(e => e.GiaTheoLoaiMay).HasColumnType("decimal(19, 2)");
            entity.Property(e => e.TenLoaiMay)
                .HasMaxLength(255)
                .IsUnicode(true);
        });

        modelBuilder.Entity<MayTinh>(entity =>
        {
            entity.HasKey(e => e.MaMayTinh).HasName("PK__MayTinh__AC328530C1CBC0D2");

            entity.ToTable("MayTinh");

            entity.Property(e => e.MaMayTinh).ValueGeneratedOnAdd();
            entity.Property(e => e.TenMayTinh)
                .HasMaxLength(255)
                .IsUnicode(true);

            entity.HasOne(d => d.MaLoaiMayNavigation).WithMany(p => p.MayTinhs)
                .HasForeignKey(d => d.MaLoaiMay)
                .HasConstraintName("FK__MayTinh__MaLoaiM__6EF57B66");

            entity.HasOne(d => d.MaTinhTrangMayTinhNavigation).WithMany(p => p.MayTinhs)
                .HasForeignKey(d => d.MaTinhTrangMayTinh)
                .HasConstraintName("FK__MayTinh__MaTinhT__6E01572D");
        });

        modelBuilder.Entity<OrderBan>(entity =>
        {
            entity.HasKey(e => e.MaOrderBan).HasName("PK__OrderBan__50559EF7D0B417B8");

            entity.ToTable("OrderBan");

            entity.Property(e => e.MaOrderBan).ValueGeneratedOnAdd();
            entity.Property(e => e.TienBan).HasColumnType("decimal(18, 0)");

            entity.HasOne(d => d.MaBookingBanNavigation).WithMany(p => p.OrderBans)
                .HasForeignKey(d => d.MaBookingBan)
                .HasConstraintName("FK_OrderBan_BookingBan");

            entity.HasOne(d => d.MaBanNavigation).WithMany(p => p.OrderBans)
                .HasForeignKey(d => d.MaBan)
                .HasConstraintName("FK__OrderBan__MaBan__160F4887");

            entity.HasOne(d => d.MaKhachHangNavigation).WithMany(p => p.OrderBans)
                .HasForeignKey(d => d.MaKhachHang)
                .HasConstraintName("FK__OrderBan__MaKhac__14270015");

          
        });

        modelBuilder.Entity<OrderMay>(entity =>
        {
            entity.HasKey(e => e.MaOrderMay).HasName("PK__OrderMay__F602CDA1E020FE24");

            entity.ToTable("OrderMay");

            entity.Property(e => e.MaOrderMay).ValueGeneratedOnAdd();
            entity.Property(e => e.TienMay).HasColumnType("decimal(18, 0)");

            entity.HasOne(d => d.MaBookingMayNavigation).WithMany(p => p.OrderMays)
                .HasForeignKey(d => d.MaBookingMay)
                .HasConstraintName("FK_OrderMay_BookingMay");

            entity.HasOne(d => d.MaKhachHangNavigation).WithMany(p => p.OrderMays)
                .HasForeignKey(d => d.MaKhachHang)
                .HasConstraintName("FK__OrderMay__MaKhac__18EBB532");


            entity.HasOne(d => d.MaMayTinhNavigation).WithMany(p => p.OrderMays)
                .HasForeignKey(d => d.MaMayTinh)
                .HasConstraintName("FK__OrderMay__MaMayT__1AD3FDA4");
        });

        modelBuilder.Entity<PhuongThucThanhToan>(entity =>
        {
            entity.HasKey(e => e.MaPhuongThuc).HasName("PK__PhuongTh__35F7404EA6E0B906");

            entity.ToTable("PhuongThucThanhToan");

            entity.Property(e => e.MaPhuongThuc).ValueGeneratedOnAdd();
            entity.Property(e => e.TenPhuongThucThanhToan)
                .HasMaxLength(255)
                .IsUnicode(true);
        });

        modelBuilder.Entity<QuanLy>(entity =>
        {
            entity.HasKey(e => e.MaQuanLy).HasName("PK__QuanLy__2AB9EAF8B14AFD30");

            entity.ToTable("QuanLy");

            entity.Property(e => e.MaQuanLy).ValueGeneratedOnAdd();
            entity.Property(e => e.DiaChi)
                .HasMaxLength(255)
                .IsUnicode(true);
            entity.Property(e => e.Mk)
                .HasMaxLength(10)
                .IsUnicode(true)
                .IsFixedLength()
                .HasColumnName("MK");
            entity.Property(e => e.SoDienThoai)
                .HasMaxLength(20)
                .IsUnicode(true);
            entity.Property(e => e.TenQuanLy)
                .HasMaxLength(255)
                .IsUnicode(true);
            entity.Property(e => e.Tk)
                .HasMaxLength(10)
                .IsUnicode(true)
                .IsFixedLength()
                .HasColumnName("TK");
        });

        modelBuilder.Entity<TinhTrangBan>(entity =>
        {
            entity.HasKey(e => e.MaTinhTrangBan).HasName("PK__TinhTran__DA3AC4E7C6BF0DC6");

            entity.ToTable("TinhTrangBan");

            entity.Property(e => e.MaTinhTrangBan).ValueGeneratedOnAdd();
            entity.Property(e => e.TenTinhTrangBan)
                .HasMaxLength(255)
                .IsUnicode(true);
        });
        modelBuilder.Entity<SuDungDichVuBan>(entity =>
        {
            entity.HasKey(e => e.MaSd).HasName("PK_SuDungDichVu");

            entity.ToTable("SuDungDichVuBan");

            entity.Property(e => e.MaSd)
                .ValueGeneratedOnAdd()
                .HasColumnName("MaSD");
            entity.Property(e => e.ThanhTien).HasColumnType("decimal(18, 0)");

            entity.HasOne(d => d.MaDichVuNavigation).WithMany(p => p.SuDungDichVuBans)
                .HasForeignKey(d => d.MaDichVu)
                .HasConstraintName("FK_SuDungDichVu_DichVu");

            entity.HasOne(d => d.MaOrderBanNavigation).WithMany(p => p.SuDungDichVuBans)
                .HasForeignKey(d => d.MaOrderBan)
                .HasConstraintName("FK_SuDungDichVu_OrderBan");
        });

        modelBuilder.Entity<SuDungDichVuMay>(entity =>
        {
            entity.HasKey(e => e.MaSd);

            entity.ToTable("SuDungDichVuMay");

            entity.Property(e => e.MaSd)
                .ValueGeneratedOnAdd()
                .HasColumnName("MaSD");
            entity.Property(e => e.ThanhTien).HasColumnType("decimal(18, 0)");

            entity.HasOne(d => d.MaDichVuNavigation).WithMany(p => p.SuDungDichVuMays)
                .HasForeignKey(d => d.MaDichVu)
                .HasConstraintName("FK_SuDungDichVuMay_DichVu");

            entity.HasOne(d => d.MaOrderMayNavigation).WithMany(p => p.SuDungDichVuMays)
                .HasForeignKey(d => d.MaOrderMay)
                .HasConstraintName("FK_SuDungDichVuMay_OrderMay");
        });
        modelBuilder.Entity<TinhTrangMayTinh>(entity =>
        {
            entity.HasKey(e => e.MaTinhTrangMayTinh).HasName("PK__TinhTran__94F46D445BC771EC");

            entity.ToTable("TinhTrangMayTinh");

            entity.Property(e => e.MaTinhTrangMayTinh).ValueGeneratedOnAdd();
            entity.Property(e => e.TenTinhTrangMayTinh)
                .HasMaxLength(255)
                .IsUnicode(true);
        });

        modelBuilder.Entity<TrangThaiDat>(entity =>
        {
            entity.HasKey(e => e.MaTrangThai);

            entity.ToTable("TrangThaiDat");

            entity.Property(e => e.MaTrangThai).ValueGeneratedOnAdd();
            entity.Property(e => e.KieuTrangThai).HasMaxLength(50).IsUnicode(true);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
