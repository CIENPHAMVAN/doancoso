﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DOANCOSO.Models;

public partial class OrderMay
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MaOrderMay { get; set; }

    public int? MaMayTinh { get; set; }

    public int? MaKhachHang { get; set; }

    public TimeOnly? ThoiGianBatDau { get; set; }

    public TimeOnly? ThoiGianKetThuc { get; set; }
    public decimal? TienMay { get; set; }
    public float? SoGio { get; set; }
    public int? MaBookingMay { get; set; }
    public virtual KhachHang? MaKhachHangNavigation { get; set; }

    public virtual MayTinh? MaMayTinhNavigation { get; set; }
    public virtual BookingMay? MaBookingMayNavigation { get; set; }

    public virtual ICollection<HoaDonMay> HoaDonMays { get; set; } = new List<HoaDonMay>();
    public virtual ICollection<SuDungDichVuMay> SuDungDichVuMays { get; set; } = new List<SuDungDichVuMay>();
}
