﻿
document.addEventListener("DOMContentLoaded", function () {
    const saveButton = document.getElementById("save-bookingban");

    saveButton.addEventListener("click", function (event) {
        const customerId = document.getElementById("customerDropdown").value;
        const arrivalTime = document.getElementById("arrivalTime").value;
        const tableId = document.getElementById("list-table").value;

        const bookingRequest = {
            maKhachHang: customerId,
            ngayNhan: arrivalTime,
            maBan: tableId
        };

        $.ajax({
            url: '/api/add-bookingban',
            type: 'POST',
            data: bookingRequest,
            success: function (data) {
                if (data.success) {
                    alert(data.message);
                    window.location.reload();
                } else {
                    alert('Đặt bàn thất bại: ' + data.message);
                    console.log(data.message);
                }
            },
            error: function (xhr, status, error) {
                let errorMessage = '';

                if (xhr.status === 400) {
                    const response = xhr.responseJSON;
                    errorMessage = response.message + "\n";
                    response.errors.forEach(function (err) {
                        errorMessage += err + "\n";
                    });
                } else if (xhr.status === 404) {
                    errorMessage = 'Không tìm thấy đường dẫn yêu cầu (404).';
                } else if (xhr.status === 500) {
                    const response = xhr.responseJSON;
                    errorMessage = response.message + ": " + response.details;
                } else {
                    errorMessage = 'Đặt bàn thất bại. Vui lòng thử lại sau.';
                }

                alert(errorMessage);
                console.log(`Status: ${xhr.status}, Error: ${error}, Message: ${xhr.responseText}`);
            }
        });
    });
});


//Cập nhật thông tin đặt 
$('#Chitietdatban').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button đã kích hoạt modal
    var bookingId = button.data('booking-id'); // Lấy id của bàn từ thuộc tính data

    $.ajax({
        url: '/api/bookingban/' + bookingId + '/edit',
        type: 'GET',
        success: function (data) {
            console.log(data);
            // Hiển thị thông tin của booking trong modal
            $('#customerName').text(data.bookingBan.maKhachHangNavigation.tenKhachHang);
            $('#customerName2').text(data.bookingBan.maKhachHangNavigation.tenKhachHang);
            $('#maDatBan').text(data.bookingBan.maBookingBan);
            $('#phoneNumber').text(data.bookingBan.maKhachHangNavigation.soDienThoai);
            $('#ngayNhan').val(data.bookingBan.ngayNhan);
            $('#banDat').empty(); // Xóa danh sách loại bàn hiện tại
            // Thêm danh sách loại bàn vào dropdown
            $.each(data.banBilliard, function (index, banbilliard) {
                $('#banDat').append($('<option>', {
                    value: banbilliard.maBan,
                    text: banbilliard.tenBan
                }));
            });
            // Chọn loại bàn hiện tại của bàn đang chỉnh sửa
            $('#banDat').val(data.bookingBan.maBan);
        },
        error: function () {
            alert('Đã xảy ra lỗi khi lấy thông tin đặt bàn');
        }
    });

    // Xóa đặt bàn
    $('#deleteBooking').off('click').on('click', function () {
        $.ajax({
            url: '/api/bookingban/' + bookingId,
            type: 'DELETE',
            success: function () {
                alert('Đã xóa đặt bàn thành công');
                $('#Chitietdatban').modal('hide');
                window.location.reload();
                // Bạn có thể cần cập nhật lại danh sách đặt bàn ở đây
            },
            error: function () {
                alert('Đã xảy ra lỗi khi xóa đặt bàn');
            }
        });
    });

    // Hủy đặt bàn
    $('#cancelBooking').off('click').on('click', function () {
        $.ajax({
            url: '/api/bookingban/' + bookingId + '/cancel',
            type: 'POST',
            success: function () {
                alert('Đã hủy đặt bàn thành công');
                $('#Chitietdatban').modal('hide');
                window.location.reload();
                // Bạn có thể cần cập nhật lại danh sách đặt bàn ở đây
            },
            error: function () {
                alert('Đã xảy ra lỗi khi hủy đặt bàn');
            }
        });
    });

    // Xếp bàn
    $('#arrangeBooking').off('click').on('click', function () {
        var maban = $('#banDat').val(); // Lấy giá trị của maban từ ô select
        var ngaynhannew = $('#ngayNhan').val();
        var bookingData = {
            maban: maban,
            ngayNhannew: ngaynhannew // Đảm bảo tên thuộc tính phù hợp với lớp ArrangeBanRequest
        };
        $.ajax({
            url: '/api/bookingban/' + bookingId + '/arrange',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(bookingData),
            success: function () {
                alert('Đã xếp bàn thành công');
                $('#Chitietdatban').modal('hide');
                window.location.reload();
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : error;
                console.log('Thất bại' + errorMessage);
                alert('Đã xảy ra lỗi khi xếp bàn');
            }
        });
    });

    // Nhận bàn
    $('#receiveBooking').off('click').on('click', function () {
        var maban = $('#banDat').val(); // Lấy giá trị của maban từ ô select
        $.ajax({
            url: '/api/bookingban/'+ bookingId + '/receive',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(maban),
            success: function () {
                alert('Đã nhận bàn thành công');
                $('#Chitietdatban').modal('hide');
                window.location.reload();
                // Bạn có thể cần cập nhật lại danh sách đặt bàn ở đây
            },
            error: function (xhr, status, error) {
                // Lấy thông tin lỗi từ phản hồi của server
                var errorMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : 'Đã xảy ra lỗi khi nhận bàn';
                alert(errorMessage);
            }
        });
    });




});

// Gửi yêu cầu cập nhật thông tin bàn
$('#saveEditBanButton').click(function () {
    var banId = $('#editBanId').val();
    var tenBan = $('#editTenBan').val();
    var maLoaiBan = $('#editLoaiBan').val();
    var data = {
        MaBan: banId,
        TenBan: tenBan,
        MaLoaiBan: maLoaiBan
    };
    $.ajax({
        url: '/api/BanBilliard/' + banId + '/Edit',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function () {
            alert('Cập nhật thông tin bàn thành công');
            // Đóng modal sau khi cập nhật thành công
            $('#editBanModal').modal('hide');
            // Tải lại trang để cập nhật danh sách bàn
            location.reload();
        },
        error: function () {
            alert('Đã xảy ra lỗi khi cập nhật thông tin bàn');
        }
    });
});


$(document).ready(function () {
    $('.form-check-input').change(function () {
        var anyChecked = $('.form-check-input:checked').length > 0;

        if (anyChecked) {
            var maTrangThai = $(this).val(); // Lấy giá trị của ô được chọn
            /*var dataToSend = { value: selectedValue };*/

            $('.form-check-input').not(this).prop('checked', false);

            // Gửi yêu cầu đến máy chủ để lấy dữ liệu tương ứng
            $.ajax({
                url: '/api/bookingban/getlist/'+maTrangThai, 
                type: 'GET', 
                success: function (response) {
                    // Xử lý dữ liệu từ server trả về
                    console.log(response);

                    $('#table_booking').hide();

                    // Xóa các hàng hiện tại trong bảng
                    $('#table_booking tbody').empty();

                    // Duyệt qua mỗi mục dữ liệu trong phản hồi
                    response.forEach(function (booking) {
                        // Tạo một hàng mới cho bảng
                        var row = '<tr>' +
                            '<td style="padding: 20px;"><a data-booking-id="' + booking.maBookingBan + '" data-bs-toggle="modal" data-bs-target="#Chitietdatban">' + booking.maBookingBan + '</a></td>' +
                            '<td style="padding: 20px;">' + booking.ngayNhan + '</td>' +
                            '<td style="padding: 20px;">' + booking.maKhachHangNavigation.tenKhachHang + '</td>' +
                            '<td style="padding: 20px;">' + booking.maKhachHangNavigation.soDienThoai + '</td>' +
                            '<td style="padding: 20px;">' + (booking.maBanNavigation != null ? booking.maBanNavigation.tenBan : "Trống") + '</td>'+
                            '<td style="padding: 20px;"><i class="bi bi-calendar text-warning"></i> ' + booking.maTrangThaiNavigation.kieuTrangThai + ' bàn</td>';
                      

                        row += '</tr>';

                        // Thêm hàng vào tbody của bảng
                        $('#table_booking tbody').append(row);
                    });

                    // Hiển thị bảng mới
                    $('#table_booking').show();


                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi nếu có
                    console.error(error);
                }
            });
        } else {
            window.location.reload();
           
        }

    });
});

document.addEventListener("DOMContentLoaded", function () {
    const saveButton = document.getElementById("save-bookingmay");

    saveButton.addEventListener("click", function (event) {
        const customerId = document.getElementById("customerDropdown").value;
        const arrivalTime = document.getElementById("arrivalTime").value;
        const mayId = document.getElementById("list-may").value;

        const bookingRequest = {
            maKhachHang: customerId,
            ngayNhan: arrivalTime,
            maMayTinh: mayId
        };

        $.ajax({
            url: '/api/add-bookingmay',
            type: 'POST',
            data: bookingRequest,
            success: function (data) {
                if (data.success) {
                    alert(data.message);
                    window.location.reload();
                } else {
                    alert('Đặt máy thất bại: ' + data.message);
                    console.log(data.message);
                }
            },
            error: function (xhr, status, error) {
                let errorMessage = '';

                if (xhr.status === 400) {
                    const response = xhr.responseJSON;
                    errorMessage = response.message + "\n";
                    response.errors.forEach(function (err) {
                        errorMessage += err + "\n";
                    });
                } else if (xhr.status === 404) {
                    errorMessage = 'Không tìm thấy đường dẫn yêu cầu (404).';
                } else if (xhr.status === 500) {
                    const response = xhr.responseJSON;
                    errorMessage = response.message + ": " + response.details;
                } else {
                    errorMessage = 'Đặt máy thất bại. Vui lòng thử lại sau.';
                }

                alert(errorMessage);
                console.log(`Status: ${xhr.status}, Error: ${error}, Message: ${xhr.responseText}`);
            }
        });
    });
});

// Cập nhật thông tin đặt máy
$('#Chitietdatmay').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button đã kích hoạt modal
    var bookingId = button.data('booking-id'); // Lấy id của máy từ thuộc tính data

    $.ajax({
        url: '/api/bookingmay/' + bookingId + '/edit',
        type: 'GET',
        success: function (data) {
            console.log(data);
            var bookingMay = data.bookingMay;
            var mayTinhs = data.mayTinhs;
            // Hiển thị thông tin của booking trong modal
            $('#customerName').text(data.bookingMay.maKhachHangNavigation.tenKhachHang);
            $('#customerName2').text(data.bookingMay.maKhachHangNavigation.tenKhachHang);
            $('#maDatMay').text(data.bookingMay.maBookingMay);
            $('#phoneNumber').text(data.bookingMay.maKhachHangNavigation.soDienThoai);
            $('#ngayNhan').val(data.bookingMay.ngayNhan);
            console.log("Clearing #mayDat");
            $('#mayDat').empty(); // Xóa danh sách loại máy hiện tại

            // Thêm danh sách loại máy vào dropdown
            if (Array.isArray(mayTinhs)) {
                $.each(mayTinhs, function (index, maytinh) {
                    $('#mayDat').append($('<option>', {
                        value: maytinh.maMayTinh,
                        text: maytinh.tenMayTinh
                    }));
                });

                // Chọn loại máy hiện tại của máy đang chỉnh sửa
                $('#mayDat').val(bookingMay.maMayTinh);
            } else {
                console.error("mayTinhs is not an array");
            }
        },
        error: function () {
            alert('Đã xảy ra lỗi khi lấy thông tin đặt máy');
        }
    });

    // Xóa đặt máy
    $('#deleteBooking').off('click').on('click', function () {
        $.ajax({
            url: '/api/bookingmay/' + bookingId,
            type: 'DELETE',
            success: function () {
                alert('Đã xóa đặt máy thành công');
                $('#Chitietdatmay').modal('hide');
                window.location.reload();
                // Bạn có thể cần cập nhật lại danh sách đặt máy ở đây
            },
            error: function () {
                alert('Đã xảy ra lỗi khi xóa đặt máy');
            }
        });
    });

    // Hủy đặt máy
    $('#cancelBooking').off('click').on('click', function () {
        $.ajax({
            url: '/api/bookingmay/' + bookingId + '/cancel',
            type: 'POST',
            success: function () {
                alert('Đã hủy đặt máy thành công');
                $('#Chitietdatmay').modal('hide');
                window.location.reload();
                // Bạn có thể cần cập nhật lại danh sách đặt máy ở đây
            },
            error: function () {
                alert('Đã xảy ra lỗi khi hủy đặt máy');
            }
        });
    });

    // Xếp máy
    $('#arrangeBooking').off('click').on('click', function () {
        var mamay = $('#mayDat').val(); // Lấy giá trị của mamay từ ô select
        var ngaynhannew = $('#ngayNhan').val();
        var bookingData = {
            mamay: mamay,
            ngayNhannew: ngaynhannew // Đảm bảo tên thuộc tính phù hợp với lớp ArrangeMayRequest
        };
        $.ajax({
            url: '/api/bookingmay/' + bookingId + '/arrange',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(bookingData),
            success: function () {
                alert('Đã xếp máy thành công');
                $('#Chitietdatmay').modal('hide');
                window.location.reload();
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : error;
                console.log('Thất bại' + errorMessage);
                alert('Đã xảy ra lỗi khi xếp máy');
            }
        });
    });

    // Nhận máy
    $('#receiveBooking').off('click').on('click', function () {
        var mamay = $('#mayDat').val(); // Lấy giá trị của mamay từ ô select
        $.ajax({
            url: '/api/bookingmay/' + bookingId + '/receive',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(mamay),
            success: function () {
                alert('Đã nhận máy thành công');
                $('#Chitietdatmay').modal('hide');
                window.location.reload();
                // Bạn có thể cần cập nhật lại danh sách đặt máy ở đây
            },
            error: function (xhr, status, error) {
                // Lấy thông tin lỗi từ phản hồi của server
                var errorMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : 'Đã xảy ra lỗi khi nhận máy';
                alert(errorMessage);
            }
        });
    });
});

// Gửi yêu cầu cập nhật thông tin máy
$('#saveEditMayButton').click(function () {
    var mayId = $('#editMayId').val();
    var tenMay = $('#editTenMay').val();
    var maLoaiMay = $('#editLoaiMay').val();
    var data = {
        MaMay: mayId,
        TenMayTinh: tenMay,
        MaLoaiMay: maLoaiMay
    };
    $.ajax({
        url: '/api/MayTinh/' + mayId + '/Edit',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function () {
            alert('Cập nhật thông tin máy thành công');
            // Đóng modal sau khi cập nhật thành công
            $('#editMayModal').modal('hide');
            // Tải lại trang để cập nhật danh sách máy
            location.reload();
        },
        error: function () {
            alert('Đã xảy ra lỗi khi cập nhật thông tin máy');
        }
    });
});

$(document).ready(function () {
    $('.form-check-input').change(function () {
        var anyChecked = $('.form-check-input:checked').length > 0;

        if (anyChecked) {
            var maTrangThai = $(this).val(); // Lấy giá trị của ô được chọn

            $('.form-check-input').not(this).prop('checked', false);

            // Gửi yêu cầu đến máy chủ để lấy dữ liệu tương ứng
            $.ajax({
                url: '/api/bookingmay/getlist/' + maTrangThai,
                type: 'GET',
                success: function (response) {
                    // Xử lý dữ liệu từ server trả về
                    console.log(response);

                    $('#table_booking').hide();

                    // Xóa các hàng hiện tại trong bảng
                    $('#table_booking tbody').empty();

                    // Duyệt qua mỗi mục dữ liệu trong phản hồi
                    response.forEach(function (booking) {
                        // Tạo một hàng mới cho bảng
                        var row = '<tr>' +
                            '<td style="padding: 20px;"><a data-booking-id="' + booking.maBookingMay + '" data-bs-toggle="modal" data-bs-target="#Chitietdatmay">' + booking.maBookingMay + '</a></td>' +
                            '<td style="padding: 20px;">' + booking.ngayNhan + '</td>' +
                            '<td style="padding: 20px;">' + booking.maKhachHangNavigation.tenKhachHang + '</td>' +
                            '<td style="padding: 20px;">' + booking.maKhachHangNavigation.soDienThoai + '</td>' +
                            '<td style="padding: 20px;">' + (booking.maMayNavigation != null ? booking.maMayNavigation.tenMay : "Trống") + '</td>' +
                            '<td style="padding: 20px;"><i class="bi bi-calendar text-warning"></i> ' + booking.maTrangThaiNavigation.kieuTrangThai + ' máy</td>';


                        row += '</tr>';

                        // Thêm hàng vào tbody của bảng
                        $('#table_booking tbody').append(row);
                    });

                    // Hiển thị bảng mới
                    $('#table_booking').show();


                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi nếu có
                    console.error(error);
                }
            });
        } else {
            window.location.reload();
        }

    });
});

