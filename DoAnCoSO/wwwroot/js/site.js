﻿
/// Event Add Loại Bàn
$(document).ready(function () {
    $('#addTypeTableForm').submit(function (event) {
        event.preventDefault(); // Ngăn form gửi request mặc định

        // Lấy dữ liệu từ form
        var tenLoaiBan = $('#TenLoaiBan').val();
        var giaTheoLoaiBan = $('#GiaTheoLoaiBan').val();

        // Gửi Ajax request
        $.ajax({
            type: 'POST',
            url: '/ManementTable/AddTypeTable', 
            data: JSON.stringify({ TenLoaiBan: tenLoaiBan, GiaTheoLoaiBan: giaTheoLoaiBan }), // Chuyển dữ liệu sang dạng JSON
            contentType: 'application/json',
            success: function (response) {
                $('#exampleModal').modal('hide');
                alert('Thêm loại bàn thành công');
                // Chuyển về trang danh sách bàn
                
                window.location.href = '/ManementTable/Index';
            },
            error: function (xhr, status, error) {
                alert('Thêm loại bàn thất bại: ' + error);
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManementTable/Index';
            }
        });
    });
});

// Event Thêm tình trạng Bàn

$(document).ready(function () {
    $('#addStatusTableForm').submit(function (event) {
        event.preventDefault(); // Ngăn form gửi request mặc định

        // Lấy dữ liệu từ form
        var tenTinhTrang = $('#TenTinhTrang').val();

        // Gửi Ajax request
        $.ajax({
            type: 'POST',
            url: '/ManementTable/AddStatusTable',
            data: JSON.stringify({ TenTinhTrangBan: tenTinhTrang }), // Chuyển dữ liệu sang dạng JSON
            contentType: 'application/json',
            success: function (response) {
                $('#exampleModal').modal('hide');
                alert('Thêm tình trạng bàn thành công');
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManementTable/Index';
            },
            error: function (xhr, status, error) {
                alert('Thêm tình trạng thất bại: ' + error);
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManementTable/Index';
            }
        });
    });
});

//// Thêm nhóm khách hàng
$(document).ready(function () {
    $('#addTypeCustomerForm').submit(function (event) {
        event.preventDefault(); // Ngăn form gửi request mặc định

        // Lấy dữ liệu từ form
        var TenLoaiKhachHang = $('#TenLoaiKhachHang').val();

        // Gửi Ajax request
        $.ajax({
            type: 'POST',
            url: '/Customer/AddTypeCustomer',
            data: JSON.stringify({ TenLoaiKhachHang: TenLoaiKhachHang }), // Chuyển dữ liệu sang dạng JSON
            contentType: 'application/json',
            success: function (response) {
                $('#exampleModal').modal('hide');
                alert('Thêm nhóm khách hàng thành công');
                // Chuyển về trang danh sách bàn
                window.location.href = '/Customer/Index';
            },
            error: function (xhr, status, error) {
                alert('Thêm nhóm khách hàng thất bại: ' + error);
                // Chuyển về trang danh sách bàn
                window.location.href = '/Customer/Index';
            }
        });
    });
});

//Xử lý mở bàn

$(document).ready(function () {
    var tableId;
    $('.open-table-button').click(function () {
        // Lấy tableId từ thuộc tính data của nút được nhấp vào
        tableId = $(this).data('table-id');


        $.ajax({
            url: '/ManementTable/GetCustomer',
            type: 'GET',
            success: function (data) {
                // Hiển thị danh sách khách hàng trong dropdownlist của popup
                var dropdown = $('#customerDropdown');
                dropdown.empty();
                $.each(data, function () {
                    dropdown.append($('<option />').val(this.maKhachHang).text(this.tenKhachHang));
                });

                // Hiển thị popup danh sách khách hàng
                $('#customerModal').modal('show');
            }
        });
    });

    $('#confirmCustomerButton').click(function () {
        // Lấy thông tin khách hàng và mã bàn được chọn
        var customerId = $('#customerDropdown').val();

        // Gửi thông tin lên server để thêm vào orderban
        $.ajax({
            url: '/ManementTable/AddOrderBan',
            type: 'POST',
            data: { maKH: customerId, maBan: tableId },
            success: function (data) {
                if (data.success) {
                    alert(data.message); // Hiển thị thông điệp thành công từ server
                    // Chuyển về trang danh sách bàn
                    window.location.href = '/ManementTable/Index';
                } else {
                    alert('Mở bàn thất bại: ' + data.message);// Hiển thị thông điệp lỗi từ server
                    console.log(data.message);
                    // Không cần chuyển trang khi xảy ra lỗi
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : error;
                alert('Thất bại' + errorMessage); // Hiển thị thông báo lỗi từ client
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManementTable/Index';
            }
        });

        // Đóng popup
        $('#customerModal').modal('hide');
    });
});


/*Thêm bàn mới*/
$(document).ready(function ()
{
    $('#add-table-button').click(function () {
        $.ajax({
            url: '/api/table/types',
            type: 'GET',
            success: function (data) {
                var dropdown = $('#tableType');
                dropdown.empty();
                $.each(data, function (key, value) {
                    dropdown.append($('<option></option>').attr('value', value.maLoaiBan).text(value.tenLoaiBan));
                });
            }
        });
    });

    $('#confirmAddTable').click(function () {
        var typeTable = $('#tableType').val();
        var nameTable = $('#nameTable').val();

        $.ajax({
            url: '/api/table/addtable',
            type: 'POST',
            data: { tenBan: nameTable, maLoaiBan: typeTable },
            success: function (data) {
                if (data.success) {
                    alert(data.message);

                    window.location.href = '/ManementTable/Index';
                } else {
                    alert('Thêm bàn thất bại: ' + data.message);
                    console.log(data.message);
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status === 400) {
                    var errors = xhr.responseJSON;
                    var errorMessage = "";
                    for (var key in errors) {
                        errorMessage += errors[key] + "\n";
                    }
                    alert(errorMessage);
                } else {
                    alert('Thêm Bàn Thất Bại');
                }
            }
        });
    });
});

/* Xóa Bàn */
$(document).on('click', '.delete-button', function () {
    // Lấy ID của bàn từ thuộc tính data-table-id
    var tableId = $(this).data('table-id');
    // Hiển thị popup xác nhận xóa
    $('#staticBackdrop').modal('show');
    // Gán ID của bàn vào nút xác nhận xóa trong modal
    $('#confirmDeleteButton').data('table-id', tableId);
});

// Lắng nghe sự kiện khi nút Xác nhận Xóa được nhấn
$(document).on('click', '#confirmDeleteButton', function () {
    // Lấy ID của bàn từ thuộc tính data-table-id của nút xác nhận xóa trong modal
    var tableId = $(this).data('table-id');
    // Thực hiện yêu cầu xóa bàn bằng cách gửi yêu cầu HTTP đến máy chủ
    $.ajax({
        url: '/api/table/delete-table/' + tableId,
        type: 'DELETE',
        success: function (response) {
            // Xử lý phản hồi từ máy chủ nếu cần
            // Ví dụ: reload trang hoặc cập nhật dữ liệu mà không cần reload trang
            window.location.reload(); // ví dụ reload trang
        },
        error: function (xhr, status, error) {
            // Xử lý lỗi nếu có
            alert('Lỗi khi xóa bàn!!!');
            console.error('Lỗi khi xóa bàn:', error);
        }
    });
});



/* Chỉnh Sửa Bàn */

// Lấy thông tin bàn và danh sách loại bàn khi nhấn vào nút "Chỉnh Sửa"
$('#editBanModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button đã kích hoạt modal
    var banId = button.data('table-id'); // Lấy id của bàn từ thuộc tính data
    $.ajax({
        url: '/api/BanBilliard/' + banId + '/Edit',
        type: 'GET',
        success: function (data) {
            // Hiển thị thông tin của bàn trong modal
            $('#editBanId').val(data.banBilliard.maBan);
            $('#editTenBan').val(data.banBilliard.tenBan);
            $('#editLoaiBan').empty(); // Xóa danh sách loại bàn hiện tại
            // Thêm danh sách loại bàn vào dropdown
            $.each(data.loaiBan, function (index, loaiBan) {
                $('#editLoaiBan').append($('<option>', {
                    value: loaiBan.maLoaiBan,
                    text: loaiBan.tenLoaiBan
                }));
            });
            // Chọn loại bàn hiện tại của bàn đang chỉnh sửa
            $('#editLoaiBan').val(data.banBilliard.maLoaiBan);
        },
        error: function () {
            alert('Đã xảy ra lỗi khi lấy thông tin bàn');
        }
    });
});

// Gửi yêu cầu cập nhật thông tin bàn
$('#saveEditBanButton').click(function () {
    var banId = $('#editBanId').val();
    var tenBan = $('#editTenBan').val();
    var maLoaiBan = $('#editLoaiBan').val();
    var data = {
        MaBan: banId,
        TenBan: tenBan,
        MaLoaiBan: maLoaiBan
    };
    $.ajax({
        url: '/api/BanBilliard/' + banId + '/Edit',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function () {
            alert('Cập nhật thông tin bàn thành công');
            // Đóng modal sau khi cập nhật thành công
            $('#editBanModal').modal('hide');
            // Tải lại trang để cập nhật danh sách bàn
            location.reload();
        },
        error: function () {
            alert('Đã xảy ra lỗi khi cập nhật thông tin bàn');
        }
    });
});

// Thêm loại Dịch Vụ
$(document).ready(function () {
    $('#themloaidichvuForm').submit(function (event) {
        event.preventDefault(); // Ngăn form gửi request mặc định

        // Lấy dữ liệu từ form
        var tenLoaiDichVu = $('#TenLoaiDV').val();

        // Gửi Ajax request
        $.ajax({
            type: 'POST',
            url: '/DichVu/ThemTypeDichVu',
            data: JSON.stringify({ TenLoaiDV: tenLoaiDichVu }), // Chuyển dữ liệu sang dạng JSON
            contentType: 'application/json',
            success: function (response) {
                $('#exampleModal').modal('hide');
                alert('Thêm loại dịch vụ thành công');
                // Chuyển về trang danh sách bàn
                window.location.href = '/DichVu/Index';
            },
            error: function (xhr, status, error) {
                alert('Tên Loại Dịch Vụ đã có, nhập tên khác ' + error);
              /*  // Chuyển về trang danh sách bàn
                window.location.href = '/DichVu/Index';*/
            }
        });
    });
});

// Xóa Loại Dịch Vụ
$(document).ready(function () {
    // Xử lý sự kiện click của nút "Xóa"
    $('.deleteldv-button').click(function () {
        var maLoaiDichVu = $(this).data('maloaidv');      
        // Kiểm tra xem bạn đã nhấn đúng nút trên hàng (row) được không
        console.log('Bạn đã nhấn vào nút "Xóa" trên hàng có mã loại dịch vụ: ' + maLoaiDichVu);
        var confirmation = confirm("Bạn có chắc chắn muốn xóa loại dịch vụ này?");

        if (confirmation) {
            // Gửi yêu cầu xóa loại dịch vụ đến máy chủ
            $.ajax({
                url: '/DichVu/XoaTypeDichVu', // Địa chỉ URL của action XoaTypeDichVu trong DichVuController
                type: 'POST',
                data: { maLoaiDichVu: maLoaiDichVu },
                success: function (response) {
                    // Xóa hàng (row) tương ứng trong bảng
                    $(this).closest('tr').remove();
                    alert("Đã xóa loại dịch vụ thành công.");
                    window.location.href = '/DichVu/Index';
                },
                error: function (xhr, status, error) {
                    alert("Đã xảy ra lỗi khi xóa loại dịch vụ." + error);
                }
            });
        }
    });
});

//Sửa Loại Dịch Vụ
$(document).ready(function () {
    // Bắt sự kiện khi nhấn nút lưu trong modal
    $("#btnSaveSuaLoaiDichVu").click(function () {
        // Lấy dữ liệu từ các trường nhập liệu
        var tenLoaiDv = $("#selectLoaiDichVu option:selected").text();
        var tenLoaiDvMoi = $("#newTenLoaiDv").val();
        console.log('Bạn đã nhấn vào nút "Xóa" trên hàng có mã loại dịch vụ: ' + tenLoaiDv);
        // Gửi dữ liệu lên server để sửa loại dịch vụ
        $.ajax({
            url: '/DichVu/SuaLoaiDichVu',
            type: 'POST',
            data: { tenLoaiDv: tenLoaiDv, tenLoaiDvMoi: tenLoaiDvMoi },
            success: function (response) {
                if (response.success) {
                    // Nếu sửa thành công, hiển thị thông báo thành công và làm mới trang
                    alert(response.message);
                    location.reload();
                } else {
                    // Nếu có lỗi, hiển thị thông báo lỗi
                    alert(response.message);
                }
            },
            error: function (xhr, status, error) {
                // Xử lý lỗi khi gửi request
                alert("Đã xảy ra lỗi: " + error);
            }
        });
    });
});
//Thêm Dịch Vụ
$(document).ready(function () {
    $('#add-dichvu-button').click(function () {
        $.ajax({
            url: '/api/dichvu/adddichvu',
            type: 'GET',
            success: function (data) {
                var dropdown = $('#loaiDichVuSelect');
                dropdown.empty();
                $.each(data, function (key, value) {
                    dropdown.append($('<option></option>').attr('value', value.maloaidichvu).text(value.tenloaidichvu));
                });
            }
        });
    });

    $('#confirmadddichvu').click(function () {

        var loaiDichVu = $('#loaiDichVuSelect').val();
        var tenDichVu = $('#tenDichVu').val();
        var giaDichVu = $('#giaDichVu').val();

        $.ajax({
            url: '/api/dichvu/adddichvu',
            type: 'POST',
            data: { TenDichVu: tenDichVu, GiaDichVu: giaDichVu, MaLoaiDichVu: loaiDichVu },
            success: function (data) {
                if (data.success) {
                    alert(data.message);

                    window.location.href = '/DichVu/Index';
                } else {
                    alert('Thêm dich vu thất bại: ' + data.message);
                    console.log(data.message);
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status === 400) {
                    var errors = xhr.responseJSON;
                    var errorMessage = "";
                    for (var key in errors) {
                        errorMessage += errors[key] + "\n";
                    }
                    alert(errorMessage);
                } else {
                    alert('Thêm Dich Vu that bai');
                }
            }
        });
    });
})

//Sửa Dịch Vụ
// Lấy Dữ Liệu Của Dịch Vụ Có trong Database để xuất hiện trong Modal
$('#SuaDichVuModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button đã kích hoạt modal
    var dichvuId = button.data('dichvu-id'); // Lấy id của dịch vụ từ thuộc tính data trong modal
    $.ajax({
        url: '/api/DichVu/' + dichvuId + '/Edit',
        type: 'GET',
        success: function (data) {
            // Hiển thị thông tin của dịch vụ trong modal
            $('#editDichVuId').val(data.dichVu.maDichVu);
            $('#editTenDichVu').val(data.dichVu.tenDichVu);
            $('#editGiaDichVu').val(data.dichVu.giaDichVu);
            $('#editLoaiDichVu').empty(); // Xóa danh sách loại dịch vụ hiện tại
            // Thêm danh sách loại dịch vụ vào dropdown
            $.each(data.loaiDichVu, function (index, loaiDichVu) {
                $('#editLoaiDichVu').append($('<option>', {
                    value: loaiDichVu.maLoaiDichVu,
                    text: loaiDichVu.tenLoaiDv
                }));
            });
            // Chọn loại dịch vụ hiện tại của dịch vụ đang chỉnh sửa
            $('#editLoaiDichVu').val(data.dichVu.maLoaiDichVu);
        },
        error: function () {
            alert('Đã xảy ra lỗi khi lấy thông tin dịch vụ');
        }
    });
});
//Tiếp tục sự kiện sửa dịch vụ khi ấn vào button
$('#saveEditDichVuButton').click(function () {
    var dichvuId = $('#editDichVuId').val();
    var tenDichVu = $('#editTenDichVu').val();
    var giaDichVu = $('#editGiaDichVu').val();
    var maLoaiDichVu = $('#editLoaiDichVu').val();
    var data = {
        MaDichVu: dichvuId,
        TenDichVu: tenDichVu,
        GiaDichVu: giaDichVu,
        MaLoaiDichVu: maLoaiDichVu
        
    };
    $.ajax({
        url: '/api/dichvu/' + dichvuId + '/Edit',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function () {
            alert('Cập nhật thông tin dịch vụ thành công');
            // Đóng modal sau khi cập nhật thành công
            $('#SuaDichVuModal').modal('hide');
            // Tải lại trang để cập nhật danh sách bàn
            location.reload();
        },
        error: function () {
            alert('Đã xảy ra lỗi khi cập nhật thông tin dịch vụ');
        }
    });
});

//Hiển thị thông tin dịch vụ có trong Database lên Modal Xóa
$('#XoaDichVuModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button đã kích hoạt modal
    var dichvuId = button.data('dichvu-id'); // Lấy id của dịch vụ từ thuộc tính data trong modal
    $.ajax({
        url: '/api/DichVu/' + dichvuId + '/Edit',
        type: 'GET',
        success: function (data) {
            // Hiển thị thông tin của dịch vụ trong modal
            $('#deleteMaDichVuId').val(data.dichVu.maDichVu);
            $('#deleteTenDichVu').val(data.dichVu.tenDichVu);
            $('#deleteGiaDichVuId').val(data.dichVu.giaDichVu);
            $('#deleteMaLoaiDichVu').empty(); // Xóa danh sách loại dịch vụ hiện tại
            // Chọn loại dịch vụ hiện tại của dịch vụ đang chỉnh sửa
            $('#deleteMaLoaiDichVu').val(data.dichVu.maLoaiDichVu);
            //Gán Id của Dịch Vụ vào nút xác nhận xóa trong Modal
            $('#btnDeleteDichVu').data('dichvu-id', dichvuId);
        },
        error: function () {
            alert('Đã xảy ra lỗi khi lấy thông tin dịch vụ');
        }
    });
});

// Lắng nghe sự kiện khi nút Xác nhận Xóa được nhấn
$(document).on('click', '#btnDeleteDichVu', function () {
    // Lấy ID của dịch vụ từ thuộc tính data-dichvu-id của nút xác nhận xóa trong modal
    var dichvuId = $(this).data('dichvu-id');
    // Thực hiện yêu cầu xóa bàn bằng cách gửi yêu cầu HTTP đến máy chủ
    $.ajax({
        url: '/api/service/delete-service/' + dichvuId,
        type: 'DELETE',
        success: function (response) {
            // Xử lý phản hồi từ máy chủ nếu cần
            // Ví dụ: reload trang hoặc cập nhật dữ liệu mà không cần reload trang
            alert('Xóa Dịch Vụ Thành Công');
            window.location.reload(); // ví dụ reload trang
        },
        error: function (xhr, status, error) {
            // Xử lý lỗi nếu có
            alert('Lỗi Khi Xóa Bàn!');
            console.error('Lỗi Khi Xóa Bàn:', error);
        }
    });
});

// --------------------------- Phần xử lý Computer ---------------------------->
// Thêm Máy
$(document).ready(function () {
    $('#add-computer-button').click(function () {
        $.ajax({
            url: '/api/computer/types',
            type: 'GET',
            success: function (data) {
                var dropdown = $('#ComputerType');
                dropdown.empty();
                $.each(data, function (key, value) {
                    dropdown.append($('<option></option>').attr('value', value.maLoaiMay).text(value.tenLoaiMay));
                });
            }
        });
    });

    $('#confirmAddComputer').click(function () {
        var typeComputer = $('#ComputerType').val();
        var nameComputer = $('#nameComputer').val();

        $.ajax({
            url: '/api/computer/addcomputer',
            type: 'POST',
            data: { tenMayTinh: nameComputer, maLoaiMay: typeComputer },
            success: function (data) {
                if (data.success) {
                    alert(data.message);

                    window.location.href = '/ManagementComputer/Index';
                } else {
                    alert('Thêm máy thất bại: ' + data.message);
                    console.log(data.message);
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status === 400) {
                    var errors = xhr.responseJSON;
                    var errorMessage = "";
                    for (var key in errors) {
                        errorMessage += errors[key] + "\n";
                    }
                    alert(errorMessage);
                } else {
                    alert('Thêm Máy Thất Bại');
                }
            }
        });
    });
});

// Thêm Loại Máy
$(document).ready(function () {
    $('#AddTypeComputer').submit(function (event) {
        event.preventDefault(); // Ngăn form gửi request mặc định

        // Lấy dữ liệu từ form
        var tenLoaiMay = $('#TenLoaiMay').val();
        var giaTheoLoaiMay = $('#GiaTheoLoaiMay').val();

        // Gửi Ajax request
        $.ajax({
            type: 'POST',
            url: '/ManagementComputer/AddTypeComputer',
            data: JSON.stringify({ TenLoaiMay: tenLoaiMay, GiaTheoLoaiMay: giaTheoLoaiMay }), // Chuyển dữ liệu sang dạng JSON
            contentType: 'application/json',
            success: function (response) {
                $('#exampleModal').modal('hide');
                alert('Thêm Loại Máy Thành Công');             
                window.location.href = '/ManagementComputer/Index';
            },
            error: function (xhr, status, error) {
                alert('Thêm loại bàn thất bại: ' + error);
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManagementComputer/Index';
            }
        });
    });
});
// Thêm Trạng Thái Máy
$(document).ready(function () {
    $('#addStatusComputerForm').submit(function (event) {
        event.preventDefault(); // Ngăn form gửi request mặc định

        // Lấy dữ liệu từ form
        var TenTinhTrangMayTinh = $('#TenTinhTrangMayTinh').val();

        // Gửi Ajax request
        $.ajax({
            type: 'POST',
            url: '/ManagementComputer/AddStatusComputer',
            data: JSON.stringify({ TenTinhTrangMayTinh: TenTinhTrangMayTinh }), // Chuyển dữ liệu sang dạng JSON
            contentType: 'application/json',
            success: function (response) {
                $('#exampleModal').modal('hide');
                alert('Thêm tình trạng thành công');
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManagementComputer/Index';
            },
            error: function (xhr, status, error) {
                alert('Thêm tình trạng thất bại: ' + error);
                // Chuyển về trang danh sách bàn
                window.location.href = '/ManagementComputer/Index';
            }
        });
    });
});

// Sửa Máy
// Lấy thông tin bàn và danh sách loại bàn khi nhấn vào nút "Chỉnh Sửa"
$('#editMayModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var maytinhid = button.data('computer-id')
    $.ajax({
        url: '/api/UpdateMay/' + maytinhid + '/Update',
        type: 'GET',
        success: function (data) {
            $('#editmayId').val(data.mayTinh.maMayTinh);
            $('#editTenMay').val(data.mayTinh.tenMayTinh);

            $('#editloaimayeditloaimay').empty(); // Xóa danh sách loại bàn hiện tại
            // Thêm danh sách loại bàn vào dropdown
            $.each(data.loaiMay, function (index, loaiMay) {
                $('#editloaimay').append($('<option>', {
                    value: loaiMay.maLoaiMay,
                    text: loaiMay.tenLoaiMay
                }));
            });
            // Chọn loại bàn hiện tại của bàn đang chỉnh sửa
            $('#editloaimay').val(data.mayTinh.maLoaiMay);
        },
        error: function () {
            alert('Đã xảy ra lỗi khi lấy thông tin máy tính');
        }
    });
});

// Gửi yêu cầu cập nhật thông tin máy tính
$('#Saveeditmaytinh').click(function () {
    var maytinhid = $('#editmayId').val();
    var tenMay = $('#editTenMay').val();
    var maLoaiMay = $('#editloaimay').val();
    var data = {
        MaMayTinh: maytinhid,
        TenMayTinh: tenMay,
        MaLoaiMay: maLoaiMay,
    };
    $.ajax({
        url: '/api/UpdateMay/' + maytinhid + '/Update',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function () {
            alert("Cập nhập thông tin máy tính thành công");
            $('#editMayModal').modal('hide');
            location.reload();
        },
        error: function () {
            alert('Đã xảy ra lỗi khi cập nhập thông tin máy');
        }
    })
})

// Xóa Máy 
$(document).on('click', '.delete-button', function () {
    // Lấy ID của bàn từ thuộc tính data-table-id
    var computerId = $(this).data('computer-id');
    // Hiển thị popup xác nhận xóa
    $('#staticBackdrop').modal('show');
    // Gán ID của bàn vào nút xác nhận xóa trong modal
    $('#confirmDeleteButtonComputer').data('computer-id', computerId);
});

// Lắng nghe sự kiện khi nút Xác nhận Xóa được nhấn
$(document).on('click', '#confirmDeleteButtonComputer', function () {
    // Lấy ID của bàn từ thuộc tính data-table-id của nút xác nhận xóa trong modal
    var computerId = $(this).data('computer-id');
    // Thực hiện yêu cầu xóa bàn bằng cách gửi yêu cầu HTTP đến máy chủ
    $.ajax({
        url: '/api/computer/delete-computer/' + computerId,
        type: 'DELETE',
        success: function () {
            alert("xóa máy tính thành công");
            $('#editMayModal').modal('hide');
            location.reload();
        },
        error: function (xhr, status, error) {
            // Xử lý lỗi nếu có
            alert('Lỗi khi xóa máy tính!!!');
            console.error('Lỗi khi xóa máy tính:', error);
        }
    });
});

/*Xử lý mở máy*/
$(document).ready(function () {
    var computerId;
    $('.open-computer-button').click(function () {
        computerId = $(this).data('computer-id');
        $.ajax({
            url: '/ManagementComputer/GetKhachHang',
            type: 'GET',
            success: function (data) {
                var dropdownkhachhang = $('#customerDropdownMayTinh');
                dropdownkhachhang.empty();
                $.each(data, function () {
                    dropdownkhachhang.append($('<option />').val(this.maKhachHang).text(this.tenKhachHang));
                });
                $('#customerModal').modal('show');
            }
        });
    });
    $('#confirmCustomerButtonMayTinh').click(function () {
        var customerId = $('#customerDropdownMayTinh').val();
        $.ajax({
            url: '/ManagementComputer/AddOrderMay',
            type: 'POST',
            data: {
                maKhacHHang: customerId, maMayTinh: computerId
            },
            success: function (data) {
                if (data.success === true) { // Kiểm tra giá trị của 'success'
                    alert(data.message); // Hiển thị thông báo thành công
                    window.location.href = '/ManagementComputer/Index'; // Chuyển hướng đến trang Index
                } else {
                    alert('Mở máy thất bại: ' + data.message); // Hiển thị thông báo lỗi nếu có
                    console.log(data.message); // Ghi log lỗi ra console
                }
            },
            error: function (xhr, status, error) {
                var errMessage = xhr.responseJSON && xhr.responseJSON.message ? xhr.responseJSON.message : error;
                alert('Thất Bại' + errMessage);
                window.location.href = '/ManagementComputer/Index';
            }
        });
        $('#customerModal').modal('hide');
    });
});
/*--------------- Xử lý thêm dịch vụ sử dụng------------------*/

//Hiện hóa đơn dịch vụ bên bàn
async function fetchInvoices(maBan) {
    try {
        var hiddenMaOrderBan = document.getElementById('maOrderBan');

        const response = await fetch(`/api/list-service/${maBan}`);
        if (response.status === 404) {
            document.getElementById('invoiceTable').getElementsByTagName('tbody')[0].innerHTML = '<tr><td colspan="6">No data available</td></tr>';
        } else if (response.ok) {

            const orderBan = await response.json();
            hiddenMaOrderBan.value = orderBan.maOrderBan;


            const services = orderBan.suDungDichVuBans;

            // Nhóm các dịch vụ theo MaDichVu
            const groupedServices = services.reduce((acc, service) => {
                const { maDichVuNavigation, soLuong, thanhTien, maSd } = service;
                if (!acc[service.maDichVu]) {
                    acc[service.maDichVu] = {
                        maSd: maSd,
                        tenLoaiDv: maDichVuNavigation.maLoaiDichVuNavigation.tenLoaiDv,
                        tenDichVu: maDichVuNavigation.tenDichVu,
                        giaDichVu: maDichVuNavigation.giaDichVu,
                        soLuong: 0,
                        thanhTien: 0
                    };
                }
                acc[service.maDichVu].soLuong += soLuong;
                acc[service.maDichVu].thanhTien += thanhTien;
                return acc;
            }, {});

            // Chuyển đổi groupedServices thành một mảng để dễ dàng hiển thị
            const serviceArray = Object.values(groupedServices);

            let totalQuantity = 0;
            let totalAmount = 0;

            // Hiển thị dịch vụ lên bảng
            let tableBody = '';
            serviceArray.forEach((service, index) => {
                tableBody += `<tr>
                            <td class="text-center">${index + 1}</td>
                            <td style="display: flex">
                                    <span class="mx-2 quantity">${service.tenLoaiDv}</span>
                                 <button type="button" class="btn btn-outline-danger btn-circle delete-btn">
                                        <i class="bi bi-trash3"></i>
                                 </button>
                            </td>
                            <td class="text-center">${service.tenDichVu}</td>
                            <td class="text-center">${service.giaDichVu} vnđ</td>    
                            
                            <td class="text-center" style="display: flex;justify-content: center;">
                                <button type="submit" class="btn btn-outline-secondary btn-circle minus-btn" value="${service.maSd}"> − </button>
                                <input type="hidden" id="hidden-maSd" value="${service.maSd}" />
                                <span class="mx-2 quantity">${service.soLuong}</span>
                                <button type="button" class="btn btn-outline-secondary btn-circle plus-btn"> + </button>
                            </td>

                            <td class="text-center">
                                <span class="mx-2 thanhtien"> ${service.thanhTien} vnđ</span>
                            </td>
                        </tr>`;
                totalQuantity += service.soLuong;
                totalAmount += service.thanhTien;
            });

            document.getElementById('invoiceTable').getElementsByTagName('tbody')[0].innerHTML = tableBody;
            const tableFooter = `<tr>
                        <td colspan="4" style="text-align: left;"><strong>Total</strong></td>
                        <td class="text-center"><strong>${totalQuantity}</strong></td>
                        <td class="text-center"><strong>${totalAmount} vnđ</strong></td>
                    </tr>`;
            document.getElementById('invoiceTable').getElementsByTagName('tfoot')[0].innerHTML = tableFooter;

        } else {
            console.error('Failed to fetch invoices');
        }
    } catch (error) {
        console.error('Error:', error);
    }
}
//Hiện hóa đơn dịch vụ bên máy
async function fetchComputerInvoices(maMay) {
    try {
        const hiddenMaOrderMay = document.getElementById('maOrderMay');
        const response = await fetch(`/api/list-service-computer/${maMay}`);

        if (response.status === 404) {
            document.getElementById('computerInvoiceTable').getElementsByTagName('tbody')[0].innerHTML = '<tr><td colspan="6">No data available</td></tr>';
        } else if (response.ok) {
            const orderMay = await response.json();
            hiddenMaOrderMay.value = orderMay.maOrderMay;

            const services = orderMay.suDungDichVuMays;

            // Nhóm các dịch vụ theo MaDichVu
            const groupedServices = services.reduce((acc, service) => {
                const { maDichVuNavigation, soLuong, thanhTien, maSd } = service;
                if (!acc[service.maDichVu]) {
                    acc[service.maDichVu] = {
                        maSd: maSd,
                        tenLoaiDv: maDichVuNavigation.maLoaiDichVuNavigation.tenLoaiDv,
                        tenDichVu: maDichVuNavigation.tenDichVu,
                        giaDichVu: maDichVuNavigation.giaDichVu,
                        soLuong: 0,
                        thanhTien: 0
                    };
                }
                acc[service.maDichVu].soLuong += soLuong;
                acc[service.maDichVu].thanhTien += thanhTien;
                return acc;
            }, {});

            // Chuyển đổi groupedServices thành một mảng để dễ dàng hiển thị
            const serviceArray = Object.values(groupedServices);
            let totalQuantity = 0;
            let totalAmount = 0;

            // Hiển thị dịch vụ lên bảng
            let tableBody = '';
            serviceArray.forEach((service, index) => {
                tableBody += `<tr>
                    <td class="text-center">${index + 1}</td>
                    <td style="display: flex">
                        <span class="mx-2 quantity">${service.tenLoaiDv}</span>
                        <button type="button" class="btn btn-outline-danger btn-circle delete-btn">
                            <i class="bi bi-trash3"></i>
                        </button>
                    </td>
                    <td class="text-center">${service.tenDichVu}</td>
                    <td class="text-center">${service.giaDichVu} vnđ</td>
                    <td class="text-center" style="display: flex;justify-content: center;">
                        <button type="button" class="btn btn-outline-secondary btn-circle minus-btn" value="${service.maSd}"> − </button>
                        <input type="hidden" id="hidden-maSd" value="${service.maSd}" />
                        <span class="mx-2 quantity">${service.soLuong}</span>
                        <button type="button" class="btn btn-outline-secondary btn-circle plus-btn"> + </button>
                    </td>
                    <td class="text-center">
                        <span class="mx-2 thanhtien">${service.thanhTien} vnđ</span>
                    </td>
                </tr>`;
                totalQuantity += service.soLuong;
                totalAmount += service.thanhTien;
            });

            document.getElementById('computerInvoiceTable').getElementsByTagName('tbody')[0].innerHTML = tableBody;
            document.getElementById('computerInvoiceTable').getElementsByTagName('tfoot')[0].innerHTML = `
                <tr>
                    <td colspan="4" style="text-align: left;"><strong>Total</strong></td>
                    <td class="text-center"><strong>${totalQuantity}</strong></td>
                    <td class="text-center"><strong>${totalAmount} vnđ</strong></td>
                </tr>`;
        } else {
            console.error('Failed to fetch invoices');
        }
    } catch (error) {
        console.error('Error:', error);
    }
}


document.querySelectorAll('.add-service').forEach(button => {
    button.addEventListener('click', function () {
        const maBan = this.getAttribute('data-table-id');
        // Set the values into hidden input fields
        document.getElementById('maBan').value = maBan;

        fetchInvoices(maBan);
    });
});


document.querySelectorAll('.add-service-computer').forEach(button => {
    button.addEventListener('click', function () {
        const maMay = this.getAttribute('data-computer-id');
        document.getElementById('maMay').value = maMay;
        fetchComputerInvoices(maMay);
    });
});


//Hiển thị dịch vụ cùng giá tiền bên bàn
document.getElementById('selectLoaiDichVu').addEventListener('change', async function () {
    const maLoaiDichVu = this.value;
    const selectDichVu = document.getElementById('selectDichVu');
    

    // Gửi yêu cầu đến API để lấy danh sách dịch vụ theo loại dịch vụ
    try {
        const response = await fetch(`/api/service-with/${maLoaiDichVu}`);
        if (response.ok) {

            const dichVus = await response.json();
            let options = '';

            dichVus.forEach(dichVu => {
                options += `<option value="${dichVu.maDichVu}" data-gia="${dichVu.giaDichVu}">${dichVu.tenDichVu}</option>`;
            });
            selectDichVu.innerHTML = options;
      

        } else {
            selectDichVu.innerHTML = '<option value="">Không có dịch vụ tồn tại</option>';
        }
    } catch (error) {
        console.error('Error fetching services:', error);
        selectDichVu.innerHTML = '<option value="">Lỗi không load được dịch vụ</option>';
    }
});

document.getElementById('selectDichVu').addEventListener('change', function () {
    const selectedOption = this.options[this.selectedIndex];
    const gia = selectedOption.getAttribute('data-gia');
    document.getElementById('giaDichVu').value = `${gia}`;
});


/*Thêm dịch vụ bên bàn*/
document.getElementById('addServiceButton').addEventListener('click', async function () {
    const maOrderBan = document.getElementById('maOrderBan').value; // Lấy mã order bàn tương ứng
    const maDichVu = document.getElementById('selectDichVu').value;
    const soLuong = parseInt(document.getElementById('soLuong').value);
    const giaDichVu = parseFloat(document.getElementById('giaDichVu').value);
    const maBan = document.getElementById("maBan").value;
const serviceData = {
    MaOrderBan: maOrderBan,
    MaDichVu: maDichVu,
    SoLuong: soLuong,
    ThanhTien: giaDichVu * soLuong
};

try {
    const response = await fetch('/api/add-or-update-service', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(serviceData)
    });

    if (response.ok) {
        //Gọi lại hàm hiển thị danh sách dịch vụ
        fetchInvoices(maBan);
       
    } else {
        alert('Có lỗi xảy ra khi thêm dịch vụ.');
    }
} catch (error) {
    console.error('Error:', error);
    alert('Có lỗi xảy ra khi thêm dịch vụ.');
}
});
document.getElementById('addServiceButton').addEventListener('click', async function () {
    const maOrderMay = document.getElementById('maOrderMay').value;
    const maDichVu = document.getElementById('selectDichVu').value;
    const soLuong = parseInt(document.getElementById('soLuong').value);
    const giaDichVu = parseFloat(document.getElementById('giaDichVu').value);
    const maMay = document.getElementById("maMay").value;

    const serviceData = {
        MaOrderMay: maOrderMay,
        MaDichVu: maDichVu,
        SoLuong: soLuong,
        ThanhTien: giaDichVu * soLuong
    };

    try {
        const response = await fetch('/api/add-or-update-service-computer', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(serviceData)
        });

        if (response.ok) {
            fetchComputerInvoices(maMay);
        } else {
            alert('Có lỗi xảy ra khi thêm dịch vụ.');
        }
    } catch (error) {
        console.error('Error:', error);
        alert('Có lỗi xảy ra khi thêm dịch vụ.');
    }
});


document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('invoiceTable').addEventListener('click', async function (event) {
        //tăng
        if (event.target.classList.contains('plus-btn')) {
            event.preventDefault();
            const button = event.target;
            const row = button.closest('tr'); // Tìm row chứa button được click
            const maSd = row.querySelector('#hidden-maSd').value;
            const maBan = document.getElementById('maBan').value;

            // Gửi yêu cầu tới server để cập nhật số lượng mới
            try {
                const updateResponse = await fetch(`/api/increment-service/${maSd}`, {
                    method: 'POST', // PUT thường được dùng cho update
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                if (updateResponse.ok) {
                    // Cập nhật UI nếu server xử lý thành công
                    fetchInvoices(maBan);
                } else {
                    console.error('Failed to update service quantity');
                }
            } catch (error) {
                console.error('Error updating service quantity:', error);
            }
        }

        /// giảm 
        if (event.target.classList.contains('minus-btn')) {
            event.preventDefault();
            const button = event.target;
            const row = button.closest('tr'); // Tìm row chứa button được click
            const maSd = row.querySelector('#hidden-maSd').value;
            const maBan = document.getElementById('maBan').value;

            // Gửi yêu cầu tới server để cập nhật số lượng mới
            try {
                const updateResponse = await fetch(`/api/decrease-service/${maSd}`, {
                    method: 'POST', // PUT thường được dùng cho update
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                if (updateResponse.ok) {
                    // Cập nhật UI nếu server xử lý thành công
                    fetchInvoices(maBan);
                } else {
                    console.error('Failed to update service quantity');
                }
            } catch (error) {
                console.error('Error updating service quantity:', error);
            }
        }

        //xóa
        if (event.target.classList.contains('delete-btn') || event.target.closest('.delete-btn')) {
            event.preventDefault();
            const button = event.target;
            const row = button.closest('tr'); // Tìm row chứa button được click
            const maSd = row.querySelector('#hidden-maSd').value;
            const maBan = document.getElementById('maBan').value;

            // Gửi yêu cầu tới server để xóa
            try {
                const updateResponse = await fetch(`/api/delete-with/${maSd}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                if (updateResponse.ok) {
                    // Cập nhật UI nếu server xử lý thành công
                    fetchInvoices(maBan);
                } else {
                    console.error('Failed to delete service used  ');
                }
            } catch (error) {
                console.error('Error deleting service used:', error);
            }
        }

    });
});
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('computerInvoiceTable').addEventListener('click', async function (event) {
        const handleAction = async (url, maSd, maMay) => {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                if (response.ok) {
                    fetchComputerInvoices(maMay);
                } else {
                    console.error('Failed to update service quantity');
                }
            } catch (error) {
                console.error('Error updating service quantity:', error);
            }
        };

        if (event.target.classList.contains('plus-btn')) {
            event.preventDefault();
            const maSd = event.target.closest('tr').querySelector('#hidden-maSd').value;
            const maMay = document.getElementById('maMay').value;
            handleAction(`/api/increment-service-computer/${maSd}`, maSd, maMay);
        }

        if (event.target.classList.contains('minus-btn')) {
            event.preventDefault();
            const maSd = event.target.closest('tr').querySelector('#hidden-maSd').value;
            const maMay = document.getElementById('maMay').value;
            handleAction(`/api/decrease-service-computer/${maSd}`, maSd, maMay);
        }

        if (event.target.closest('.delete-btn')) {
            event.preventDefault();
            const maSd = event.target.closest('tr').querySelector('#hidden-maSd').value;
            const maMay = document.getElementById('maMay').value;

            if (confirm('Are you sure you want to delete this service?')) {
                try {
                    const response = await fetch(`/api/delete-with-computer/${maSd}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    });

                    if (response.ok) {
                        fetchComputerInvoices(maMay);
                    } else {
                        console.error('Failed to delete service');
                    }
                } catch (error) {
                    console.error('Error deleting service:', error);
                }
            }
        }
    });
});



///Thanh toán bên bàn
$(document).on('click', '.payment-button', function () {

    var maBan = $(this).data('table-id');

    $('#confirmPaymentModal').modal('show');

    $('#confirmPaymentButton').attr('href', `/HoaDon/ThanhToan?maBan=${maBan}`);
});

function printBill() {
    // Tạo một bản sao của phần nội dung cần in
    var printContent = document.querySelector('.card').cloneNode(true);

    // Tạo một cửa sổ mới để in
    var printWindow = window.open('', '_blank');

    // Thêm phần nội dung đã sao chép vào cửa sổ in
    printWindow.document.body.appendChild(printContent);

    // In cửa sổ mới
    printWindow.print();
}
//Thanh toán bên máy
$(document).on('click', '.computer-payment-button', function () {
    var maMay = $(this).data('computer-id');

    $('#confirmPaymentModalComputer').modal('show');

    $('#confirmPaymentButtonComputer').attr('href', `/HoaDon/ThanhToanMay?maMay=${maMay}`);
});



