﻿namespace DOANCOSO.DTOs
{
    public class SaleReportDto
    {

        public DateTime Date { get; set; }
        public decimal TotalSales { get; set; }
        public int NumberOfSales { get; set; }
    }
}
