﻿using DOANCOSO.Models;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);




builder.Services.AddDbContext<QuanlydichvuContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DemoConnection")));

builder.Services.AddRazorPages();
builder.Services.AddControllers();

// Add services to the container.
builder.Services.AddControllersWithViews()
                .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles); //x? lư v?n ?? v? chu tŕnh ho?c tham chi?u l?ng nhau khi serialize ??i t??ng thành JSON.

builder.Services.AddSession();
builder.Services.AddHttpContextAccessor();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();
app.UseSession();

app.UseAuthorization();



app.MapRazorPages();


app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=QuanLyDatBan}/{action=Index}/{id?}");

    endpoints.MapControllerRoute(
        name: "api",
        pattern: "api/{controller}/{action=Index}/{id?}");

    endpoints.MapControllerRoute(
        name: "sales-report",
        pattern: "sales-report/{action}/{year?}/{startDate?}/{endDate?}",
        defaults: new { controller = "SalesReport", action = "MonthlySalesReport" });
    endpoints.MapControllers();
    endpoints.MapRazorPages();
});
app.Run();
